class AddMoreDetailsToOrder < ActiveRecord::Migration
  def up
    add_column :orders, :category_id, :integer
    remove_column :orders, :style

    add_index :orders, :category_id
  end

  def down
    add_column :orders, :style, :string
  end
end
