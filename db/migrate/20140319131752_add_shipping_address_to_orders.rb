class AddShippingAddressToOrders < ActiveRecord::Migration
  def up
    add_column :orders, :shipping_info, :text

    remove_column :orders, :category_id
    remove_column :orders, :size
    remove_column :orders, :quantity
  end

  def down
    add_column :orders, :category_id, :integer
    add_column :orders, :size, :string
    add_column :orders, :quantity, :integer

    remove_column :orders, :shipping_info

    add_index :orders, :category_id
  end
end
