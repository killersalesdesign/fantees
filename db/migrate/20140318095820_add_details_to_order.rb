class AddDetailsToOrder < ActiveRecord::Migration
  def up
    add_column :orders, :product_id, :integer
    add_column :orders, :user_id, :integer
    add_column :orders, :token, :string
    add_column :orders, :payer_id, :string
    add_column :orders, :style, :string
    add_column :orders, :size, :string
    add_column :orders, :quantity, :integer

    remove_column :orders, :first_name
    remove_column :orders, :last_name
    remove_column :orders, :billing_address
    remove_column :orders, :shipping_address

    add_index :orders, :product_id
    add_index :orders, :user_id
  end

  def down
    remove_column :orders, :product_id
    remove_column :orders, :token
    remove_column :orders, :payer_id
    remove_column :orders, :style
    remove_column :orders, :size
    remove_column :orders, :quantity

    add_column :orders, :first_name, :string
    add_column :orders, :last_name, :string
    add_column :orders, :billing_address, :string
    add_column :orders, :shipping_address, :string
  end
end
