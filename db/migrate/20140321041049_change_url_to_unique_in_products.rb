class ChangeUrlToUniqueInProducts < ActiveRecord::Migration
  def up
    add_column :products, :slug, :string

    add_index :products, :slug, unique: true
    remove_index :products, :url
  end

  def down
    remove_column :products, :slug

    add_index :products, :url
  end
end
