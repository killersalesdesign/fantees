class AddIndexToProducts < ActiveRecord::Migration
  def change
    change_column :products, :url, :string

    add_index :products, :url
  end
end
