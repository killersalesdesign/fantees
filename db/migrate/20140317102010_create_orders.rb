class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :ip_address
      t.string :card_type
      t.date :card_expires_on
      t.string :billing_address
      t.string :shipping_address

      t.timestamps
    end
  end
end
