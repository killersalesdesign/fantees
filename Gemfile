source 'http://rubygems.org'

# App
gem 'rails', '3.2.13'
gem 'mysql2'
gem 'figaro'
gem 'devise', '2.2.4'
gem 'rabl'
# gem 'oj'
gem 'ledermann-rails-settings', :require => 'rails-settings'
gem 'paperclip', '~> 3.4.2'
gem 'aws-sdk'
gem 'friendly_id'
gem 'groupdate'
gem 'mail_form'
gem 'net-ssh', '~> 2.8.0'
# gem 'will_paginate'
gem 'kaminari'
gem 'algoliasearch-rails', '~> 1.13'
# gem 'searchkick'

# Admin
# gem 'rails_admin'
gem 'meta_search'

# Worker
gem 'sidekiq', '~> 3.1.3'
gem 'sinatra', :require => nil

# Assets
gem 'gon'
gem 'eco'
gem 'haml-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'js-routes'
gem 'toastr-rails'
gem 'angularjs-rails'
gem 'angular-ui-rails'
gem 'ng-rails-csrf'
gem 'hogan_assets'

# Cron Jobs
gem 'whenever', :require => false

# Server
gem 'unicorn'
gem 'airbrake', '~> 4.0.0'

# Payment gateway
# gem 'activemerchant', :git => 'git@github.com:arcknine/active_merchant.git'
gem 'activemerchant', '1.43.1'
gem 'active_utils'


group :assets do
  gem 'coffee-rails', '~> 3.2.1'
  gem 'therubyracer', :platforms => :ruby
  gem 'uglifier', '>= 1.0.3'
  gem 'haml_coffee_assets'
  gem 'execjs'
  gem 'asset_sync'
end

group :development do
  gem 'better_errors', '~> 1.1.0'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'thin'
  gem 'letter_opener'

  gem 'capistrano', '~> 2.15.5'
  gem 'capistrano_colors'
  gem 'capistrano-ext'
  gem 'rvm-capistrano', :require => false
  gem 'capistrano-rbenv', '1.0.1'
  gem 'capistrano-sidekiq'
end

gem 'rspec-rails', :group => [:test, :development]

group :test do
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'guard-rspec'
  gem 'database_cleaner'
  # gem 'simplecov', '~> 0.7.1', :require => false
end

group :production do
  gem 'newrelic_rpm'
end