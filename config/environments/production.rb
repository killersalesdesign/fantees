Fantees::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = false

  # Generate digests for assets URLs
  config.assets.digest = true
  config.assets.enabled = true
  config.assets.version = "1.0.0"

  # Defaults to nil and saved in location specified by config.assets.prefix
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  config.action_controller.asset_host = "//#{ENV['AWS_ASSET_HOST']}"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  config.assets.precompile += %w( home.css dashboard.js dashboard.css home.js admin.css products.js products.css )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # User mailer
  # config.action_mailer.asset_host = 'https://fantees.com.au/'
  config.action_mailer.asset_host = "https://#{ENV['BASE_URL']}"
  # config.action_mailer.default_url_options = { :host => 'fantees.com.au', :protocol => 'https' }
  config.action_mailer.default_url_options = { :host => "#{ENV['BASE_URL']}", :protocol => "https" }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :user_name => ENV['MAILER_USERNAME'],
    :password  => ENV['MAILER_PASSWORD'],
    :domain    => ENV['MAILER_DOMAIN'],
    :address   => ENV['MAILER_ADDRESS'],
    :port      => ENV['MAILER_PORT'],
    :authentication => :plain,
    :enable_starttls_auto => true
  }

  # Active merchant api credentials
  config.after_initialize do
    ActiveMerchant::Billing::Base.mode = :production
    paypal_options = {
      :login     => ENV['PAYPAL_API_LOGIN'],
      :password  => ENV['PAYPAL_API_PASSWORD'],
      :signature => ENV['PAYPAL_API_SIGNATURE']
    }
    ::STANDARD_GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
    ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
  end
end
