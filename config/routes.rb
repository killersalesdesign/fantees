Fantees::Application.routes.draw do
  root :to => 'home#index'

  devise_for :users, :skip => [:registrations]

  get "dashboard", :controller => 'dashboard/home', :action => :index
  namespace :dashboard do
    resources :users, :only => [:show, :update]
    resources :giftcards, :only => [:index, :show ]
    resources :orders, :only => [:index, :show, :update] do

      member do
        get :ordered_products
      end
      collection do
        get :latest
      end
    end
  end

  resources :products, :only => [:index, :show] do
    collection do
      get  :checkout
      post :set_checkout_session
      post :paypal_checkout
      post :eway_checkout
      post :order_checkout
      get  "api/:slug", :action => :product_info
      get  "upsell/:key", :action => :product_upsell
      # get :success, :action => :return_url
    end
    member do
      get :success, :action => :return_url
    end
  end
  # get "product_info/:slug", :controller => :products, :action => :product_info
  get "products/:id/:url", :controller => "products", :action => :show

  resources :categories, :only => [:index, :show]
  resources :orders, :only => [:show, :create] do
    member do
      get :success, :action => :show
    end
    collection do
      post :update_unfinished, :action => :update_unfinished_order
    end
  end
  get "order/rebuild/:tracking_id", :controller => :orders, :action => :rebuild

  # checkout pages
  resources :cart, :only => [:index, :create, :update, :destroy] do
    collection do
      get :all
      get :creditcard
      get :paypal_express
      get :checkout
      post :checkout
      get :giftcards
      post :giftcard_express

    end
  end
  get "cart/rebuild/:tracking_id", :controller => :cart, :action => :rebuild

  post "pages/send_contact_us"
  get "contact-us",           :controller => :pages, :action => :contact_us
  get "about-us",             :controller => :pages, :action => :about_us
  get "faq",                  :controller => :pages, :action => :faq
  get "privacy-policy",       :controller => :pages, :action => :privacy
  get "terms-of-service",     :controller => :pages, :action => :terms
  get "shipping",             :controller => :pages, :action => :shipping
  get "returns-exchanges",    :controller => :pages, :action => :returns_exchanges
  get "sizing",               :controller => :pages, :action => :sizing
  get "process",              :controller => :pages, :action => :processing
  get "pinterest-56764.html", :controller => :pages, :action => :pinterest
  get "win",                  :controller => :pages, :action => :win
  get "coupons/:code",        :controller => :coupons, :action => :show
  get "giftcards/:code",      :controller => :giftcards, :action => :show
  get "bundle",               :controller => :home, :action => :bundled
  get "giftcards",            :controller => :home, :action => :giftcards
  get "home/search",               :controller => :home, :action => :search
  get "search/name",          :controller => :products, :action => :name_search
  post "search/name/notify",  :controller => :products, :action => :name_search_notify

  get "render_email_pixel/:time_frame",   :controller => :mail_tracker, :action => :render_pixel, :as => :render_email_pixel

  require 'sidekiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'admin' && password == 'f@nt33s'
  end
  mount Sidekiq::Web => '/sidekiq'

  get "fathersday",           :controller => :home, :action => :custom_url
  get "pet-rescue",           :controller => :home, :action => :custom_url
  get "fishing-shirts",       :controller => :home, :action => :fishing_shirts
  get "nurse-shirts",         :controller => :home, :action => :nurse_shirts
  get "mechanic-shirts",      :controller => :home, :action => :mechanic_shirts
  get "sprint-car-shirts",    :controller => :home, :action => :sprint_car_shirts
  get "firefighter-shirts",   :controller => :home, :action => :firefighter_shirts
  get "dog-shirts",           :controller => :home, :action => :dog_shirts
  get "builder-shirts",       :controller => :home, :action => :builder_shirts
  get "electrician-shirts",   :controller => :home, :action => :electrician_shirts
  get "dirt-bike-shirts",     :controller => :home, :action => :dirt_bike_shirts
  get "british-comedy-shirts",:controller => :home, :action => :british_comedy_shirts
  get "beard-shirts",         :controller => :home, :action => :beard_shirts
  get "drag-racing-shirts",   :controller => :home, :action => :drag_racing_shirts
  get "camping-shirts",       :controller => :home, :action => :camping_shirts
  get "football-shirts",      :controller => :home, :action => :football_shirts
  get "torana-shirts",        :controller => :home, :action => :torana_shirts
  get "rum-shirts",           :controller => :home, :action => :rum_shirts
  get "wine-shirts",          :controller => :home, :action => :wine_shirts
  get "v8-shirts",            :controller => :home, :action => :v8_shirts
  get "army-shirts",          :controller => :home, :action => :army_shirts
  get "burnout-shirts",       :controller => :home, :action => :burnout_shirts
  get "farmanimalrescue",     :controller => :home, :action => :farmanimalrescue_shirts

  get ":slug",                :controller => "products", :action => :show
  
end
