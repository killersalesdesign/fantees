##################################
##### SET THESE VARIABLES ########
##################################
ssh_options[:keys] = ["~/.ssh/teecommerce.pem"]
server "75.101.148.14", :web, :app, :db, primary: true
set :ngnix_conf_file_loc, "newzealand/nginx.conf" # location of nginx conf file
set :unicorn_init_file_loc, "newzealand/unicorn_init.sh" # location of unicor init shell file
set :github_account_name, "killersalesdesign" # name of accout on git hub
set :github_repo_name, "fantees" # name of git hub repo
set :git_branch_name, "master" # name of branch to deploy
set :rails_env, "newzealand" # name of environment: production, staging, ...
##################################