##################################
##### SET THESE VARIABLES ########
##################################
ssh_options[:keys] = ["~/.ssh/ksd-us.pem"]
server "54.225.193.43", :web, :app, :db, primary: true
set :ngnix_conf_file_loc, "production/nginx.conf" # location of nginx conf file
set :unicorn_init_file_loc, "production/unicorn_init.sh" # location of unicor init shell file
set :github_account_name, "killersalesdesign" # name of accout on git hub
set :github_repo_name, "fantees" # name of git hub repo
set :git_branch_name, "master" # name of branch to deploy
set :rails_env, "production" # name of environment: production, staging, ...
##################################