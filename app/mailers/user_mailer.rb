include ActionView::Helpers::NumberHelper

class UserMailer < ActionMailer::Base
  default from: "support@fantees.com.au"

  def send_on_order(id, password)
    @password = password
    @order   = Order.find(id)
    @discount = @order.coupon_id.nil? ? 0 : @order.coupon.discount
    @image   = @order.product_image
    @product = @order.product
    to       = @order.user.email
    # from     = @order.website.support_mail
    from = "support@fantees.com.au"

    mail(:to => to, from: from, :subject => "[FanTees] Order ID #{@order.order_id}")
  end

  def on_purchase(id, items, password)
    @password = password
    @order    = Order.find(id)
    parse_items(items)
    @product  = @order.product
    @website  = @product.website
    to        = @order.user.email

    @giftcards = Giftcard.where(:order_id => @order.id)
    # @delivery_date = @product.date + 14.days
    @delivery_date = @product.date + 9.days
    if @product.status == 2
      delivery = DeliveryService.new(2, Time.now)
      @delivery_date = delivery.bussiness_days
    elsif @product.status == 1
      @delivery_date = Time.now.beginning_of_day + 12.days
    end

    if @product.status == 2
      @international = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 4.days
    elsif @product.status == 1
      @international = Time.now.beginning_of_day + 14.days
      # @domestic      = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 9.days
    else
      @international = @product.date + 14.days
      # @domestic      = @product.date + 10.days
      @domestic      = @product.date + 9.days
    end

    mail(:to => to, :from => "Fantees Family <support@fantees.com.au>", :subject => "[FanTees] Order ID #{@order.order_id}")
  end

  def on_purchase_giftcard(id, items, password)
    @password = password
    @order    = Order.find(id)
    parse_items(items)
    @product  = @order.product
    @website  = @product.website
    to        = @order.user.email

    @giftcards = Giftcard.where(:order_id => @order.id)
    # @delivery_date = @product.date + 14.days
    @delivery_date = @product.date + 9.days
    if @product.status == 2
      delivery = DeliveryService.new(2, Time.now)
      @delivery_date = delivery.bussiness_days
    elsif @product.status == 1
      @delivery_date = Time.now.beginning_of_day + 12.days
    end

    if @product.status == 2
      @international = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 4.days
    elsif @product.status == 1
      @international = Time.now.beginning_of_day + 14.days
      # @domestic      = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 9.days
    else
      @international = @product.date + 14.days
      # @domestic      = @product.date + 10.days
      @domestic      = @product.date + 9.days
    end

    mail(:to => to, :from => "Fantees Family <support@fantees.com.au>", :subject => "[FanTees] Order ID #{@order.order_id}")
  end

  def send_on_print(id)
    @order = Order.find(id)
    to     = @order.user.email
    from   = @order.website.support_mail

    mail(:to => to, from: from, :subject => "[#{@order.website.name}] Order ID #{@order.order_id} is now printing")
  end

  def send_on_shipping(id)
    @order = Order.find(id)
    to     = @order.user.email
    from     = @order.website.support_mail

    mail(:to => to, from: from, :subject => "[#{@order.website.name}] Order ID #{@order.order_id} is now being shipped")
  end

  def send_error_logs(to, message)
    @logs = message

    mail(:to => to, :subject => "[FanTees] Error logs")
  end

  def send_duplicate_order(order)
    @order = order
    @user = @order.user
    mail(:to => @user.email, :from => "Fantees Family <support@fantees.com.au>", :subject => "Your FanTees Order")
  end

  def unfinished_order_30_minutes(unfinished_order_id)
    return false if unfinished_order_id.nil?

    @order = UnfinishedOrder.find(unfinished_order_id)

    if is_items_active(@order)
      if @order and @order.settings(:cart).user_email != ""
        @first_name = "hey"
        @rebuild_url = "#{root_url}cart/rebuild/#{@order.tracking_id}?time_frame=30m"
        if @order.settings(:cart).first_name != ""
          @first_name = @order.settings(:cart).first_name.humanize
        end

        mail(:to => @order.settings(:cart).user_email, :from => "Fantees Family <support@fantees.com.au>", :subject => "Your Items Await...")
      end
    end
  end

  def unfinished_order_24_hours(unfinished_order_id)
    return false if unfinished_order_id.nil?

    @order = UnfinishedOrder.find(unfinished_order_id)

    if is_items_active(@order)
      if @order and @order.settings(:cart).user_email != ""
        @first_name = "hey"
        @rebuild_url = "#{root_url}cart/rebuild/#{@order.tracking_id}?time_frame=1d"
        if @order.settings(:cart).first_name != ""
          @first_name = @order.settings(:cart).first_name.humanize
        end

        mail(:to => @order.settings(:cart).user_email, :from => "Fantees Family <support@fantees.com.au>", :subject => "I've Saved Your Cart [Discount Inside]")
      end
    end
  end

  def unfinished_order_10_days(unfinished_order_id)
    return false if unfinished_order_id.nil?

    @order = UnfinishedOrder.find(unfinished_order_id)

    if @order and @order.settings(:cart).user_email != ""
      @first_name = "hey"
      @rebuild_url = "#{root_url}cart/rebuild/#{@order.tracking_id}?time_frame=10d"
      if @order.settings(:cart).first_name != ""
        @first_name = @order.settings(:cart).first_name.humanize
      end

      mail(:to => @order.settings(:cart).user_email, :from => "Fantees Family <support@fantees.com.au>", :subject => "New Designs Just Released")
    end
  end

  private

  def is_items_active(order)
    isActive = false
    order.settings(:cart).items.each do |i|
      product = Product.find(i[:product_id])
      if product.active
        isActive = true
      end
    end

    isActive
  end

  def parse_items(items)
    @items     = []
    @total_qty = 0
    @subtotal  = 0
    @shipping  = 0
    @coupon    = @order.coupon
    domestic   = "AU"
    cutoff     = 0
    add_cost   = 0
    free_shipping = 0

    items.each do |i|
      item     = {}
      product  = Product.find(i[:product_id])
      category = Category.find(i[:category_id])
      info     = product.price_and_profit_of_style(i[:category_id])
      discount = @coupon.nil? ? 0 : @coupon.discount
      amount   = info[:amount].to_f - (info[:amount].to_f * (discount.to_f / 100))

      free_shipping = product.free_shipping > free_shipping ? product.free_shipping : free_shipping

      @total_qty += i[:quantity].to_i
      @subtotal  += i[:quantity].to_f * amount.to_f

      # shipping cost
      domestic     = product.website.name == "FanTees-NZ" ? "NZ" : "AU"
      shpping_info = JSON.parse(@order.shipping_info)
      if shpping_info["Country"] != domestic
        @shipping = info[:i_postage].to_f if info[:i_postage] && info[:i_postage] > @shipping
        cutoff    = product.international_cutoff > cutoff ? product.international_cutoff : cutoff
        add_cost  = product.additional_item_cost.to_f > add_cost.to_f ? product.additional_item_cost : add_cost
      else
        @shipping = info[:postage].to_f if info[:postage] && info[:postage] > @shipping
        cutoff    = product.free_shipping > cutoff ? product.free_shipping : cutoff
      end
      # end shipping cost

      @shipping = free_shipping > @total_qty ? @shipping : 0

      # results
      item[:name]     = product.title
      item[:category] = category.name
      item[:size]     = i[:size]
      item[:amount]   = amount
      item[:quantity] = i[:quantity].to_i
      item[:image]    = ProductImage.where(:product_id => i[:product_id], :category_id => i[:category_id]).first.product_image(:medium)

      @items << item
    end
  end
end