class CartCheckout
  def initialize(order)
    @order  = order
    @errors = []

    # @website   = Product.find(order.items.first[:product_id]).website
    @website = Website.find_by_name(ENV['APP_NAME'])
    @coupon    = Coupon.find_by_code @order.coupon_code
    @giftcard  = Giftcard.find_by_key_code @order.giftcard_code
    @order_id  = Order.generate_order_id
    @items     = []
    @shipping  = 0
    @subtotal  = 0
    @total_qty = 0
    @description = []
    initialize_items
  end

  def paypal_checkout_url(options)
    options  = options.reverse_merge(paypal_checkout_options)
    response = EXPRESS_GATEWAY.setup_purchase(@total.round, options)
    if @errors.empty?
      EXPRESS_GATEWAY.redirect_url_for(response.token)
    else
      @errors
    end
  end

  def paypal_purchase?(options)
    if @errors.empty?
      options = options.reverse_merge(paypal_checkout_options)
      response = EXPRESS_GATEWAY.purchase(@total.round, options)
      response.success?
    else
      @errors
    end
  end

  def eway_purchase?(customer_id)
      gateway   = ActiveMerchant::Billing::EwayGateway.new(:login => customer_id)
      gateway.purchase(@total.to_i, credit_card, eway_checkout_options)
  end

  def create_user_and_order(params = {}, ip, eway)
    unless eway
      details  = EXPRESS_GATEWAY.details_for params[:token]
      @user    = User.find_or_initialize_by_email details.params["payer"]
    else
      @user = User.find_or_initialize_by_email @order.email
    end
    @password = nil

    if @user.id.nil?
      @password                   = Devise.friendly_token[0,8]
      @user.password              = @password
      @user.password_confirmation = @password
      address                     = []

      if eway == false
        @user.first_name = details.params["first_name"]
        @user.last_name  = details.params["last_name"]
        address << details.params["street1"] unless details.params["street1"].blank?
        address << details.params["street2"] unless details.params["street2"].blank?
        address << details.params["city_name"] unless details.params["city_name"].blank?
        address << details.params["country"] unless details.params["country"].blank?
      else
        @user.first_name = @order.first_name
        @user.last_name  = @order.last_name
        address << @order.address
        address << @order.city
        address << @order.state
        address << @order.country
        address << @order.zip
      end
      @user.address = address.join(", ")
    end

    if @user.save
      @order.user_id    = @user.id
      @order.product_id = @order.items.first[:product_id]
      @order.token      = params[:token]
      @order.payer_id   = params[:PayerID]
      @order.ip_address = ip
      @order.order_id   = @order_id
      @order.website_id = @website.id
      @order.coupon_id  = @coupon.nil? ? nil : @coupon.id
      @order.giftcard_id  = @giftcard.nil? ? nil : @giftcard.id
      @order.shipping_info = eway == false ? details.params["PaymentDetails"]["ShipToAddress"].to_json : paypal_shipping_format.to_json
      @order.quantity   = 0

      if @order.save
        @order.items.each_with_index do |item, index|
          ordered_products = @order.ordered_products.new(
            :product_id  => item[:product_id],
            :category_id => item[:category_id],
            :size        => item[:size],
            :quantity    => item[:quantity]
          )

          product = Product.find(item[:product_id])
          if product.gift_card == true
            (1..item[:quantity].to_i).each do |i|
              @order.giftcards.new(
                :is_used => false,
                :key_code => SecureRandom.hex[0..10].upcase,
                :product_id => item[:product_id],
                :amount => item[:price].to_i,
                :active => true
              ).save
            end
          end

          if ordered_products.save
            # UserMailer.delay.on_purchase(@order.id, @order.items, @password) if (index + 1) == @order.items.count
            # UserMailer.on_purchase(@order.id, @order.items, @password).deliver if (index + 1) == @order.items.count
            @order.quantity += item[:quantity].to_i
          end
        end

        @order.save
        @order
      else
        @errors << "Something went wrong. Please try again."
      end
    else
      @errors << "Something went wrong. Please try again."
    end
  end



  def gc_create_user_and_order(params = {}, ip)
    puts "------bbbbbbb------", params["first_name"]
    @user = User.find_or_initialize_by_email params[:email]

    @password = nil

    shippingInfo = {
      Name: "#{params["first_name"]} #{params["last_name"]}",
      CountryName: params["country"],
      Country: params["country"],
      Street1: params["address"],
      CityName: params["city"],
      StateOrProvince: params["state"],
      Zip: params["zip"],
      Phone: params["phone"]

    }

    if @user.id.nil?
      @password                   = Devise.friendly_token[0,8]
      @user.password              = @password
      @user.password_confirmation = @password
      address                     = []

      @user.first_name = params["first_name"]
      @user.last_name  = params["last_name"]
      # shippingInfo['Name'] >> "#{params["first_name"]} #{params["last_name"]}"
      address << params["city"] unless params["city"].blank?
      address << params["address"] unless params["address"].blank?
      address << params["country"] unless params["country"].blank?
      address << params["zip"] unless params["zip"].blank?
      address << params["state"] unless params["state"].blank?
      address << params["phone"] unless params["phone"].blank?
      @user.address = address.join(", ")

    end

    if @user.save
      @order.user_id    = @user.id
      @order.product_id = @order.items.first[:product_id]
      @order.token      = params[:token]
      @order.payer_id   = params[:PayerID]
      @order.ip_address = ip
      @order.order_id   = @order_id
      @order.website_id = @website.id
      @order.coupon_id  = @coupon.nil? ? nil : @coupon.id
      @order.giftcard_id  = @giftcard.nil? ? nil : @giftcard.id
      @order.shipping_info = shippingInfo.to_json
      @order.quantity   = 0

      if @order.save
        @order.items.each_with_index do |item, index|
          ordered_products = @order.ordered_products.new(
            :product_id  => item[:product_id],
            :category_id => item[:category_id],
            :size        => item[:size],
            :quantity    => item[:quantity]
          )

          product = Product.find(item[:product_id])
          if product.gift_card == true
            (1..item[:quantity].to_i).each do |i|
              @order.giftcards.new(
                :is_used => false,
                :key_code => SecureRandom.hex[0..10].upcase,
                :product_id => item[:product_id],
                :amount => item[:price].to_i,
                :active => true
              ).save
            end
          end

          if ordered_products.save
            # UserMailer.delay.on_purchase(@order.id, @order.items, @password) if (index + 1) == @order.items.count
            # UserMailer.on_purchase(@order.id, @order.items, @password).deliver if (index + 1) == @order.items.count
            @order.quantity += item[:quantity].to_i
          end
        end

        @order.save
        @order
      else
        @errors << "Something went wrong. Please try again."
      end
    else
      @errors << "Something went wrong. Please try again."
    end
  end

  def errors
    @errors
  end

  def get_user
    @user
  end

  def get_password
    @password
  end

  def get_order
    @order
  end

  def delete_order
    @order.ordered_products.delete_all
    @order.delete
  end

  def restock
    @order.items.each do |item|
      product = Product.find(item[:product_id])
      if product.status != Product::CAMPAIGN
        stock = product.stocks.where(:size => item[:size], :category_id => item[:category_id]).first
        stock.increment(:amount, item[:quantity].to_i)
      end
    end
  end

  private

  def initialize_items
    domestic   = "AU"
    cutoff     = 0
    add_cost   = 0

    @order.items.each do |i|
      product  = Product.find(i[:product_id])
      category = Category.find(i[:category_id])
      info     = product.price_and_profit_of_style(i[:category_id])
      discount = @coupon.nil? ? 0 : @coupon.discount
      discount_giftcard = @giftcard.nil? ? 0 : @giftcard.amount
      amount   = info[:amount].to_f - (info[:amount].to_f * (discount.to_f / 100))

      puts "discount", discount_giftcard
      if amount <= discount_giftcard
        amount   = 0
      else
       amount = amount - discount_giftcard
      end

      @items << {:name => product.title, :quantity => i[:quantity].to_i, :description => "#{i[:size]} #{category.name}", :amount => (amount * 100)}
      @description << "#{i[:quantity]} #{i[:size]} #{category.name}"

      @total_qty += i[:quantity].to_i
      @subtotal  += i[:quantity].to_f * amount.to_f

      # shipping cost
      domestic = product.website.name == "FanTees-NZ" ? "NZ" : "AU"
      if @order.country != domestic
        @shipping = info[:i_postage].to_f if info[:i_postage] && info[:i_postage] > @shipping
        cutoff    = product.international_cutoff > cutoff ? product.international_cutoff : cutoff
        add_cost  = product.additional_item_cost.to_f > add_cost.to_f ? product.additional_item_cost : add_cost
      else
        @shipping = info[:postage].to_f if info[:postage] && info[:postage] > @shipping
        cutoff    = product.free_shipping > cutoff ? product.free_shipping : cutoff
      end
      # end shipping cost

      # check_stocks(product, i) if product.status != Product::CAMPAIGN

      #check product stocks if product is in stocks
      if product.status != Product::CAMPAIGN
        order    = product.stocks.where(:category_id => i[:category_id], :size => i[:size]).first
        category = Category.find(i[:category_id])
        order.amount = 0 if order.amount.nil?

        if order.amount < i[:quantity].to_i
          error    = {product_id: product.id, size: i[:size], category_id: i[:category_id].to_i, message: "#{category.name} for #{product.title}, size '#{i[:size]}' is out of stock"}
          @errors << "#{category.name} for #{product.title}, size '#{i[:size]}' is out of stock"
        end
      end
    end

    if @order.country != domestic && @total_qty < cutoff
      @shipping += add_cost.to_f * (@total_qty - 1).to_f
    end

    @shipping = 0 if @total_qty >= cutoff
    @shipping = @shipping * 100
    @subtotal = @subtotal * 100
    @total    = (@shipping + @subtotal)
  end

  def check_stocks(product, item)
    order    = product.stocks.where(:category_id => item[:category_id], :size => item[:size]).first
    category = Category.find(item[:category_id])
    if order.amount < item[:quantity].to_i
      error    = {product_id: product.id, size: item[:size], category_id: item[:category_id].to_i, message: "#{category.name} for #{product.title}, size '#{item[:size]}' is out of stock"}
      @errors << "#{category.name} for #{product.title}, size '#{item[:size]}' is out of stock"
    end
  end

  def paypal_checkout_options
    {
      :subtotal          => @subtotal.round,
      :shipping          => @shipping.round,
      :handling          => 0,
      :tax               => 0,
      :items             => @items,
      :currency          => 'AUD',
    }
  end

  def eway_checkout_options
    {
      :subtotal => @subtotal * 100,
      :shipping => @shipping * 100,
      :handling => 0,
      :tax      => 0,
      :order_id => @order_id,
      :email    => @order.email,
      :address => {
        :address1 => @order.address,
        :city     => @order.city,
        :state    => @order.state,
        :country  => @order.country,
        :zip      => @order.zip
      },
      :shipping_address => {
        :address1 => @order.address,
        :city     => @order.city,
        :state    => @order.state,
        :country  => @order.country,
        :zip      => @order.zip
      },
      :description => @description.join(", ")
    }
  end

  def paypal_shipping_format
    {
      :Name            => "#{@order.first_name.humanize} #{@order.last_name.humanize}",
      :Street1         => @order.address,
      :CityName        => @order.city,
      :StateOrProvince => @order.state,
      :Country         => @order.country,
      :CountryName     => @order.country,
      :PostalCode      => @order.zip,
      :Phone           => @order.phone
    }
  end

  def credit_card
    ActiveMerchant::Billing::CreditCard.new(
      :number     => @order.cc_number,
      :month      => @order.month.to_i,
      :year       => @order.year.to_i,
      :first_name => @order.first_name,
      :last_name  => @order.last_name,
      :verification_value => @order.cvn
    )
  end
end