class DeliveryService
  def initialize(days, date)
    @days = days
    @date = date
  end

  def bussiness_days
    ctr = 0
    if ctr < @days
      while ctr < @days  
        @date += 1.day    
        unless @date.saturday? or @date.sunday?
          ctr += 1
        end
      end
    end
    return @date
  end
end