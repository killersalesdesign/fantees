class EwayCheckout
  def initialize(params)
    @params = params
    # last_name  = params[:name].split(" ").last
    # first_name = params[:name].gsub(last_name, "").strip
    @creditcard = ActiveMerchant::Billing::CreditCard.new(
      :number     => params[:cc_number],
      :month      => params[:month].to_i,
      :year       => params[:year].to_i,
      :first_name => params[:first_name],
      :last_name  => params[:last_name],
      :verification_value => params[:cvn]
    )

    @product   = Product.find(params[:product_id])
    order_id   = SecureRandom.hex[0..7].upcase

    @subtotal = 0
    @shipping = 0
    key       = 0
    @items    = []
    desc      = []
    quantity  = 0
    add_charge = 0

    if params[:cat_ids]
      params[:cat_ids].each do |cat_id|
        size  = params[:size][key]
        style = Category.find(cat_id)
        info  = @product.price_and_profit_of_style(cat_id)
        quantity  += params[:qty][key].to_i
        @subtotal += params[:qty][key].to_i * info[:amount].to_f
        postage   = params[:country] != "AU" ? info[:i_postage] : info[:postage]
        @shipping = postage.to_f if postage && postage > @shipping
        # @shipping = info[:postage].to_f if info[:postage] && info[:postage] > @shipping

        # if params[:qty][key].to_i > 1
        #   additional  = (params[:qty][key].to_i - 1) * info[:additional_item]
        #   @shipping += additional
        # end

        @items << {:category_id => cat_id, :quantity => params[:qty][key].to_i, :size => size, :price => info[:amount]}
        desc << "#{params[:qty][key].to_i} #{size} #{style.name}"

        key += 1
      end
    else
      params[:orders][:qty].each do |o|
        category_id = params[:orders][:styles][key]
        size        = params[:orders][:sizes][key]
        style       = Category.find(category_id)
        info        = @product.price_and_profit_of_style(category_id)
        quantity   += o.to_i
        @subtotal  += o.to_i * info[:amount].to_f

        postage   = params[:country] != "AU" ? info[:i_postage] : info[:postage]
        @shipping = postage.to_f if postage && postage > @shipping
        # @shipping   = info[:postage].to_f if info[:postage] && info[:postage] > @shipping

        # if o.to_i > 1
        #   additional  = (o.to_i - 1) * info[:additional_item]
        #   @shipping += additional
        # end

        @items << {:category_id => style.id, :quantity => o.to_i, :size => size, :price => info[:amount]}
        desc << "#{o} #{size} #{style.name}"
        key += 1
      end
    end

    cut_off    = params[:country] != "AU" ? @product.international_cutoff : @product.free_shipping
    @shipping += (quantity - 1) * @product.additional_item_cost if params[:country] != "AU" && quantity > 1
    @shipping  = cut_off > quantity ? @shipping : 0

    unless @params[:coupon].blank?
      coupon    = Coupon.find_by_code @params[:coupon]
      @subtotal = @subtotal.to_f - (@subtotal.to_f * (coupon.discount.to_f / 100)) unless coupon.nil?
    end

    @total     = (@subtotal * 100) + (@shipping * 100)

    @options = {
      :subtotal => @subtotal * 100,
      :shipping => @shipping * 100,
      :handling => 0,
      :tax      => 0,
      :order_id => order_id,
      :email    => params[:email],
      :address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :shipping_address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :description => desc.join(", ")
    }
  end

  def paypal_shipping_format
    pp_format = {
      :Name            => "#{@params[:first_name].humanize} #{@params[:last_name].humanize}",
      :Street1         => @params[:address],
      :CityName        => @params[:city],
      :StateOrProvince => @params[:state],
      :Country         => @params[:country],
      :CountryName     => @params[:country],
      :PostalCode      => @params[:zip],
      :Phone           => @params[:phone]
    }
    return pp_format
  end

  def get_order_id
    @options[:order_id]
  end

  def get_product
    @product
  end

  def get_items
    @items
  end

  def checkout(customer_id)
    gateway  = ActiveMerchant::Billing::EwayGateway.new(:login => customer_id)
    response = gateway.purchase(@total.to_i, @creditcard, @options)

    return response
  end

  def check_orders
    product = Product.find @params[:product_id]
    result  = true
    if product.status != Product::CAMPAIGN
      @items.each do |o|
        order = product.stocks.where(:category_id => o[:category_id].to_i, :size => o[:size]).first
        if order.amount < o[:quantity]
          result = false
        end
      end
    end

    result
  end

end