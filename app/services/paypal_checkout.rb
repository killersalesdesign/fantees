class PaypalCheckout
  # def initialize(params, session)
  def initialize(params, orders)
    @params   = params
    @orders   = orders
    @order_id = SecureRandom.hex[0..7].upcase
    @details  = EXPRESS_GATEWAY.details_for(@params[:token])


    # @orders  = session[:orders]
    # @shipping_info = session[:shipping]
  end

  def orders
    @orders
  end

  def params
    @params
  end

  def user
    @user
  end

  def purchase?
    response = EXPRESS_GATEWAY.purchase(price_in_cents, express_purchase_options)
    response.success?
  end

  def create_order
    website  = Website.find_by_name ENV['APP_NAME']
    product  = Product.find(@params[:id])
    @user    = create_user

    @order   = @user.orders.new(
      :ip_address => @params[:ip],
      :product_id => @params[:id],
      :token      => @params[:token],
      :payer_id   => @params[:PayerID],
      :shipping_info => @details.params["PaymentDetails"]["ShipToAddress"].to_json,
      # :shipping_info => @shipping_info.to_json || @details.params["PaymentDetails"]["ShipToAddress"].to_json,
      :order_id   => @order_id,
      :website_id => website.id
    )

    if @order.save
      @order.quantity = 0
      @orders.each_with_index do |o, index|
        order = @order.ordered_products.new(
          :quantity => o[:quantity].to_i,
          :size => o[:size].strip,
          :category_id => o[:category_id].to_i,
          :product_id => @params[:id].to_i
        ).save

        if product.gift_card == true
          (1..o[:quantity].to_i).each do |i|
            @order.giftcards.new(
              :is_used => false,
              :key_code => SecureRandom.hex[0..10].upcase
            ).save
          end
        end

        @order.quantity += o[:quantity].to_i
      end

      if @order.save
        UserMailer.on_purchase(@order, @orders, @password).deliver
        # UserMailer.on_purchase(@order, @orders, @password).deliver
        @order
      end
    end
  end

  # def create_order
  #   @details = EXPRESS_GATEWAY.details_for(@params[:token])

  #   if @details.params["CheckoutStatus"] == "PaymentActionCompleted"
  #     create_user

  #     website = Website.find_by_name ENV['APP_NAME']
  #     @order  = @user.orders.new(
  #       :ip_address => @params[:ip],
  #       :product_id => @params[:id],
  #       :token      => @details.params["token"],
  #       :payer_id   => @details.params["payer_id"],
  #       :shipping_info => @details.params["PaymentDetails"]["ShipToAddress"].to_json,
  #       # :shipping_info => @shipping_info.to_json || @details.params["PaymentDetails"]["ShipToAddress"].to_json,
  #       :order_id   => SecureRandom.hex[0..7].upcase,
  #       :website_id => website.id
  #     )

  #     if @order.save
  #       # orders_session = session[:orders]
  #       # session.delete(:orders)

  #       @orders.each_with_index do |o, index|
  #         order = @order.ordered_products.new(
  #           :quantity => o[:quantity],
  #           :size => o[:size].strip,
  #           :category_id => o[:category_id],
  #           :product_id => @params[:id]
  #         ).save

  #         # if order.save
  #         #   # @order.on_purchase(@password, order) if index == @orders.count + 1
  #         #   UserMailer.delay.on_purchase(@order.id, order, @password) if (index + 1) == @orders.count
  #         # end
  #       end

  #       @order.quantity = @order.ordered_products.sum(:quantity)
  #       @order.save

  #       # @order.send_order_mail(@password)
  #       UserMailer.delay.on_purchase(@order, @orders, @password)
  #     end
  #   else
  #     @order = false
  #   end

  #   @order
  # end

  def check_orders
    product = Product.find @params[:id]
    result  = true
    if product.status != Product::CAMPAIGN
      @orders.each do |o|
        order = product.stocks.where(:category_id => o[:category_id], :size => o[:size]).first
        if order.amount < o[:quantity]
          result = false
        end
      end
    end

    result
  end

  private

  def create_user
    user      = User.find_or_initialize_by_email(@details.params["payer"])
    @password = nil

    if user.id.nil?
      @password                  = Devise.friendly_token[0,8]
      user.password              = @password
      user.password_confirmation = @password
      user.first_name            = @details.params["first_name"]
      user.last_name             = @details.params["last_name"]
      address                     = []

      address << @details.params["street1"] unless @details.params["street1"].blank?
      address << @details.params["street2"] unless @details.params["street2"].blank?
      address << @details.params["city_name"] unless @details.params["city_name"].blank?
      address << @details.params["country"] unless @details.params["country"].blank?
      user.address = address.join(", ")

      user.save
    end

    user
  end


  def price_in_cents
    @subtotal = 0
    @shipping = 0
    quantity  = 0
    product   = Product.find(@params[:id])
    @orders.each do |p|
      info       = product.price_and_profit_of_style(p[:category_id])
      @shipping  = info[:postage].to_f if info[:postage] && info[:postage] > @shipping
      @subtotal += p[:quantity] * info[:amount]
      quantity  += p[:quantity]
    end
    @shipping = product.free_shipping > quantity ? @shipping : 0
    (@subtotal * 100).round + (@shipping * 100).round
  end

  def express_purchase_options
    {
      :subtotal => (@subtotal * 100).round,
      :shipping => (@shipping * 100).round,
      :handling => 0,
      :tax      => 0,
      :ip       => @params[:ip],
      :currency => 'AUD',
      :token    => @params["token"],
      :payer_id => @params["PayerID"]
    }
  end
end