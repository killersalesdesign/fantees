class Dashboard::OrdersController < Dashboard::BaseController
  def index
    @orders = current_user.search_orders(params[:search])
    @orders = @orders.limit(params[:limit]) unless params[:limit].blank?
  end

  def show
    @order = current_user.orders.find_by_order_id(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    @order.update_attributes(description: params[:description], shipping_info: params[:shipping_info].to_json)
  end

  def ordered_products
    @ordered_products = OrderedProduct.where(:order_id => params[:id])
  end

  def latest
    @products = @website.products.my_search(nil, false, false).limit(4)
  end
end
