class Dashboard::GiftcardsController < Dashboard::BaseController
  def index
    @giftcards = current_user.giftcards
    render :json => @giftcards
  end

  def show
    @giftcard = current_user.giftcards.find_by_giftcard_id(params[:id])
    render :json => @giftcard
  end


end
