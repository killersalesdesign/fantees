class CartController < ApplicationController
  layout "products"
  def index
    session.delete(:coupon)
    session.delete(:giftcard)

    @order          = Order.new
    @errors         = []
    gon.website     = @website
    @title          = "Cart | FanTees"
    @has_giftcards  = false
    # gon.order   = order_fields
    # gon.payment = "PP"

    unfinished_orders unless session[:rebuild]
  end

  def creditcard
    if giftcard_total_zero() == true
      redirect_to giftcards_cart_index_path
    else
      @order      = Order.new
      @errors     = []
      gon.website = @website
      gon.order   = order_fields
      gon.payment = "PP"
      gon.coupon  = Coupon.find_by_code session[:coupon]
      gon.giftcard  = Giftcard.find_by_key_code session[:giftcard]
      @title      = "Secure Checkout | FanTees"
    end
    # render :json => session
  end

  def rebuild
    order = UnfinishedOrder.find_by_tracking_id(params[:tracking_id])

    # add mail tracker for email clicks
    if params[:time_frame]
      case params[:time_frame]
      when "30m"
        time_frame = "30-minutes"
      when "1d"
        time_frame = "1-day"
      when "10d"
        time_frame = "10-days"
      end
      MailTracker.increment_count("click", time_frame)
      session[:rebuild_time_frame] = time_frame
    end

    # check if order is this available
    if order.nil?
      redirect_to root_url
    else
      # rebuild the cart here
      cart_items = []
      order.settings(:cart).items.each do |item|
        product = Product.find(item[:product_id])
        if product.active
          cart_items.push item
        end
      end

      @cart.delete_all

      @cart.create cart_items
      session[:unfinished_order_tracking_id] = order.tracking_id

      session[:rebuild] = true

      # redirect here
      redirect_to cart_index_url
    end
  end

  def create
    @cart.create params[:cart][:items]
    render :json => {items: @cart.all}
  end

  def update
    @cart.update_attributes(params[:id], params[:cart])
    items = @cart.all
    render :json => {items: items, shipping: @cart.shipping_fee, delivery_date: @cart.delivery_date, total_qty: @cart.quantity}
  end

  def destroy
    @cart.delete params[:id]
    items = @cart.all
    render :json => {items: items, shipping: @cart.shipping_fee, delivery_date: @cart.delivery_date, total_qty: @cart.quantity}
  end

  def all
    items = @cart.all
    begin
      unfinished_order              = UnfinishedOrder.find_by_tracking_id(session[:unfinished_order_tracking_id])
      unfinished_cart               = unfinished_order.settings(:cart)
      checkout_details              = {}
      checkout_details[:email]      = unfinished_cart.user_email
      checkout_details[:first_name] = unfinished_cart.first_name
      checkout_details[:last_name]  = unfinished_cart.last_name
      checkout_details[:address]    = unfinished_cart.address
      checkout_details[:town]       = unfinished_cart.town
      checkout_details[:state]      = unfinished_cart.state
      checkout_details[:postal]     = unfinished_cart.postal
      checkout_details[:country]    = unfinished_cart.country
      checkout_details[:phone]      = unfinished_cart.phone
    rescue
      checkout_details = {}
    end

    render :json => {unfinished_order: checkout_details, items: items, shipping: @cart.shipping_fee, delivery_date: @cart.delivery_date, total_qty: @cart.quantity}
  end

  def checkout
    unless session[:items].count > 0
      redirect_to cart_index_path
    else
      order       = get_order(params)
      @order      = Order.new(order)
      gon.website = @website
      # gon.order   = params[:order]
      gon.payment = params[:payment]
      if giftcard_total_zero() == true
        redirect_to giftcards_cart_index_path
      else
        if params[:payment] == "PP"
          express_paypal
        else
          # render :json => order
          eway_creditcard
        end
      end
    end
  end

  def paypal_express
    unless session[:items].nil? && session['_csrf_token'] == params[:session_token]
      order  = get_order
      @order = Order.new(order)
      cart   = @order.cart

      if cart.errors.empty?
        order  = cart.create_user_and_order(params, request.remote_ip, false)

        if order
          response = cart.paypal_purchase?(
            :ip       => request.remote_ip,
            :token    => params["token"],
            :payer_id => params["PayerID"]
          )

          if response == true
            update_coupon_usage if session[:coupon]
            if session[:giftcard]
              totalAmount = 0
              session[:items].each do |i|
                totalAmount += i[:price].to_f
              end
              update_giftcard_usage(totalAmount, cart.get_user.id)
            end
            # send mail
            if @has_giftcards == true
              UserMailer.on_purchase_giftcard(order.id, cart.get_order.items, cart.get_password).deliver
            else
              UserMailer.on_purchase(order.id, cart.get_order.items, cart.get_password).deliver
            end

            session.delete(:coupon)
            session.delete(:giftcard)
            session.delete(:order)
            @cart.delete_all

            sign_in(:user, cart.get_user)
            # order.send_order_mail(cart.get_password)

            redirect_to order_url(order.order_id)
          else
            cart.delete_order
            redirect_to cart_index_path(:error => true)
          end
        end
      else
        @errors = cart.errors
        render "index"
      end
    else
      redirect_to cart_index_path(:error => true)
    end
  end

  def giftcards
    if giftcard_total_zero() == true
      @order      = Order.new
      @errors     = []
      gon.website = @website
      gon.order   = order_fields
      gon.payment = "PP"
      gon.coupon  = Coupon.find_by_code session[:coupon]
      gon.giftcard  = Giftcard.find_by_key_code session[:giftcard]
      @title      = "Secure Checkout | FanTees"
    else
      redirect_to cart_index_path
    end
  end

  def giftcard_express
    if giftcard_total_zero() == true
      order  = get_order
      @order = Order.new(order)
      cart   = @order.cart

      cart  = @order.cart
      order = params[:order]
      order.delete(:cc_number)
      order.delete(:cvn)


      if cart.errors.empty?
        order  = cart.gc_create_user_and_order(params[:order], request.remote_ip)

        if session[:giftcard]
          totalAmount = 0
          session[:items].each do |i|
            totalAmount += i[:price].to_f
          end
          update_giftcard_usage(totalAmount, cart.get_user.id)
        end
        # send mail
        if @has_giftcards == true
          UserMailer.on_purchase_giftcard(order.id, cart.get_order.items, cart.get_password).deliver
        else
          UserMailer.on_purchase(order.id, cart.get_order.items, cart.get_password).deliver
        end

        session.delete(:coupon)
        session.delete(:giftcard)
        session.delete(:order)
        @cart.delete_all

        sign_in(:user, cart.get_user)
        redirect_to order_url(order.order_id)
      end
    else
      redirect_to cart_index_path
    end


  end

  private

  def get_order(params = {})
    order = {}
    order[:items]   = []
    order[:coupon_code] = session[:coupon] unless session[:coupon].blank?
    order[:giftcard_code] = session[:giftcard] unless session[:giftcard].blank?
    session[:items].each do |i|
      product = Product.find(i[:product_id])
      @has_giftcards = product.gift_card == true ? true : false
      order[:items] << {product_id: i[:product_id], category_id: i[:category_id], quantity: i[:quantity], size: i[:size], price: i[:price]}
    end

    order = params[:order].reverse_merge(order) unless params[:order].blank?
    order[:country] = @website.name == "FanTees-NZ" ? "NZ" : "AU" if order[:country].blank?

    order
  end

  def express_paypal
    # session[:order] = params[:order]
    cart = @order.cart
    express_checkout_url = cart.paypal_checkout_url(
      :ip                => request.remote_ip,
      :return_url        => paypal_express_cart_index_url(:session_token => session['_csrf_token']),
      :cancel_return_url => cart_index_url
    )

    if cart.errors.empty?
      redirect_to express_checkout_url
    else
      @errors = cart.errors
      render "index"
      # render :json => cart.errors
    end
  end

  def eway_creditcard
    if giftcard_total_zero() == true
      render "giftcards"
    else
      cart  = @order.cart
      order = params[:order]
      order.delete(:cc_number)
      order.delete(:cvn)
      order[:month] = 1.month.from_now.strftime("%m").to_i
      order[:year]  = 1.month.from_now.strftime("%Y").to_i
      gon.order = order

      # render :json => @cart.all
      if cart.errors.empty?
        order = cart.create_user_and_order({:token => "", :PayerID => ""}, request.remote_ip, true)

        response = cart.eway_purchase?(ENV['EWAY_COSTUMER_ID'])
        if response.success?
          # render :json => cart.get_order.items
          order.token = response.authorization
          order.save

          # update coupon
          update_coupon_usage if session[:coupon]
          if session[:giftcard]
            totalAmount = 0
            session[:items].each do |i|
              totalAmount += i[:price].to_f
            end
            update_giftcard_usage(totalAmount, cart.get_user.id)
          end

          # send mail
          if @has_giftcards == true
            UserMailer.on_purchase_giftcard(order.id, cart.get_order.items, cart.get_password).deliver
          else
            UserMailer.on_purchase(order.id, cart.get_order.items, cart.get_password).deliver
          end
          session.delete(:coupon)
          session.delete(:giftcard)
          session.delete(:order)
          @cart.delete_all

          sign_in(:user, cart.get_user)
          # order.send_order_mail(cart.get_password)

          redirect_to order_url(order.order_id)
        else
          cart.restock
          cart.delete_order

          @errors = cart.errors
          @errors << response.message
          render "creditcard"
        end
      else

        @errors = cart.errors
        render "creditcard"
      end
    end
  end

  def update_coupon_usage
    coupon = Coupon.find_by_code session[:coupon]
    coupon.increment!(:usage)
  end
  def update_giftcard_usage(amount, id)
    giftcard = Giftcard.find_by_key_code session[:giftcard]
    subtotal = giftcard.amount - amount
    if subtotal < 0
      giftcard.amount = 0
      giftcard.active = false
    else
      giftcard.amount = giftcard.amount - amount
    end
    giftcard.user_id = id
    giftcard.is_used = true
    giftcard.save
  end

  def order_fields
    {
      :email      => "",
      :first_name => "",
      :last_name  => "",
      :phone      => "",
      :address    => "",
      :city       => "",
      :phone      => "",
      :zip        => "",
      :country    => @website.name == "FanTees-NZ" ? "NZ" : "AU",
      :state      => "",
      :cc_number  => "",
      :month      => 1.month.from_now.strftime("%m").to_i,
      :year       => 1.month.from_now.strftime("%Y").to_i,
      :cvn        => ""
    }
  end

  def unfinished_orders
    # browse through all items
    # in the cart
    cart_items = []
    total_qty = 0
    @cart.all.each do |i|
      cart_items << i
      total_qty += i[:quantity]
    end

    # remove unfinished order
    # to generate a new one
    if session[:unfinished_order_tracking_id]
      UnfinishedOrder.remove_order(session[:unfinished_order_tracking_id])
    end

    # compute shipping fee here
    shipping = @cart.shipping_fee
    cutoff = shipping[:cutoff]

    if total_qty >= cutoff
      shipping_fee = 0
    else
      shipping_fee = shipping[:domestic]
    end

    # generate new tracking id for
    # unfinished orders
    session[:unfinished_order_tracking_id] = SecureRandom.hex(10)

    begin
      # initialize new unfinished order
      unfinished_order = UnfinishedOrder.new

      # set data for unfinished orders
      unfinished_order.tracking_id = session[:unfinished_order_tracking_id]
      unfinished_order.website_id = 1 #fantees
      unfinished_order.settings(:cart).items = cart_items
      unfinished_order.settings(:cart).shipping_fee = shipping_fee
      unfinished_order.recovered = false
      unfinished_order.save!
    rescue
    end

    # remove scheduled jobs
    if session[:thirthy_minute] || session[:twenty_four_hours] || session[:ten_days]
      scheduled_jobs = Sidekiq::ScheduledSet.new
      jobs = scheduled_jobs.select {|i| i.jid == "#{session[:thirthy_minute]}" || i.jid == "#{session[:twenty_four_hours]}" || i.jid == "#{session[:ten_days]}"}
      jobs.each(&:delete)
    end

    # create job here to send email
    # 30 minute email
    session[:thirthy_minute] = UnfinishedOrderWorker.perform_in(30.minutes, unfinished_order.id, "thirthy_minutes")
    # session[:thirthy_minute] = UnfinishedOrderWorker.perform_in(1.minute, unfinished_order.id, "thirthy_minutes")

    # 24 hours after
    session[:twenty_four_hours] = UnfinishedOrderWorker.perform_in(1.day, unfinished_order.id, "twenty_four_hours")
    # session[:twenty_four_hours] = UnfinishedOrderWorker.perform_in(2.minutes, unfinished_order.id, "twenty_four_hours")

    # 10 days after
    session[:ten_days] = UnfinishedOrderWorker.perform_in(10.days, unfinished_order.id, "ten_days")
    # session[:ten_days] = UnfinishedOrderWorker.perform_in(3.minutes, unfinished_order.id, "ten_days")
  end

  def giftcard_total_zero
    if session[:giftcard]
      totalAmount = 0
      subtotal = 0
      total_qty = 0
      session[:items].each do |i|
        totalAmount += i[:price].to_f * i[:quantity]
        total_qty += i[:quantity]
      end

      shipping = @cart.shipping_fee
      cutoff = shipping[:cutoff]

      if total_qty >= cutoff
        shipping_fee = 0
      else
        shipping_fee = shipping[:domestic]
      end
      totalAmount = totalAmount + shipping_fee
      giftcard = Giftcard.find_by_key_code session[:giftcard]
      subtotal =  totalAmount - giftcard.amount
      puts "subtotal", totalAmount, giftcard.amount, subtotal
      if subtotal <= 0
        puts "awa true ni"
        return true
      else
        puts "awa false ni"
        return false
      end

    else
      return false
    end

  end
end
