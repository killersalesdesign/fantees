class GiftcardsController < ApplicationController
  def show
    @giftcard = Giftcard.find_by_key_code params[:code]
    save_to_session

    render :json => @giftcard
  end

  private

  def save_to_session
    if @giftcard.nil?
      # has error
      session.delete(:giftcard)
    elsif !@giftcard.active
      # has errors
      session.delete(:giftcard)
    elsif @giftcard.amount <= 0
      # has errors
      session.delete(:giftcard)
    else
      session[:giftcard] = @giftcard.key_code
    end
  end
end
