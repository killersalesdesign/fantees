class HomeController < ApplicationController
  def index
    @keyword  = params[:search] ? params[:search] : nil
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "Custom Designed Apparel | Fantees"

    @group1 = []
    @group2 = []
    @fb_og_image = []

    @products.order.each_with_index do |p, index|
      @fb_og_image << p.product_images.sample.product_image(:medium) if @fb_og_image.count <= 3 && !p.product_images.sample.nil?

      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def bundled
    @keyword  = params[:search] ? params[:search] : nil
    @products = @website.products.my_search(@keyword, true, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "Custom Designed Apparel | Fantees"

    @group1 = []
    @group2 = []
    @fb_og_image = []

    @products.order.each_with_index do |p, index|
      @fb_og_image << p.product_images.sample.product_image(:medium) if @fb_og_image.count <= 3 && !p.product_images.sample.nil?

      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def giftcards
    @keyword  = params[:search] ? params[:search] : nil
    @products = @website.products.my_search(@keyword, false, true).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "Custom Designed Apparel | Fantees"

    @group1 = []
    @group2 = []
    @fb_og_image = []

    @products.order.each_with_index do |p, index|
      @fb_og_image << p.product_images.sample.product_image(:medium) if @fb_og_image.count <= 3 && !p.product_images.sample.nil?

      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def search
    @keyword  = params[:search] ? params[:search] : nil
    # keyword   = params[:keyword]
    # @products = Product.algolia_search(@keyword).page(params[:page]).per(5)
    # @products = Product.algolia_search(@keyword).page(params[:page]).per(5)
    render :json => @products
  end
  
  # def fathersday
  #   @keyword  = params[:search] ? params[:search] : "fathers day"
  #   @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
  #   @fans     = @website.orders.where(:shipped => 1).count
  #   @rating   = @website.ratings.last
  #   @title    = "Fathers Day | Fantees"

  #   @group1 = []
  #   @group2 = []
  #   @fb_og_image = []

  #   @products.order.each_with_index do |p, index|
  #     @fb_og_image << p.product_images.sample.product_image(:medium) if @fb_og_image.count <= 3 && !p.product_images.sample.nil?

  #     if index < 8
  #       @group1 << p
  #     else
  #       @group2 << p
  #     end
  #   end
  # end

  def nurse_shirts
    page_keyword      = "nurse"
    page_title        = "Nurse Shirts | Custom Nurse Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Nurse Shirts for all the lovely nurses. Check them out now."
    @social_share_img = "/assets/custom_page_banners/nurse-shirt.png"
    @custom_img       = "custom_page_banners/nurses.png"
    @header_title     = "Nurse"
    @header_text      = "Check out these nurse shirts, hoodies, phone cases, mugs & more. Only available on FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def fishing_shirts
    page_keyword      = "fishing"
    page_title        = "Fishing Shirts | Custom Fishing Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Fishing Shirts for all enthusiasts. You wont find them anywhere else. Check them out now."
    @social_share_img = "/assets/custom_page_banners/fishing-shirt.png"
    @custom_img       = "custom_page_banners/fishing.png"
    @header_title     = "Fishing"
    @header_text      = "Check out these awesome fishing shirts, hoodies, hats & more."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def mechanic_shirts
    page_keyword      = "mechanic"
    page_title        = "Mechanic Shirts | Custom Mechanic Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Mechanic Shirts. One for every humour style. Find yours now."
    @social_share_img = "/assets/custom_page_banners/mechanic-shirt.png"
    @custom_img       = "custom_page_banners/mechanic.png"
    @header_title     = "Mechanic"
    @header_text      = "Awesome Mechanic shirts, hoodies, hats & more. Only available on FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def sprint_car_shirts
    page_keyword      = "sprint car"
    page_title        = "Sprint Car Shirts | Custom Sprint Car Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Sprint Car shirts for all fans. See our range of designs here."
    @social_share_img = "/assets/custom_page_banners/sprint-car-shirt.png"
    @custom_img       = "custom_page_banners/sprint-car.png"
    @header_title     = "Sprint Car"
    @header_text      = "Shirts, hoodies, mugs & more for every sprint car fan. Available at FanTees only."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def firefighter_shirts
    page_keyword      = "firefighting"
    page_title        = "Firefighter Shirts | Custom Firefighter Apparel"
    @customize_page   = true
    @meta_desc        = "Firefighter shirts for volunteers and the like. Unique designs available here only."
    @social_share_img = "/assets/custom_page_banners/firefighter-shirt.png"
    @custom_img       = "custom_page_banners/fire-fighter.png"
    @header_title     = "Fire Fighter"
    @header_text      = "Check out our awesome Firefighters shirts, hoodies & more.  Find yours today."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def dog_shirts
    page_keyword      = "dog"
    page_title        = "Dog Shirts | Custom Dog Apparel"
    @customize_page   = true
    @meta_desc        = "Dog shirts, hoodies & more. See our full range here."
    @social_share_img = "/assets/custom_page_banners/dog-shirt.png"
    @custom_img       = "custom_page_banners/dog.png"
    @header_title     = "Dog"
    @header_text      = "Dog shirts, hoodies, mugs & more for fans of all breeds. Cute comes in all shapes and sizes, we've got something for everyone."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def builder_shirts
    page_keyword      = "builder"
    page_title        = "Builder Shirts | Custom Builder Apparel"
    @customize_page   = true
    @meta_desc        = "Builder shirts for volunteers and the like. Unique designs available here only."
    @social_share_img = "/assets/custom_page_banners/builder-shirt.png"
    @custom_img       = "custom_page_banners/builder.png"
    @header_title     = "Builder"
    @header_text      = "Check out our awesome builder shirts, hoodies & more. Only available on FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def electrician_shirts
    page_keyword      = "electrician"
    page_title        = "Electrician Shirts | Custom Electrician Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Electrician Shirts, hoodies, mugs and more. Available designs here."
    @social_share_img = "/assets/custom_page_banners/electrician-shirt.png"
    @custom_img       = "custom_page_banners/electrician.png"
    @header_title     = "Electrician"
    @header_text      = "Check out our awesome electrician shirts, hoodies & more. Available here only."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def dirt_bike_shirts
    page_keyword      = "dirt bike"
    page_title        = "Dirt Bike Shirts | Custom Dirt Bike Apparel"
    @customize_page   = true
    @meta_desc        = "Dirt bike clothing, mugs and phones. Check them out now"
    @social_share_img = "/assets/custom_page_banners/dirt-bike-shirt.png"
    @custom_img       = "custom_page_banners/dirt-bike.png"
    @header_title     = "Dirt Bike"
    @header_text      = "Shirts, hoodies, mugs & more for every dirt bike fan. Find yours today."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def british_comedy_shirts
    page_keyword      = "british comedy"
    page_title        = "British Comedy Shirts"
    @customize_page   = true
    @meta_desc        = "British Comedy shows shirt, hoodies, mugs and more. Available designs here."
    @social_share_img = "/assets/custom_page_banners/british-comedy-shirt.png"
    @custom_img       = "custom_page_banners/british-comedy.png"
    @header_title     = "British Comedy"
    @header_text      = "Check out these awesome British comedy shirts, hoodies, hats & more."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def beard_shirts
    page_keyword      = "beard"
    page_title        = "Beard Shirts | Custom Beard Apparel"
    @customize_page   = true
    @meta_desc        = "Fear the beard, beard shirts, hoodies, mugs and more. Check them out now."
    @social_share_img = "/assets/custom_page_banners/beard-shirt.png"
    @custom_img       = "custom_page_banners/beard.png"
    @header_title     = "Beard"
    @header_text      = "Awesome Beard shirts, hoodies, hats & more. Only available on FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def drag_racing_shirts
    page_keyword      = "drag racing"
    page_title        = "Drag Racing Shirts"
    @customize_page   = true
    @meta_desc        = "Unique Drag Racing Shirts. Nice designs available here only."
    @social_share_img = "/assets/custom_page_banners/drag-racing-shirt.png"
    @custom_img       = "custom_page_banners/drag-race.png"
    @header_title     = "Drag Racing"
    @header_text      = "Check out our awesome drag racing shirts, hoodies & more."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def camping_shirts
    page_keyword      = "camping"
    page_title        = "Camping Shirts | Custom Camping Apparel"
    @customize_page   = true
    @meta_desc        = "Camp shirts, hoodies, mugs and more. Check them out now."
    @social_share_img = "/assets/custom_page_banners/camping-shirt.png"
    @custom_img       = "custom_page_banners/camping.png"
    @header_title     = "Camping"
    @header_text      = "Shirts, hoodies, mugs & more for all camping lovers. Available only at FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def football_shirts
    page_keyword      = "football"
    page_title        = "Football Shirts | Custom Footy Apparel"
    @customize_page   = true
    @meta_desc        = "Footy, Rugby and football shirts, hoodies, mugs and more. Available here only."
    @social_share_img = "/assets/custom_page_banners/football-shirt.png"
    @custom_img       = "custom_page_banners/football.png"
    @header_title     = "Football"
    @header_text      = "Shirts, hoodies, mugs & more for every Footy, Rugby and football fan."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def torana_shirts
    page_keyword      = "torana"
    page_title        = "Torana Shirts | Custom Torana Apparel"
    @customize_page   = true
    @meta_desc        = "Get your Torana shirts, hoodies, mugs and more. Nice designs available here only."
    @social_share_img = "/assets/custom_page_banners/torana-shirt.png"
    @custom_img       = "custom_page_banners/torana.png"
    @header_title     = "Torana"
    @header_text      = "Shirts, hoodies, mugs & more for every Torana fan. Find your next design here."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def rum_shirts
    page_keyword      = "rum"
    page_title        = "Rum Shirts | Custom Rum Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Rum Shirts. One for every humour style. Find yours now."
    @social_share_img = "/assets/custom_page_banners/rum-shirt.png"
    @custom_img       = "custom_page_banners/rum.png"
    @header_title     = "Rum"
    @header_text      = "Shirts, hoodies, mugs & more for every Rum lover. Only available on FanTees."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def wine_shirts
    page_keyword      = "wine"
    page_title        = "Wine Shirts | Custom Wine Apparel"
    @customize_page   = true
    @meta_desc        = "Unique Wine Shirts. One for every humour style. Find your now."
    @social_share_img = "/assets/custom_page_banners/wine-shirt.png"
    @custom_img       = "custom_page_banners/wine.png"
    @header_title     = "Wine"
    @header_text      = "Shirts, hoodies, mugs & more for every Wine lover."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def v8_shirts
    page_keyword      = "v8"
    page_title        = "V8 Shirts | Custom V8 Apparel"
    @customize_page   = true
    @meta_desc        = "Get your V8 shirts, hoodies, mugs and more. Nice designs available here only."
    @social_share_img = "/assets/custom_page_banners/v8-shirt.png"
    @custom_img       = "custom_page_banners/v8.png"
    @header_title     = "V8"
    @header_text      = "Shirts, hoodies, mugs & more for every V8 fan."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def army_shirts
    page_keyword      = "army"
    page_title        = "Army Shirts | Custom Army Apparel"
    @customize_page   = true
    @meta_desc        = "Get your cool Army shirts, hoodies, mugs and more. Nice designs available here only."
    @social_share_img = "/assets/custom_page_banners/army-shirt.png"
    @custom_img       = "custom_page_banners/army.png"
    @header_title     = "Army"
    @header_text      = "Check out these awesome Army shirts, hoodies, hats & more."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def burnout_shirts
    page_keyword      = "burnout"
    page_title        = "Burnout Shirts | Custom Burnout Apparel"
    @customize_page   = true
    @meta_desc        = "Get your Burnout shirts, hoodies, mugs and more. Nice designs available here only."
    @social_share_img = "/assets/custom_page_banners/burnout-shirt.png"
    @custom_img       = "custom_page_banners/burnout.png"
    @header_title     = "Burnout"
    @header_text      = "Shirts, hoodies, mugs & more for every Burnout fan."

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def farmanimalrescue_shirts
    page_keyword      = "farmanimalrescue"
    page_title        = "FarmAnimalRescue Shirts"
    @customize_page   = true
    @meta_desc        = "Limited Edition Farm Animal Rescue tees & hoodies for passionate farm animal rescue lovers and vegans alike!"
    @social_share_img = "/assets/custom_page_banners/farmanimal-shirt.jpg"
    @custom_img       = "custom_page_banners/farmanimal.png"
    @header_title     = "FarmAnimalRescue"
    @header_text      = "Help your favourite rescue by getting some quality merch with the proceeds going directly to farmanimalrescue.org.au"

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

  def custom_url
    @page_url = request.env["PATH_INFO"].gsub('/','')
    @customize_page = false

    case @page_url
    when "fathersday"
      page_keyword = "fathers day"
      page_title = "Fathers Day"
    when "pet-rescue"
      page_keyword = "petrescue"
      page_title = "Pet Rescue"
    end

    @keyword  = params[:search] ? params[:search] : page_keyword
    @products = @website.products.my_search(@keyword, false, false).page(params[:page]).per(16)
    @fans     = @website.orders.where(:shipped => 1).count
    @rating   = @website.ratings.last
    @title    = "#{page_title} | Fantees"

    @group1 = []
    @group2 = []

    @products.order.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end

end
