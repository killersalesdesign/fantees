class GcardsController < ApplicationController
  def show
    @coupon = Coupon.find_by_code params[:code]
    save_to_session

    render :json => @coupon
  end

  private

  def save_to_session
    if @coupon.nil?
      # has error
      session.delete(:coupon)
    elsif !@coupon.active
      # has errors
      session.delete(:coupon)
    elsif @coupon.expiration && @coupon.expiration < DateTime.now.to_date
      # has errors
      session.delete(:coupon)
    elsif @coupon.limit && @coupon.usage >= @coupon.limit
      # has errors
      session.delete(:coupon)
    else
      session[:coupon] = @coupon.code
    end
  end
end
