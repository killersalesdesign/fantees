include ActionView::Helpers::NumberHelper
class ProductsController < ApplicationController
  before_filter :check_product, :only => [:paypal_checkout, :set_checkout_session, :checkout]

  def index
    @products = Product.where(active: true)
    respond_to do |format|
      format.xml
    end
  end

  def name_search
    unless params[:q].nil?
      keyword = [params[:q], params[:s]].join(" ")
      searches = Product.search_by_tags(keyword)
      if searches.empty?
        @s = Search.new
        @s.name = keyword
        @s.fname = params[:q]
        @s.lname = params[:s]
        @s.save
      else
        @product = searches.first
      end
    end
  end

  def name_search_notify
    @notify = true
    @search = Search.find(params[:i])
    @search.email = params[:email]
    @search.save
    render "name_search"
  end

  def show

    session.delete(:rebuild)
    session.delete(:rebuild_time_frame)

    @product = @website.products.where(:slug => params[:slug]).first

    unless @product.nil?
      # puts @cart.inspect
      # render :json => {cart: @cart}
      @fb_og_image = @product.product_images.sample.product_image(:medium)
      @sold        = @product.total_sold
      start_num    = @product.start_number ? @product.start_number : 0
      @sales       = start_num + @sold
      # @categories  = Category.where(:id => @product.settings(:options).categories).where(:active => true)
      @error       = params[:error] ? true : false
      @order_err   = params[:o] ? true : false
      @title       = @product.title

      gon.category_id = params[:cid].nil? ? nil : params[:cid]
      # gon.current_user = current_user
      # gon.product      = @product
      # gon.gallery      = @product.gallery
      # gon.prices       = @product.settings(:options).prices
      # gon.categories   = @categories
      # gon.stocks       = @product.stocks if @product.status != 0

      # delivery_date = @product.date + 14.days
      # if @product.status == 2
      #   # if have stocks
      #   delivery = DeliveryService.new(2, Time.now)
      #   delivery_date = delivery.bussiness_days
      # end

      # gon.delivery_date = delivery_date.strftime("%b %d, %Y")
    else
      redirect_to "https://campaigns.fantees.com.au/#{params[:slug]}"
      # redirect_to root_path
      # respond_to do |format|
      #   format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      #   format.xml  { head :not_found }
      #   format.any  { head :not_found }
      # end
    end
  end

  def product_info
    product = @website.products.where(:slug => params[:slug]).first
    unless product.nil?
      # delivery_date = product.date + 14.days
      delivery_date = product.date + 9.days
      if product.status == 2
        # if have stocks
        # delivery = DeliveryService.new(2, Time.now)
        # delivery_date = delivery.bussiness_days
        delivery_date = Time.now.beginning_of_day + 4.days
      elsif product.status == 1
        delivery_date = Time.now.beginning_of_day + 12.days
      end

      if product.status == 2
        international = Time.now.beginning_of_day + 10.days
        domestic      = Time.now.beginning_of_day + 4.days
      elsif product.status == 1
        international = Time.now.beginning_of_day + 14.days
        # domestic      = Time.now.beginning_of_day + 10.days
        domestic      = Time.now.beginning_of_day + 9.days
      else
        international = product.date + 14.days
        # domestic      = product.date + 10.days
        domestic      = product.date + 9.days
      end

      render :json => {
        product: product,
        gallery: product.gallery,
        prices:  product.settings(:options).prices,
        stocks:  product.status != 0 ? product.stocks : nil,
        delivery_date: delivery_date.strftime("%b %d, %Y"),
        international: international.strftime("%b %d, %Y"),
        domestic: domestic.strftime("%b %d, %Y"),
        categories: Category.where(:id => product.settings(:options).categories).where(:active => true)
      }
    else
      render :json => product
    end
  end

  def product_upsell
    begin
      product = Product.where(:slug => params[:key]).first
      tags = JSON.parse product.tags

      # puts "tag awa", product.tags
      search = ''
      tags.each  do |tag|
        search = "#{search} #{tag['text']}"
      end
      products = Product.where((['products.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%\"#{keyword}\%"}).limit(3)

      allData = []

      products.each do |p|
        # delivery_date = p.date + 14.days
        delivery_date = p.date + 9.days
        if p.status == 2
          # if have stocks
          # delivery = DeliveryService.new(2, Time.now)
          # delivery_date = delivery.bussiness_days
          delivery_date = Time.now.beginning_of_day + 4.days
        elsif p.status == 1
          delivery_date = Time.now.beginning_of_day + 12.days
        end
        allData << {
          id: p.id,
          product: p,
          gallery: p.gallery,
          prices:  p.settings(:options).prices,
          stocks:  p.status != 0 ? p.stocks : nil,
          delivery_date: delivery_date.strftime("%b %d, %Y"),
          categories: Category.where(:id => p.settings(:options).categories).where(:active => true)
        }
      end
    rescue
      allData = []
    end

    render :json => allData

    # render :json => products
  end

  def giftcards
    @product = @website.products.where(:slug => params[:slug]).first

    unless @product.nil?
      # puts @cart.inspect
      # render :json => {cart: @cart}
      @fb_og_image = @product.product_images.sample.product_image(:medium)
      @sold        = @product.total_sold
      start_num    = @product.start_number ? @product.start_number : 0
      @sales       = start_num + @sold
      @categories  = Category.where(:id => @product.settings(:options).categories).where(:active => true)
      @error       = params[:error] ? true : false
      @order_err   = params[:o] ? true : false

      gon.current_user = current_user
      gon.product      = @product
      gon.gallery      = @product.gallery
      gon.prices       = @product.settings(:options).prices
      gon.categories   = @categories
      gon.stocks       = @product.stocks if @product.status != 0

      # delivery_date = @product.date + 14.days
      delivery_date = @product.date + 9.days
      if @product.status == 2
        # if have stocks
        # delivery = DeliveryService.new(2, Time.now)
        # delivery_date = delivery.bussiness_days
        delivery_date = Time.now.beginning_of_day + 4.days
      elsif @product.status == 1
        delivery_date = Time.now.beginning_of_day + 12.days
      end

      gon.delivery_date = delivery_date.strftime("%b %d, %Y")
    else
      redirect_to root_path
    end
  end

  def paypal_checkout

    begin
      # puts params.inspect
      product  = Product.find(params[:product_id])
      redirect_to root_path unless @product.active

      qty      = 0
      key      = 0
      subtotal = 0
      shipping = 0
      items    = []
      session[:orders] = []
      params[:orders][:qty].each do |o|
        category_id = params[:orders][:styles][key]
        size  = params[:orders][:sizes][key]
        style = Category.find(category_id)
        info  = product.price_and_profit_of_style(category_id)

        qty      += o.to_i
        subtotal += o.to_i * info[:amount].to_f
        shipping = info[:postage].to_f if info[:postage] && info[:postage] > shipping

        session[:orders] << {:category_id => style.id, :quantity => o.to_i, :size => size, :price => info[:amount]}
        items << {:name => product.title, :quantity => o.to_i, :description => "#{size} #{style.name} type_id: #{style.id}", :amount => (info[:amount]*100)}

        key += 1
      end

      # total = qty * product.price
      subtotal = subtotal * 100
      shipping = product.free_shipping > qty ? shipping * 100 : 0
      total    = subtotal + shipping
      # total = subtotal
      response = EXPRESS_GATEWAY.setup_purchase(total.round,
        :subtotal          => subtotal.round,
        :shipping          => shipping.round,
        :handling          => 0,
        :tax               => 0,
        :items             => items,
        :ip                => request.remote_ip,
        :currency          => 'AUD',
        :return_url        => success_product_url(product.id),
        :cancel_return_url => "#{root_url}/#{product.slug}"
      )

      # render :json => {response: response, shipping: shipping, :subtotal => subtotal, items: items}
      redirect_to "#{EXPRESS_GATEWAY.redirect_url_for(response.token)}&useraction=commit"


    rescue Exception => exc
       error_msg = "Message for the log file: #{exc.message}"
       UserMailer.delay.send_error_logs('matt.stenning@graphicsadmin.com', error_msg)
       UserMailer.delay.send_error_logs('rick@fantees.com.au', error_msg)
       # redirect_to root_path
    end

  end

  # def return_url
  #   orders = []
  #   if session[:orders].nil?
  #     gateway = EXPRESS_GATEWAY.details_for(params[:token])
  #     gateway.params["PaymentDetails"]["PaymentDetailsItem"].each do |o|
  #       desc = o["Description"].split(" ")
  #       amt  = o["Amount"].to_f
  #       qty  = o["Quantity"]
  #       orders << {category_id: desc.last.to_i, quantity: qty.to_i, size: desc.first, price: amt}
  #     end
  #   else
  #     orders = session[:orders]
  #   end

  #   unless orders.blank?
  #     params[:ip] = request.remote_ip
  #     # paypal      = PaypalCheckout.new(params, session)
  #     paypal      = PaypalCheckout.new(params, session[:orders])
  #     product     = Product.find(params[:id])
  #     order       = Order.find_by_token(params["token"])
  #     check_order = paypal.check_orders

  #     # render :json => {paypal: paypal, orders: paypal.check_orders}
  #     if order.nil? && check_order
  #       if paypal.purchase?
  #         order = paypal.create_order
  #         if order
  #           session.delete(:orders)

  #           sign_in(:user, paypal.user)

  #           redirect_to order_url(order.order_id)
  #         else
  #           redirect_to "#{root_url}#{product.slug}?error=true"
  #         end
  #       else
  #         redirect_to "#{root_url}#{product.slug}?error=true"
  #       end
  #     else
  #       if check_order == false
  #         redirect_to "#{root_url}#{product.slug}?error=true&o=true"
  #       else
  #         redirect_to "#{root_url}#{product.slug}?error=true"
  #       end
  #     end
  #   else
  #     product = Product.find(params[:id])
  #     redirect_to "#{root_url}#{product.slug}?error=true"
  #   end
  # end

  def return_url
    product = Product.find(params[:id])

    unless session[:orders].empty?
      params[:ip] = request.remote_ip
      paypal      = PaypalCheckout.new(params, session[:orders])
      order       = Order.find_by_token(params["token"])
      check_order = paypal.check_orders

      if order.nil? && check_order
        order = paypal.create_order
        if order && paypal.purchase?
          session.delete(:orders)
          sign_in(:user, paypal.user)

          redirect_to order_url(order.order_id)
        else
          order.delete
          redirect_to "/#{product.slug}?error=true"
        end
      else
        opt_error = check_order == false ? "&o=true" : ""
        redirect_to "/#{product.slug}?error=true#{opt_error}"
      end
    else
      redirect_to "/#{product.slug}?error=true"
    end
  end

  def set_checkout_session
    session[:checkout] = params
    session.delete(:rebuild)
    redirect_to checkout_products_url
  end

  def checkout
    if session[:checkout]
      params = session[:checkout]

      # @product = Product.find(params[:product_id])
      @order   = Order.new
      @info    = session[:checkout][:info]
      @coupon_err = request.GET["coupon"] == "error" ? true : false

      gon.product = @product
      gon.website = @website
      gon.info = @info || {}
      gon.cut_off = {domestic: @product.free_shipping, international: @product.international_cutoff, add_cost: @product.additional_item_cost}
      gon.prices  = @product.settings(:options).prices
      gon.params  = params

      gallery = @product.gallery
      # @delivery_date = @product.date + 14.days
      @delivery_date = @product.date + 9.days
      if @product.status == 2
        # if have stocks
        # delivery = DeliveryService.new(2, Time.now)
        # @delivery_date = delivery.bussiness_days
        @delivery_date = Time.now.beginning_of_day + 4.days
      elsif @product.status == 1
        @delivery_date = Time.now.beginning_of_day + 12.days
      end

      if session[:rebuild]
        unfinished_order_tracking_id = session[:unfinished_order_tracking_id]
      else
        unfinished_order_tracking_id = SecureRandom.hex(10)
        # session[:unfinished_order_tracking_id] = unfinished_order_tracking_id
      end

      gon.tracking_id = unfinished_order_tracking_id
      gon.root_url = root_url

      quantity  = 0
      @total    = 0
      @shipping = 0
      key       = 0
      @orders   = []
      @pending_orders = []

      params[:orders][:qty].each do |o|
        size  = params[:orders][:sizes][key]
        style = Category.find(params[:orders][:styles][key])

        sizes = style.settings(:options).sizes

        p_image = ""
        gallery.each do |g|
          if g[:category_id] == style.id
            p_image = g[:medium]
            gon.product_image = p_image
          end
        end

        info  = @product.price_and_profit_of_style(style.id)
        price = info[:amount].to_f * o.to_f
        quantity += o.to_f
        @shipping = info[:postage].to_f if info[:postage] && info[:postage] > @shipping
        @total += price
        @pending_orders << {
          :product_id => params[:product_id],
          :name => @product.title,
          :description => @product.description,
          :qty => o,
          :size => size,
          :style => style.name,
          :price => number_to_currency(price),
          :image => p_image,
          :sizes => sizes,
          :category_id => style.id,
          :unfinished_order_tracking_id => unfinished_order_tracking_id
        }
        @orders << {:description => "#{o}x #{style.name} (#{size})", :price => number_to_currency(price)}

        unless session[:rebuild]
          if session[:unfinished_order_tracking_id]
            UnfinishedOrder.remove_order(session[:unfinished_order_tracking_id])
          end

          # create unfinished orders here
          UnfinishedOrder.create(
            :product_id => @product.id,
            :qty => o,
            :size => size,
            :category => style.id,
            :style => style.name,
            :price => price,
            :tracking_id => unfinished_order_tracking_id,
            :website_id => 1 #fantees
          )
        end


        key += 1
      end

      gon.total = number_to_currency(@total)

      session[:unfinished_order_tracking_id] = unfinished_order_tracking_id

      @shipping = @product.free_shipping > quantity ? @shipping : 0
    else
      redirect_to root_url
    end

    # render :json => {session: session, params: params}
  end

  def eway_checkout
    session[:checkout][:info] = params
    params[:orders]           = session[:checkout][:orders]
    params[:product_id]       = session[:checkout][:product_id]

    eway     = EwayCheckout.new(params)
    response = eway.checkout(EWAY_COSTUMER_ID)

    puts "RESPONSE: #{response.inspect}"
    # render :json => {eway: eway}

    if response.success?
      token = response.authorization

      user     = User.find_or_initialize_by_email(params[:email])
      password = nil
      if user.id.nil?
        password                   = Devise.friendly_token[0,8]
        user.password              = password
        user.password_confirmation = password
        user.first_name            = params[:first_name]
        user.last_name             = params[:last_name]
        user.phone                 = params[:phone]
        address                    = []

        address << params["address"] unless params["address"].blank?
        address << params["city"] unless params["city"].blank?
        address << params["state"] unless params["state"].blank?
        address << params["country"] unless params["country"].blank?
        user.address = address.join(", ")

        user.save
      end

      order = user.orders.new(
        :ip_address => request.remote_ip,
        :product_id => params[:product_id],
        :token      => token,
        :shipping_info => eway.paypal_shipping_format.to_json,
        :order_id   => eway.get_order_id,
        :website_id => @website.id
      )
      # render :json => {eway: eway.creditcard}
      if order.save
        session.delete(:checkout)

        eway.get_items.each do |o|
          # total += (o[:quantity] * o[:price])

          order.ordered_products.new(
            :quantity => o[:quantity],
            :size => o[:size],
            :category_id => o[:category_id]
          ).save

          order.quantity = order.ordered_products.sum(:quantity)
          order.save
        end
      end

      sign_in(:user, user)
      # send mail after create
      order.send_order_mail(password)

      redirect_to order_url(order.order_id)
    else
      redirect_to :back, :flash => { :error => response.message }
    end
  end

  def order_checkout
    # puts 'paramsssssssss'
    # puts params.inspect

    if params[:mode] == "paypal"
      begin
        ctr = 0
        qty = 0
        subtotal = 0
        shipping = 0
        items = []

        product  = Product.find(params[:product_id])
        session[:shipping] = {
          :Name            => "#{params[:first_name].humanize} #{params[:last_name].humanize}",
          :Street1         => params[:address],
          :CityName        => params[:city],
          :StateOrProvince => params[:state],
          :Country         => params[:country],
          :CountryName     => params[:country],
          :PostalCode      => params[:zip],
          :Phone           => params[:phone]
        }
        session[:orders] = []
        params[:cat_ids].each do |cat_id|
          style = Category.find(cat_id)
          size  = params[:size][ctr]
          info  = product.price_and_profit_of_style(cat_id)
          qty   += params[:qty][ctr].to_i
          subtotal += params[:qty][ctr].to_i * info[:amount].to_f
          shipping = info[:postage].to_f if info[:postage] && info[:postage] > shipping

          session[:orders] << {:category_id => cat_id, :quantity => params[:qty][ctr].to_i, :size => size, :price => info[:amount]}
          items << {:name => product.title, :quantity => params[:qty][ctr].to_i, :description => "#{size} #{style.name} type_id: #{style.id}", :amount => (info[:amount]*100)}

          ctr += 1
        end

        subtotal = subtotal * 100
        shipping = product.free_shipping > qty ? shipping * 100 : 0
        total    = subtotal + shipping

        response = EXPRESS_GATEWAY.setup_purchase(total.round,
          :subtotal          => subtotal.round,
          :shipping          => shipping.round,
          :handling          => 0,
          :tax               => 0,
          :items             => items,
          :ip                => request.remote_ip,
          :currency          => 'AUD',
          :return_url        => success_product_url(product.id),
          :cancel_return_url => "#{root_url}/#{product.slug}"
        )

        # render :json => {response: response, shipping: shipping, :subtotal => subtotal, items: items}
        redirect_to "#{EXPRESS_GATEWAY.redirect_url_for(response.token)}&useraction=commit"

      rescue Exception => exc
         error_msg = "Message for the log file: #{exc.message}"
         UserMailer.delay.send_error_logs('matt.stenning@graphicsadmin.com', error_msg)
         UserMailer.delay.send_error_logs('rick@fantees.com.au', error_msg)
         # redirect_to root_path
      end

    else
      # credit card
      eway  = EwayCheckout.new(params)
      check = Coupon.check params[:coupon] if params[:coupon]
      product  = Product.find(params[:product_id])
      # render :json => {check: eway.check_orders , coupon: check}

      if params[:coupon].blank? != true && check == false
        redirect_to "#{root_url}products/checkout?coupon=error"
      elsif eway.check_orders == false
        redirect_to "#{root_url}#{eway.get_product.slug}?error=true&o=true"
      else
        response = eway.checkout(EWAY_COSTUMER_ID)

        if response.success?
          token  = response.authorization

          user     = User.find_or_initialize_by_email(params[:email])
          password = nil
          if user.id.nil?
            password                   = Devise.friendly_token[0,8]
            user.password              = password
            user.password_confirmation = password
            user.first_name            = params[:first_name]
            user.last_name             = params[:last_name]
            user.phone                 = params[:phone]
            address                    = []

            address << params["address"] unless params["address"].blank?
            address << params["city"] unless params["city"].blank?
            address << params["state"] unless params["state"].blank?
            address << params["country"] unless params["country"].blank?
            user.address = address.join(", ")

            user.save
          end

          order = user.orders.new(
            :ip_address => request.remote_ip,
            :product_id => params[:product_id],
            :token      => token,
            :shipping_info => eway.paypal_shipping_format.to_json,
            :order_id   => eway.get_order_id,
            :website_id => @website.id
          )
          unless params[:coupon].blank?
            coupon = Coupon.find_by_code params[:coupon]
            coupon.increment!(:usage)

            order.coupon_id = coupon.id
          end

          # render :json => {eway: eway, params: params}
          if order.save
            session.delete(:checkout)
            eway.get_items.each_with_index do |o, index|
              # total += (o[:quantity] * o[:price])

              ordered_products = order.ordered_products.new(
                :quantity => o[:quantity],
                :size => o[:size].strip,
                :category_id => o[:category_id],
                :product_id => params[:product_id]
              ).save

              if product.gift_card == true
                (1..o[:quantity].to_i).each do |i|
                  order.giftcards.new(
                    :is_used => false,
                    :key_code => SecureRandom.hex[0..10].upcase
                  ).save
                end
              end

              # if ordered_products.save
              #   # code here
              #   UserMailer.delay.on_purchase(order, eway.get_items, password) if (index + 1) == eway.get_items.count
              # end
            end

            order.quantity = order.ordered_products.sum(:quantity)
            order.save
          end

          sign_in(:user, user)
          # send mail after create
          # order.send_order_mail(password)
          UserMailer.on_purchase(order, eway.get_items, password).deliver

          redirect_to order_url(order.order_id)
        else
          redirect_to :back, :flash => { :error => response.message }
        end
      end

    end

  end

  private

  def check_product
    id       = params[:product_id] || session[:checkout][:product_id]
    @product = Product.find(id)
    redirect_to root_path unless @product.active
  end
end
