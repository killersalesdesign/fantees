class PagesController < ApplicationController
  # layout "application", :only => :contact_us
  layout "pinterest", :only => :pinterest

  # before_filter :set_cache_buster
  before_filter :set_page_name
  def contact_us
    @contact = ContactUs.new
  end

  def send_contact_us
    # contact = ContactUs.new(params[:contact_us])
    contact = ContactUs.new(params[:contact_us])
    if contact.valid? && !contact.spam?
      contact.deliver
      respond_to do |format|
        format.html { redirect_to "/contact-us?er=0", :flash => { :notice => "Thanks for getting in contact! We'll be in touch with you in the next 24-48 hours." } }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to "/contact-us?er=1", :flash => { :error => contact.errors.full_messages } }
        format.json { head :no_content }
      end
    end
    # render :json => {params: params}
  end

  def about_us
  end

  def faq
  end

  def terms
  end

  def privacy
  end

  def shipping
  end

  def returns_exchanges
  end

  def sizing
  end

  def processing
  end

  def pinterest
  end

  def win
  end

  private

  def set_page_name
    gon.page = action_name
  end

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
