include ActionView::Helpers::NumberHelper
class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :get_website, :get_total_shipped, :start_cart
  # before_filter :get_total_shipped

  def get_website
    @website = Website.find_by_name(ENV['APP_NAME'])
  end

  def get_total_shipped
  	@shipped = Order.where(shipped: true).count
  end

  def start_cart
    @cart = Cart.new(session)
  end
end
