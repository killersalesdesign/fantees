class OrdersController < ApplicationController
  def show
    @order    = Order.find_by_order_id(params[:id])
    @products = @order.ordered_products
    @product  = @order.product

    # @delivery_date = @product.date + 14.days
    @delivery_date = @product.date + 9.days
    if @product.status == 2
      delivery = DeliveryService.new(2, Time.now)
      @delivery_date = delivery.bussiness_days
    elsif @product.status == 1
      @delivery_date = Time.now.beginning_of_day + 12.days
    end

    if @product.status == 2
      @international = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 4.days
    elsif @product.status == 1
      @international = Time.now.beginning_of_day + 14.days
      # @domestic      = Time.now.beginning_of_day + 10.days
      @domestic      = Time.now.beginning_of_day + 9.days
    else
      @international = @product.date + 14.days
      # @domestic      = @product.date + 10.days
      @domestic      = @product.date + 9.days
    end

    # remove scheduled unfinished order emails
    if session[:thirthy_minute] || session[:twenty_four_hours] || session[:ten_days]
      scheduled_jobs = Sidekiq::ScheduledSet.new
      jobs = scheduled_jobs.select {|i| i.jid == "#{session[:thirthy_minute]}" || i.jid == "#{session[:twenty_four_hours]}" || i.jid == "#{session[:ten_days]}"}
      jobs.each(&:delete)
    end

    if session[:rebuild]
      order = UnfinishedOrder.find_by_tracking_id(session[:unfinished_order_tracking_id])
      order.recovered = true
      order.recovered_date = Time.now
      order.save!

      if session[:rebuild_time_frame]
        # record rejoin in mail tracker
        MailTracker.increment_count("rejoins", session[:rebuild_time_frame])
      end
    else
      # remove unfinished order
      UnfinishedOrder.remove_order(session[:unfinished_order_tracking_id])
    end

    #get suggested products
    if @product.tags
      tags      = JSON.parse @product.tags
      search    = ''
      tags.each  do |tag|
        search  = "#{search} #{tag['text']}"
      end
      products  = Product.where((['products.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%\"#{keyword}\%"}).limit(3)
      @upsell   = []
      begin
        products.each do |p|
          @upsell << {
            price: p.settings(:options).prices[0][:amount],
            image: p.gallery[0][:medium],
            slug: p.slug
          }
        end
      rescue
      end
    end



    # cleanup sessions
    session.delete(:unfinished_order_tracking_id)
    session.delete(:rebuild)
    session.delete(:rebuild_time_frame)
    session.delete(:thirthy_minute)
    session.delete(:twenty_four_hours)
    session.delete(:ten_days)
  end

  # old order action
  # def show
  #   # @style      = "margin-bottom: 286px;"
  #   @order      = Order.find_by_order_id(params[:id])
  #   @image      = @order.product_image
  #   @product    = @order.product
  #   @sold       = @product.total_sold
  #   @sales      = @product.start_number + @sold
  #   @categories = Category.where(:id => @product.settings(:options).categories).where(:active => true)

  #   @delivery_date = @product.date + 14.days
  #   if @product.status == 2
  #     delivery = DeliveryService.new(2, Time.now)
  #     @delivery_date = delivery.bussiness_days
  #   end

  #   gon.order_id   = @order.order_id
  #   gon.user       = @order.user
  #   gon.product    = @product
  #   gon.gallery    = @product.gallery
  #   gon.categories = @categories

  #   UnfinishedOrder.remove_order(session[:unfinished_order_tracking_id])
  #   session.delete(:unfinished_order_tracking_id)
  #   session.delete(:rebuild)
  # end

  def rebuild
    order = UnfinishedOrder.find_tracking_id(params[:tracking_id])
    if order.nil?
      redirect_to root_url
    else
      temp_sess = Hash.new
      temp_sess[:product_id] = order.first.product_id

      qty = []
      sizes = []
      styles = []
      prices = []
      order.each do |o|
        qty << o.qty
        sizes << o.size
        styles << o.category
        prices << o.price
      end

      temp_sess[:orders] = Hash.new
      temp_sess[:orders][:qty] = qty
      temp_sess[:orders][:sizes] = sizes
      temp_sess[:orders][:styles] = styles
      temp_sess[:orders][:prices] = prices

      # rebuild session data
      session[:checkout] = temp_sess
      session[:rebuild] = true
      session[:unfinished_order_tracking_id] = params[:tracking_id]

      # redirect to checkout
      redirect_to checkout_products_url
    end
  end

  def update_unfinished_order
    begin
      unfinished = UnfinishedOrder.find_by_tracking_id(session[:unfinished_order_tracking_id])

      unfinished.settings(:cart).user_email = params[:email] if params[:email]
      unfinished.settings(:cart).first_name = params[:first_name] if params[:first_name]
      unfinished.settings(:cart).last_name  = params[:last_name] if params[:last_name]
      unfinished.settings(:cart).address    = params[:address] if params[:address]
      unfinished.settings(:cart).town       = params[:town] if params[:town]
      unfinished.settings(:cart).state      = params[:state] if params[:state]
      unfinished.settings(:cart).postal     = params[:postal] if params[:postal]
      unfinished.settings(:cart).country    = params[:country] if params[:country]
      unfinished.settings(:cart).phone      = params[:phone] if params[:phone]

      unfinished.save!
    rescue
    end

    render :json => {msg: "Success"}
  end
end
