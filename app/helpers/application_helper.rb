module ApplicationHelper
  def socail_smart_layer
    js = <<-eos
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5320264f0ac93ecd"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent',
    'share' : {
      'position' : 'left',
      'services' : 'facebook,twitter,pinterest_share,email,print,more'
    }

  });
</script>
<!-- AddThis Smart Layers END -->
    eos
    js.html_safe
  end

  def recommend4u_js
    js = <<-eos
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5357386f341bcdfcc5000018.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
    eos
    js.html_safe
  end

  def olark_js
    js = <<-eos
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7964-580-10-9213');/*]] > */</script><noscript><a href="https://www.olark.com/site/7964-580-10-9213/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
    eos
    js.html_safe
  end

  def fb_conversion_js
    js = <<-eos
<!-- Facebook Conversion Code for FanTeesAU Sale -->
<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6013515465699';
fb_param.value = '0.00';
fb_param.currency = 'USD';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6013515465699&amp;value=0&amp;currency=USD" /></noscript>
    eos
    js.html_safe
  end

  def spec_ga_js(id)
    js = <<-eos
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', '#{id}', 'auto');
  ga('send', 'pageview');
</script>
    eos
    js.html_safe
  end

  def ga_js
    js = <<-eos
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-38692336-14', 'fantees.com.au');
      ga('send', 'pageview');
      ga('require', 'ecommerce', 'ecommerce.js');
      ga('require', 'linker');
      ga('linker:autoLink', ['blog.fantees.com.au']);
    </script>
    eos
    js.html_safe
  end

  def ge_ecommerce_js(order)
    items     = order.ordered_products
    product   = order.product
    shipping = 0
    quantity  = 0
    js = []
    items.each do |i|
      info      = product.price_and_profit_of_style(i.category_id)
      shipping  = info[:postage].to_f if info[:postage] && info[:postage] > shipping
      quantity += i.quantity
      js << "ga('ecommerce:addItem', {
    'id': '#{order.order_id}',
    'name': \"#{product.title}\",
    'sku': '#{product.id}',
    'category': '#{Category.find(i.category_id).name}',
    'price': '#{info[:amount]}',
    'quantity': '#{i.quantity}'
  });"
    end
    shipping = product.free_shipping > order.quantity ? shipping : 0
    shipping = number_with_precision(shipping, :precision => 2)
    js = <<-eos
<script>
  #{js.join("\n")}
  ga('ecommerce:addTransaction', {
    'id': '#{order.order_id}',
    'affiliation': 'FanTees',
    'revenue': '#{order.total_orders_amount}',
    'shipping': '#{shipping}',
    'tax': '0'
  });
  ga('ecommerce:send');
</script>
    eos
    js.html_safe
  end

  def perfectaudience_js(product)
    pa = ""
    unless product.nil?
      pa = "_pa.productId = '#{product}';"
    end
    js = <<-eos
    <script type="text/javascript">
      (function() {
        window._pa = window._pa || {};
        // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
        // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
        // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
        #{pa}
        var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
        pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5357386f341bcdfcc5000018.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
      })();
    </script>
    eos
    js.html_safe
  end

  def visualweboptimizer_js
    js = <<-eos
<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=43628,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
    eos
    js.html_safe
  end

  def vwo_js
    js = <<-eos
      <!-- Start Visual Website Optimizer Asynchronous Code -->
      <script type='text/javascript'>
        var _vwo_code=(function(){
          var account_id=15681,
            settings_tolerance=2000,
            library_tolerance=2500,
            use_existing_jquery=false,
        // DO NOT EDIT BELOW THIS LINE
        f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
      </script>
      <!-- End Visual Website Optimizer Asynchronous Code -->
    eos
    js.html_safe
  end

  def fb_share(page_url)
    uniq = "#.#{SecureRandom.hex[0..7]}.facebook"
    url  = "https://www.facebook.com/login.php?next=https://www.facebook.com/sharer/sharer.php?u=#{page_url}&display=popup"
    return URI::escape(url)
  end

  def twitter_share(page_url)
    url  = "https://twitter.com/intent/tweet?text=Fantees&url=#{page_url}&related="
    return URI::escape(url)
  end

  def gplus_share(page_url)
    url = "https://accounts.google.com/ServiceLogin?service=oz&passive=1209600&continue=https://plus.google.com/share?url=#{page_url}&t=Fantees&gpsrc=frameless&btmpl=popup"
    return URI::escape(url)
  end

  def pinterest_share(product)
    # http://www.pinterest.com/pin/create/button/
    #     ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
    #     &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
    #     &description=Next%20stop%3A%20Pinterest
    page_url = "#{root_url}#{product.slug}"
    img      = product.product_images.first.product_image(:medium)
    img_url  = "#{root_url}#{img[1..-1]}"
    url      = "http://www.pinterest.com/pin/create/button/?url=#{page_url}&media=#{img_url}&description=#{product.title}"
    return URI::escape(url)
  end

  def retargeting_js(id = 733031176719197)
    js = <<-eos
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
      _fbq.push(['addPixelId', "#{id}"]);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=#{id}&amp;ev=NoScript" /></noscript>
    eos
    js.html_safe
  end

  def fb_pixel_code
    js = <<-eos
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
      _fbq.push(['addPixelId', '316067231915502']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=316067231915502&amp;ev=PixelInitialized" /></noscript>
    eos
    js.html_safe
  end

  def sendgrid_layout_script
    js = <<-eos
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://s3.amazonaws.com/subscription-cdn/0.2/widget.min.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "sendgrid-subscription-widget-js");</script>
    eos
    js.html_safe
  end

  def kissmetrics_js
    js = <<-eos
    <!-- KISSmetrics tracking snippet -->
    <script type="text/javascript">var _kmq = _kmq || [];
    var _kmk = _kmk || '09b37789010a307e419118379bb28a4b40c29dc8';
    function _kms(u){
      setTimeout(function(){
        var d = document, f = d.getElementsByTagName('script')[0],
        s = d.createElement('script');
        s.type = 'text/javascript'; s.async = true; s.src = u;
        f.parentNode.insertBefore(s, f);
      }, 1);
    }
    _kms('//i.kissmetrics.com/i.js');
    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>
    eos
    js.html_safe
  end

  def rejoiner_conversion
    js = <<-eos
    <!-- Rejoiner Conversion -->
    <script type='text/javascript'>
    var _rejoiner = _rejoiner || [];
    _rejoiner.push(['setAccount', '53aa5390281cb721dc5de492']);
    _rejoiner.push(['setDomain', '.fantees.com.au']);
    _rejoiner.push(['sendConversion']);
    (function() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://s3.amazonaws.com/rejoiner/js/v3/t.js';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
    </script>
    <!-- End Rejoiner Conversion -->
    eos
    js.html_safe
  end

  def rejoiner_tracking
    js = <<-eos
    <!-- Rejoiner Tracking -->
    <script type='text/javascript'>
    var _rejoiner = _rejoiner || [];
    _rejoiner.push(['setAccount', '53aa5390281cb721dc5de492']);
    _rejoiner.push(['setDomain', '.fantees.com.au']);
    (function() {
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://s3.amazonaws.com/rejoiner/js/v3/t.js';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
    </script>
    <!-- End Rejoiner Tracking -->
    eos
    js.html_safe
  end

  def respond_js
    js = <<-eos
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>;
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>;
    <![endif]-->
    eos
    js.html_safe
  end

  # duplicate???
  # def respond_js
  #   js = <<-eos
  #   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  #   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  #   <!--[if lt IE 9]>
  #   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>;
  #   <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>;
  #   <![endif]-->
  #   eos
  #   js.html_safe
  # end

  # def fb_conversion_code
  #   js = <<-eos
  #   <!-- Facebook Conversion Code for FanTeesAU Sale -->
  #   <script>(function() {
  #     var _fbq = window._fbq || (window._fbq = []);
  #     if (!_fbq.loaded) {
  #       var fbds = document.createElement('script');
  #       fbds.async = true;
  #       fbds.src = '//connect.facebook.net/en_US/fbds.js';
  #       var s = document.getElementsByTagName('script')[0];
  #       s.parentNode.insertBefore(fbds, s);
  #       _fbq.loaded = true;
  #     }
  #   })();
  #   window._fbq = window._fbq || [];
  #   window._fbq.push(['track', '6013515465699', {'value':'0.00','currency':'USD'}]);
  #   </script>
  #   <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6013515465699&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
  #   eos
  #   js.html_safe
  # end

  def fb_conversion_code(id, currency)
    # hompage - 6013515465699/USD
    # landing - 6020438160761/AUD
    js = <<-eos
    <!-- Facebook Conversion Code for FanTees Conversion -->
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '#{id}', {'value':'0.00','currency':'#{currency}'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=#{id}&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
    eos
    js.html_safe
  end

  def criteo_js(page, data, current_user)
    email = current_user.blank? ? "" : current_user.email
    case page
    when 'search'
      id = data.first(3).collect(&:id)
      event  = "{ event: \"viewList\", item: #{id.to_s} }"
    when 'thankyou'
      items = []
      data.ordered_products.each do |p|
        product = Product.find(p.product_id)
        info    = product.price_and_profit_of_style(p.category_id)
        # price   = number_to_currency(info[:amount], unit: "AUD")
        price   = info[:amount]
        items.push "{ id: \"#{p.product_id}\", price: \"#{price}\", quantity: #{p.quantity} }"
      end
      new_customer = data.user.orders.count > 1 ? 0 : 1
      items = items.join(', ').to_s
      event = "{ event: \"trackTransaction\" , id: \"#{data.order_id}\", new_customer: #{new_customer}, item: [#{items}] }"
    when 'product'
      event = "{ event: \"viewItem\", item: \"#{data.id}\" }"
    when 'cart'
      event = data.inspect
      items = []
      data.each do |p|
        # price  = number_to_currency(p[:price], unit: "AUD")
        price   = p[:price]
        # product = Product.find(p[:product_id])
        items.push "{ id: \"#{p[:product_id]}\", price: \"#{price}\", quantity: #{p[:quantity]} }"
      end
      items = items.join(', ').to_s
      event = "{ event: \"viewBasket\", item: [#{items}] }"
    else
      event = data
    end

    js = <<-eos
    <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
    <script type="text/javascript">
    window.criteo_q = window.criteo_q || [];
    window.criteo_q.push(
      { event: "setAccount", account: 18121 },
      { event: "setHashedEmail", email: "#{email}" },
      { event: "setSiteType", type: "d" },
      #{event}
    );
    </script>
    eos
    js.html_safe
  end

  def special_fb_pixel(id, currency, page, value = '0.00')
    js = <<-eos
<!-- FB Pixel Conversion -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
    eos

    if page == 'landing'
      js += <<-eos
  _fbq.push(['addPixelId', '#{id}']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=#{id}&amp;ev=PixelInitialized" /></noscript>
<!-- End FB Pixel Conversion -->
      eos
    else
      js += <<-eos
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '#{id}', {'value':'#{value}','currency':'#{currency}'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=#{id}&amp;cd[value]=#{value}&amp;cd[currency]=#{currency}&amp;noscript=1" /></noscript>
</script>
<!-- End FB Pixel Conversion -->
      eos
    end

    js.html_safe
  end

  def twitter_conversion
    js = <<-eos
    <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
twttr.conversion.trackPid('l61ha', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l61ha&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l61ha&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" /></noscript>
    eos
    js.html_safe
  end

  def fb_cart_code
    js = <<-eos
    <!-- Facebook Conversion Code for FANTEES - Add To Carts -->
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6028171604961', {'value':'0.00','currency':'AUD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028171604961&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
    eos
    js.html_safe
  end

  def kpv_code
    js = <<-eos
    <!-- Facebook Conversion Code for FANTEES - Key Page Views -->
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6028171580361', {'value':'0.00','currency':'AUD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028171580361&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
    eos
    js.html_safe
  end

  def google_conversion
    js = <<-eos
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 958973834;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "qfJ9CL6Eyl4Qio-jyQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/958973834/?label=qfJ9CL6Eyl4Qio-jyQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
    eos
    js.html_safe
  end

  def hotjar_tracking
    js = <<-eos
<!-- Hotjar Tracking Code for fantees.com.au -->
<script>
(function(h,o,t,j,a,r){
  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  h._hjSettings={hjid:97558,hjsv:5};
  a=o.getElementsByTagName('head')[0];
  r=o.createElement('script');r.async=1;
  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    eos

    js.html_safe
  end

  def optimonk_js
    js = <<-eos
<script type="text/javascript">
(function(e,a){
  var t,r=e.getElementsByTagName("head")[0],c=e.location.protocol;
  t=e.createElement("script");t.type="text/javascript";
  t.charset="utf-8";t.async=!0;t.defer=!0;
  t.src=c+"//front.optimonk.com/public/"+a+"/js/preload.js";r.appendChild(t);
})(document,"7830");
</script>
    eos
    js.html_safe
  end

  def optinmonster_js
    js = <<-eos
<!-- This site is converting visitors into subscribers and customers with OptinMonster - http://optinmonster.com --><div id="om-gw2jcpbwhfgtangl-holder"></div><script>var gw2jcpbwhfgtangl,gw2jcpbwhfgtangl_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){gw2jcpbwhfgtangl_poll(function(){if(window['om_loaded']){if(!gw2jcpbwhfgtangl){gw2jcpbwhfgtangl=new OptinMonsterApp();return gw2jcpbwhfgtangl.init({u:"12799.239447",staging:0,dev:0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;gw2jcpbwhfgtangl=new OptinMonsterApp();gw2jcpbwhfgtangl.init({u:"12799.239447",staging:0,dev:0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster -->
    eos
    js.html_safe
  end

  def new_fb_pixel(current_page, amount='0.00')
    additional_code = ""
    case current_page
    when "search"
      additional_code = "fbq('track', 'Search');"
    when "cart"
      additional_code = "fbq('track', 'AddToCart');"
      # additional_code = "fbq('track', 'InitiateCheckout');"
    when "creditcard"
      additional_code = "fbq('track', 'AddPaymentInfo');"
    when "thankyou"
      # additional_code = "fbq('track', 'Purchase', {value: '#{amount}', currency: 'AUD'});"
      additional_code = "fbq('track', 'Purchase', {value: '0.00', currency: 'USD'});"
    end

    js = <<-eos
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1481753238797789');
    fbq('track', "PageView");

    #{additional_code}

    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1481753238797789&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    eos
    js.html_safe
  end

  def my_hellobar_js
    js = <<-eos
    <script src="//my.hellobar.com/3e3e4d8da5762d0e162f1f750fcc7cf085d5e98b.js" type="text/javascript" charset="utf-8" async="async"></script>
    eos
    js.html_safe
  end
end
