class UnfinishedOrderWorker
	include Sidekiq::Worker

	def perform(unfinished_order_id, time_frame)
		# unfinished_order = UnfinishedOrder.find_by_tracking_id(unfinished_order_tracking_id)

		case time_frame
		when "thirthy_minutes"
			puts '30 minutes email'
			UserMailer.unfinished_order_30_minutes(unfinished_order_id).deliver
		when "twenty_four_hours"
			puts 'twenty four hours email'
			UserMailer.unfinished_order_24_hours(unfinished_order_id).deliver
		when "ten_days"
			puts 'ten days after'
			UserMailer.unfinished_order_10_days(unfinished_order_id).deliver
		end
	end
end