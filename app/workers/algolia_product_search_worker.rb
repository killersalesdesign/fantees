class AlgoliaProductSearchWorker
  def perform(id, remove)
    p = Product.find(id)
    remove ? p.remove_from_index! : p.index!
  end
end