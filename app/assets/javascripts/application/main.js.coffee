$(document).ready ->
  stickyNavTop  = $(".cart-btn").offset().top
  stickyNavLeft = $(".cart-btn").offset().left
  $(".cart-btn").css("left", stickyNavLeft+"px")
  stickyNav = ->
    scrollTop = $(window).scrollTop()
    if scrollTop > stickyNavTop
      $(".cart-btn").addClass "sticky"
    else
      $(".cart-btn").removeClass "sticky"
    return

  stickyNav()
  $(window).scroll ->
    stickyNav()
    return

  $("body").css("visibility", "visible")


@algolia_search =
  index: null
  page:  0
  client: null

  init: (id, search_key, index) ->
    @client = new AlgoliaSearch(id, search_key)
    @index  = @client.initIndex(index).ttAdapter()
    # @addTypeAhead()

    # @index = new AlgoliaSearch(id, search_key).initIndex(index)
    # @bindEvent()

  bindEvent: ->
    _this = @
    $("input[name='search']").keyup (e) ->
      keyword = $(this).val()
      _this.search_keyword(keyword, _this)

  search_keyword: (keyword, _this) ->
    _this.index.search keyword, ((success, hits) ->
      console.log success, hits
      return
    ),
      hitsPerPage: 16
      page: @page

  addTypeAhead: ->
    template = Hogan.compile('<div class="hit-auto-complete">' +
        '<div class="name">{{{_highlightResult.title.value}}}</div>' +
      '</div>')
    params =
      source: @index
      displayKey: 'title'
      templates: 
        suggestion: (hit) ->
          return template.render(hit)
    $("input[name='search']").typeahead(null, params).on('change', (e) ->
      console.log "onchange: ", $("input[name='search']").val()
    ).on('keypress', (e) ->
      if e.which is 13
        console.log "onsubmit: ", $("input[name='search']").val()
    ).on('typeahead:selected', (e) ->
      console.log 'onselected: ', $("input[name='search']").val()
    )
