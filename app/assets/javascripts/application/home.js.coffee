$(document).ready ->
  $("#diagram-id-1").diagram
    size: "70"
    borderWidth: "6"
    bgFill: "#2F3D51"
    frFill: "#42BD9C"
    textSize: 23
    textColor: "#333"