'use strict';
@app_name = $("html").attr("ng-app")
@App      = angular.module @app_name, ["ngResource", "ng-rails-csrf", "ngRoute", "ui.bootstrap", "angularFileUpload", "flashr", "md5", "ngSanitize"]