App.factory "Order", [ "$resource", ($resource) ->
  Order = $resource "#{Routes.dashboard_orders_path()}/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}


  Order::is_printed = ->
    return (if @printed then "Yes" else "No")

  Order::is_shipped = ->
    return (if @shipped then "Yes" else "No")

  Order::current_status = ->
    status = false
    if @printed
      status = '/assets/printing-img.png'
    if @shipped == 1
      status = '/assets/shipping-img.png'

    return status

  return Order
]