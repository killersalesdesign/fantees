App.factory "Giftcard", [ "$resource", ($resource) ->
  Giftcard = $resource "#{Routes.dashboard_giftcards_path()}/:id",
                        {id: "@id"}


  return Giftcard
]