App.factory "User", [ "$resource", ($resource) ->
  User = $resource "/dashboard/users/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}


  return User
]