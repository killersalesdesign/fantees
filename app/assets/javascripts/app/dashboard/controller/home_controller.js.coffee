@HomeCtrl = [ "$scope", "$http", "$location", "$modal", "Order", ($scope, $http, $location, $modal, Order) ->
  NProgress.start()
  Order.query {limit: 6}, (orders) ->
    $scope.orders = orders
    NProgress.done()
  $scope.search =
    name: ""
    date: ""
    keyword: ""

  $http.get(Routes.latest_dashboard_orders_path()).success((data, status, headers, config) ->
    $scope.products = []
    $.each data, (key, value) ->
      value.start_num   = (if value.start_num then value.start_num else 0)
      value.total_sales = value.start_num + value.total_sold
      value.percent     = (parseFloat(value.total_sales) / parseFloat(value.sales)) * 100
      value.percent     = 100 if value.percent > 100
      $scope.products.push value

  ).error (data, status, headers, config) ->
    console.log "error"


  $scope.show = (order) ->
    $location.path("/orders/#{order.order_id}")

  $scope.runSearch = ->
    if !$scope.search.name && !$scope.search.date && !$scope.search.keyword
      $scope.orders = Order.query()
    else
      $scope.orders = Order.query(search: $scope.search)

  $scope.openModal = (order) ->
    $scope.order = order
    $modal.open
      template: JST["app/dashboard/views/orders/modal"]
      backdrop: true
      windowClass: 'modal-shipping'
      controller: ["$scope", "$modalInstance", "flashr", "COUNTRIES", "order", ($scope, $modalInstance, flashr, countries, order) ->
        $scope.order     = order
        $scope.countries = countries
        $.each countries, (key, value) ->
          $scope.country = value if value.iso is $scope.order.shipping_info.Country

        $scope.selectCountry = (e) ->
          $scope.country = e.country
          $scope.order.shipping_info.Country = e.country.iso

        $scope.submit = ->
          NProgress.start()
          $scope.order.$update (updatedProduct, putResponseHeaders) ->
            flashr.now.success('Shipping Info updated!')
            NProgress.done()

          $modalInstance.dismiss('cancel')

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')
      ]
      resolve:
        order: ->
          $scope.order

  # NProgress.done()
]