@OrdersListCtrl = [ "$scope", "$http", "$location", "Order", ($scope, $http, $location, Order) ->
  NProgress.start()
  $scope.orders = Order.query()
  $scope.search =
    name: ""
    date: ""
    keyword: ""

  $scope.show = (order) ->
    $location.path("/orders/#{order.order_id}")

  $scope.runSearch = ->
    console.log $scope.search
    if !$scope.search.name && !$scope.search.date && !$scope.search.keyword
      $scope.orders = Order.query()
    else
      $scope.orders = Order.query(search: $scope.search)

  NProgress.done()
]


@OrdersShowCtrl = [ "$scope", "$http", "$location", "$routeParams", "$modal", "flashr", "Order", "COUNTRIES", ($scope, $http, $location, $routeParams, $modal, flashr, Order, countries) ->
  NProgress.start()
  $scope.user      = gon.current_user
  $scope.order     = Order.get id: $routeParams.id, (order) ->

    $scope.countries = countries
    $.each countries, (key, value) ->
      $scope.country = value if value.iso is $scope.order.shipping_info.Country

    $http.get Routes.ordered_products_dashboard_order_path(order.id)
    .success (data, status) ->
      $scope.ordered_products = data
    .error (data, status) ->
      flashr.now.error(data.errors)

  updateCountry = (country) ->
    $scope.country = country

  $scope.updateDescription = ->
    NProgress.start()
    $scope.order.$update (updatedProduct, putResponseHeaders) ->
      flashr.now.success('Order Description updated!')
      NProgress.done()

  $scope.openModal = ->
    $modal.open
      template: JST["app/dashboard/views/orders/modal"]
      backdrop: true
      windowClass: 'modal-shipping'
      controller: ["$scope", "$modalInstance", "flashr", "countries", "country", "order", ($scope, $modalInstance, flashr, countries, country, order) ->
        $scope.order     = order
        $scope.countries = countries
        $scope.country   = country

        $scope.selectCountry = (e) ->
          $scope.country = e.country
          $scope.order.shipping_info.Country = e.country.iso

        $scope.submit = ->
          NProgress.start()
          $scope.order.$update (updatedProduct, putResponseHeaders) ->
            flashr.now.success('Shipping Info updated!')
            updateCountry $scope.country
            NProgress.done()

          $modalInstance.dismiss('cancel')

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')
      ]
      resolve:
        order: ->
          $scope.order
        countries: ->
          $scope.countries
        country: ->
          $scope.country

  NProgress.done()
]


@OrdersEditCtrl = [ "$scope", "$http", "$location", ($scope, $http, $location) ->
  NProgress.start()


  NProgress.done()
]
