App.config [ "$routeProvider", "$locationProvider", "$httpProvider", ($routeProvider, $locationProvider, $httpProvider) ->
  # $locationProvider.html5Mode(true)

  $httpProvider.defaults.transformRequest.push (data, headersGetter) ->
    utf8_data = data
    unless angular.isUndefined(data)
      d = angular.fromJson(data)
      d["utf8"] = "&#9731;"
      utf8_data = angular.toJson(d)
    utf8_data

  $routeProvider
  .when "/home",
    template: JST["app/dashboard/views/home/index"]
    controller: "HomeCtrl"

  .when "/orders",
    template: JST["app/dashboard/views/orders/index"]
    controller: "OrdersListCtrl"

  .when "/orders/:id",
    template: JST["app/dashboard/views/orders/show"]
    controller: "OrdersShowCtrl"

  .when "/orders/:id/edit",
    template: JST["app/dashboard/views/orders/edit"]
    controller: "OrdersEditCtrl"

  .when "/giftcards",
    template: JST["app/dashboard/views/giftcards/index"]
    controller: "GiftcardListCtrl"

  .when "/giftcards/:id",
    template: JST["app/dashboard/views/giftcards/show"]
    controller: "GiftcardShowCtrl"

  .when "/profile",
    template: JST["app/dashboard/views/profile/show"]
    controller: "ProfileCtrl"

  .when "/profile/password",
    template: JST["app/dashboard/views/profile/password"]
    controller: "ProfilePasswordCtrl"

  .otherwise redirectTo: '/home'
]