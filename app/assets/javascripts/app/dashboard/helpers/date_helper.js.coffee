angular.module(@app_name).filter "fromNow", ->
  (date) ->
    moment(date).fromNow()

angular.module(@app_name).filter "niceDate", ->
  (date) ->
    moment(date).format("MMM Do YYYY")

angular.module(@app_name).filter "niceDate2", ->
  (date) ->
    moment(date).format("MMMM D, YYYY")

angular.module(@app_name).filter "niceDateTime", ->
  (date) ->
    moment(date).format("MMM DD, YYYY h:mm:ss a")