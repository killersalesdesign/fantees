angular.module(@AppName).constant "STATES", [
  {"abr":"ACT","name":"Australian Capital Territory"},
  {"abr":"NSW","name":"New South Wales"},
  {"abr":"NT","name":"Northern Territory "},
  {"abr":"QLD","name":"Queensland"},
  {"abr":"SA","name":"South Australia"},
  {"abr":"TAS","name":"Tasmania"},
  {"abr":"VIC","name":"Victoria"},
  {"abr":"WA","name":"Western Australia"}
]