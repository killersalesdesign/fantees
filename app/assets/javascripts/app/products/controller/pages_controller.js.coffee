@PagesCtrl = [ "$scope", ($scope) ->
  # 355
  # $scope.height = "auto"
  wrapper = $("#content-wrapper").height()
  page    = $( document ).height() - 355

  if gon.user
    # kissmetrics
    _kmq.push ['identify', gon.user.email]
    _kmq.push ['record', 'User successfully paid!']

  if gon.page
    _kmq.push ['record', 'Viewed Page', {name: gon.page}]

  $scope.height = (if page > wrapper then "#{page}px" else "auto")
]