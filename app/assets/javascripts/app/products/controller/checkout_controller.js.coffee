@CheckoutCtrl = [ "$scope", "$http", "geolocation", "COUNTRIES", "Coupon", ($scope, $http, geolocation, COUNTRIES, Coupon) ->
  $scope.countries = COUNTRIES

  $scope.checkout =
    email: ""
    first_name: ""
    last_name: ""
    phone: ""
    address: ""
    city: ""
    zip: ""
    country: (if gon.website.name is "FanTees-NZ" then "NZ" else "AU")
    state: ""
    cc_number: ""
    month: ""
    year: ""
    cvn: ""

  $scope.country = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  $scope.payment = "CC"
  $scope.total   = 0

  # $scope.total_amount = 0
  # $scope.regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  # $scope.fillEmail = (e) ->
  #   if $scope.regex.test($scope.checkout.email)
  #     _rejoiner.push ['setCartData',
  #       email: $scope.checkout.email
  #       value: $scope.total_amount
  #       totalItems: gon.params.orders.styles.length
  #       returnUrl: '#{gon.root_url}order/rebuild/#{gon.tracking_id}'
  #     ]

  shipping_fee = (intl) ->
    fee        = 0
    add_charge = 0 # internation additional charge
    orders     = gon.params.orders
    total_qty  = 0
    this_price = {i_postage: 0, postage: 0}
    total      = 0
    $.each gon.prices, (k, price) ->
      i = 0
      this_price = price if (price.i_postage > this_price.i_postage) || (price.postage > this_price.postage)

      $.each orders.styles, (key, order) ->
        if parseFloat(order) == parseFloat(price.category_id) && intl
          total += parseFloat(price.amount) * parseFloat(orders.qty[i])
          fee = (if fee > price.i_postage then fee else price.i_postage)
          total_qty += parseFloat(orders.qty[i])

        else if parseFloat(order) == parseFloat(price.category_id)
          total += parseFloat(price.amount) * parseFloat(orders.qty[i])
          fee = (if fee > price.postage then fee else price.postage)
          total_qty += parseFloat(orders.qty[i])

        i++

    if intl
      fee = (if total_qty < gon.cut_off.international then fee else 0)
      add_charge = (total_qty - 1) * gon.cut_off.add_cost if total_qty > 1
    else
      fee = (if total_qty < gon.cut_off.domestic then fee else 0)

    $scope.sub_total = total
    $scope.total = total + fee + add_charge
    # console.log $scope.total, total, fee, add_charge
    return fee + add_charge

  $scope.shipping_fee = shipping_fee(false)
  # console.log $scope.shipping_fee

  $scope.selectCountry = (e) ->
    if e.country.iso != "AU"
      $scope.shipping_fee = shipping_fee(true)
    else
      $scope.shipping_fee = shipping_fee(false)

    $scope.checkout.country = e.country.iso

  $scope.selectPayment = (type) ->
    $scope.payment = type

  # $scope.coupon = Coupon.get code: "123456", (coupon) ->
  #   console.log coupon

  $scope.coupon = {description: "Retrieving description..", error: false, discount: 0, checked: false, btn: "Apply Coupon"}

  $scope.applyCoupon = ->
    $scope.coupon.error    = false
    $scope.coupon.discount = 0
    $scope.checkCoupon ->
      if $scope.coupon.code && $scope.coupon.error is false && !$scope.coupon.check
        $scope.coupon.class = "active"
        $scope.coupon.check = true
        $scope.coupon.btn   = "Successfully Applied"
        $scope.coupon.txtClass = ""
      else if $scope.coupon.code && $scope.coupon.error
        $scope.coupon.class = ""
        $scope.coupon.check = false
        $scope.coupon.btn   = "Apply Coupon"
        $scope.coupon.txtClass = "txtbox-error"
      else if !$scope.coupon.code
        $scope.coupon.class = ""
        $scope.coupon.check = false
        $scope.coupon.btn   = "Apply Coupon"
        $scope.coupon.txtClass = ""

  $scope.checkCoupon = (callback) ->
    if $scope.coupon.code
      NProgress.start()
      Coupon.get code: $scope.coupon.code, (coupon) ->
        unless coupon.code
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if coupon.active != true
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if (coupon.expiration != null && new Date(coupon.expiration) < new Date())
          $scope.coupon.description = "Coupon already expired."
          $scope.coupon.error = true
        else if coupon.limit != null && coupon.usage >= coupon.limit
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else
          $scope.coupon.discount = $scope.sub_total * (coupon.discount / 100)
          $scope.coupon.description = "#{coupon.discount}% discount"

        NProgress.done()
        callback()
    else
      $scope.coupon =
        description: "Retrieving description.."
        error: false
        discount: 0
        code: null

      callback()

  $scope.show_coupon = false
  $scope.toggleCoupon = ->
    $scope.show_coupon = (if $scope.show_coupon is false then true else false)
    $scope.coupon =
      description: "Retrieving description.."
      error:       false
      discount:    0
      checked:     false
      btn:         "Apply Coupon"
      class:       ""
      checked:     false
      txtClass:    ""

  $scope.coupons = []
  $scope.addCoupon = ->
    $scope.coupons.push {value: ""}

  $scope.removeCoupon = (index) ->
    $scope.coupons.splice(index, 1)

  $(document).ready ->
    $("body").attr("style","background:url(/assets/bg-body-trick.png)repeat-y scroll right 0 rgba(0, 0, 0, 0) !important;font-family:'Lato';")

    $("input[type='radio']").click ->
      if $(this).attr("value") is "creditcard"
        $(".checkout-credit-card").show "fast"
        $(".checkout-credit-card-paypal").hide "fast"
        $(".radio01").addClass "radioSelected"
        $(".radio02").removeClass "radioSelected"
        $("input[name='mode']").attr "value", "credit"
      if $(this).attr("value") is "paypal"
        $(".radio01").removeClass "radioSelected"
        $(".radio02").addClass "radioSelected"
        $(".checkout-credit-card").hide "fast"
        $(".checkout-credit-card-paypal").show "fast"
        $(".radio01").removeClass "radioCurrent"
        $("input[name='mode']").attr "value", "paypal"

    # used for rejoiner
    # total = 0
    # orders = gon.params.orders
    # $.each orders.styles, (index, value) ->
    #   $scope.total_amount = $scope.total_amount + (parseFloat(orders.price[index]) * parseFloat(orders.qty[index]))
    #   _rejoiner.push ['setCartItem',
    #     product_id: gon.product.id
    #     name: gon.product.title
    #     item_qty: parseFloat(orders.qty[index])
    #     # size: orders.sizes[index]
    #     category: value
    #     price: orders.price[index]
    #   ]

  $(document).on "submit", "#new_order", ->
    error = false
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    $.each $scope.checkout, (key, value) ->
      if $scope.payment == "CC" && ["cc_number", "exp_month", "year", "cvn"].indexOf(key) != -1 && value is ""
        error = true
        $("input[name='cc_number']").addClass("txtbox-error")
        $("input[name='month']").addClass("txtbox-error")
        $("input[name='year']").addClass("txtbox-error")
        $("input[name='cvn']").addClass("txtbox-error")
      else if $scope.payment == "PP" && ["cc_number", "month", "year", "cvn"].indexOf(key) != -1
        $("input[name=#{key}]").removeClass("txtbox-error")
      else if value is ""
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else if key is "email" and !regex.test(value)
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else
        $("input[name=#{key}]").removeClass("txtbox-error")

    # checking for coupon
    if ($scope.coupon.code && !$scope.coupon.check) || ($scope.coupon.check && !$scope.coupon.code) || ($scope.coupon.error && $scope.show_coupon)
      $("input[name=coupon]").addClass("txtbox-error")
      $("#coupon-desc").addClass("txtbox-error")
      # $(".checkout-coupon-btn").removeClass("active")
      $scope.coupon.check = false
      $scope.coupon.class = ""
      $scope.coupon.btn   = "Apply Coupon"
      $scope.$apply()
    else
      $("input[name=coupon]").removeClass("txtbox-error")
      $("#coupon-desc").removeClass("txtbox-error")

    # checking for coupons
    # if $scope.coupons.length > 0
    #   $.each $('input[name^=coupon]'), (key, value) ->
    #     unless $(value).val()
    #       error = true
    #       $(value).addClass("txtbox-error")
    #     else
    #       $(value).removeClass("txtbox-error")

    #       $.each $('input[name^=coupon]'), (k, v) ->
    #         if key != k && $(value).val() == $(v).val()
    #           error = true
    #           $(value).addClass("txtbox-error")

    if error
      return false
    else
      return true
]


@CheckoutCtrlOld = [ "$scope", "$http", "geolocation", "COUNTRIES", "STATES", ($scope, $http, geolocation, COUNTRIES, STATES) ->
  $scope.countries = COUNTRIES
  # $scope.country   = COUNTRIES[0]
  $scope.country   = {name: "Australia", iso: "AU"}
  $scope.states    = STATES
  $scope.state     = STATES[0]

  $scope.checkout  =
    address:    gon.info.address
    city:       gon.info.city
    state:      gon.info.state || $scope.state.abr
    country:    gon.info.country || $scope.country.iso
    zip:        gon.info.zip
    first_name: gon.info.first_name
    last_name:  gon.info.last_name
    email:      gon.info.email
    cc_number:  gon.info.cc_number
    month:      gon.info.month
    year:       gon.info.year
    cvn:        gon.info.cvn
    phone:      gon.info.phone

  # geolocation.getLocation().then (data) ->
  #   coords =
  #     lat:  data.coords.latitude
  #     long: data.coords.longitude

  #   $.getJSON "//maps.googleapis.com/maps/api/geocode/json?latlng=#{coords.lat},#{coords.long}&sensor=true", (data) ->
  #     if data && data.status == "OK"
  #       results = data.results.pop()
  #       try
  #         country = results.address_components[0].short_name
  #         $.each COUNTRIES, (key, value) ->
  #           $scope.country = value if value.iso == country

  #         $scope.checkout.country = gon.info.country || $scope.country.iso
  #         $scope.$apply()
  #     else
  #       $scope.country = COUNTRIES[0]

  #   coords

  $scope.selectCountry = (e) ->
    $scope.checkout.country = e.country.iso

  $scope.selectState = (e) ->
    $scope.checkout.state = e.state.abr

  $(document).on "submit", "#new_order", ->
    error = false
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    $.each $scope.checkout, (key, value) ->
      unless value
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else if ["month", "year", "zip"].indexOf(key) isnt -1 and isNaN(value)
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else if key is "email" and !regex.test(value)
        error = true
        $("input[name=#{key}]").addClass("txtbox-error")
      else
        $("input[name=#{key}]").removeClass("txtbox-error")

    if error
      return false
    else
      return true

]