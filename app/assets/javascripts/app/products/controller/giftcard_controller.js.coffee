@GiftCardCtrl = ["$scope", "$http", "geolocation", "COUNTRIES", "Coupon", "Cart", ($scope, $http, geolocation, COUNTRIES, Coupon, Cart) ->
  console.log "aw wewe"
  $scope.countries = COUNTRIES
  domestic         = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  shipping         = {}
  total_qty        = 0
  $scope.payment   = gon.payment
  $scope.country   = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  $scope.shipping  = 0
  $scope.regex     = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  $scope.giftcard = gon.giftcard
  # default values
  $scope.checkout = gon.order

    # email: ""
    # first_name: ""
    # last_name: ""
    # phone: ""
    # address: ""
    # city: ""
    # zip: ""
    # country: (if gon.website.name is "FanTees-NZ" then "NZ" else "AU")
    # state: ""
    # cc_number: ""
    # month: ""
    # year: ""
    # cvn: ""

  # all_cart_index
  $http method: "GET", url: Routes.all_cart_index_path()
    .success (data, status, headers, config) ->
      initValues(data)

      initCheckoutDetails(data.unfinished_order)

      $scope.total    = 0
      $scope.subtotal = getSubtotal(data.items)

      $scope.coupon = {code: "", description: "", discount: 0, percent: 0}
      $scope.show_coupon = false

      if gon.coupon && gon.coupon.code
        $scope.coupon.code        = gon.coupon.code
        $scope.show_coupon        = true
        $scope.coupon.discount    = $scope.subtotal * (gon.coupon.discount / 100)
        $scope.coupon.percent     = gon.coupon.discount
        $scope.coupon.description = "#{gon.coupon.discount}% discount"

    .error (data, status, headers, config) ->
      # error message here

  $scope.removeItem = (id) ->
    Cart.delete id: id, (data) ->
      initValues(data)

      $scope.subtotal = getSubtotal(data.items)
      updateCartCounter()
      # counter update

  $scope.ci_country = $scope.country.name
  $scope.selectCountry = (e) ->
    result = $.grep(COUNTRIES, (e) ->
      e.name is $scope.ci_country
    )

    $scope.country = result[0] if result.length > 0

    # update shipping cost
    if domestic.iso is $scope.country.iso
      shipping_cost   = (if domestic.iso is $scope.country.iso then shipping.domestic else shipping.international)
      shipping_cutoff = shipping.cutoff
    else
      shipping_cost   = shipping.international
      shipping_cutoff = shipping.intl_cutoff

    $scope.shipping = (if total_qty < shipping_cutoff then shipping_cost else 0)

  $scope.updateCheckout = (e) ->
    console.log e

  initCheckoutDetails = (details) ->
    $scope.checkout.email       = details.email
    $scope.checkout.first_name  = details.first_name
    $scope.checkout.last_name   = details.last_name
    $scope.checkout.address     = details.address
    $scope.checkout.city        = details.town
    $scope.checkout.state       = details.state
    $scope.checkout.zip         = details.postal
    $scope.ci_country           = (if $scope.ci_country then $scope.ci_country else details.country)
    $scope.checkout.phone       = details.phone

  $scope.updateUnfinishedOrder = (param) ->
    $http method: "POST", url: Routes.update_unfinished_orders_path(), params: param
      .success (data, status, headers, config) ->
        console.log data.msg
      .error (data, status, headers, config) ->
        console.log data.msg

  $scope.fieldBlur = ->

    # check all textboxes
    $("input").each (index, elem) ->
      elemId = $.trim($(elem).prop("id"))
      elemVal = $(elem).val()
      if elemId is "ci-address"
        $scope.checkout.address = elemVal
      else if elemId is "ci-town"
        $scope.checkout.city = elemVal
      else if elemId is "ci-state"
        $scope.checkout.state = elemVal
      else if elemId is "ci-pcode"
        $scope.checkout.zip = elemVal
      else if elemId is "ci-country"
        $scope.checkout.country = elemVal

    params            = {}
    params.email      = $scope.checkout.email if $scope.checkout.email isnt "" and $scope.regex.test($scope.checkout.email)
    params.first_name = $scope.checkout.first_name if $scope.checkout.first_name isnt ""
    params.last_name  = $scope.checkout.last_name if $scope.checkout.last_name isnt ""
    params.address    = $scope.checkout.address if $scope.checkout.address isnt ""
    params.town       = $scope.checkout.city if $scope.checkout.city isnt ""
    params.state      = $scope.checkout.state if $scope.checkout.state isnt ""
    params.postal     = $scope.checkout.zip if $scope.checkout.zip isnt ""
    params.country    = $scope.ci_country if $scope.ci_country isnt ""
    params.phone      = $scope.checkout.phone if $scope.checkout.phone isnt ""

    $scope.updateUnfinishedOrder params

  # updates for cart 12.16.2014
  $scope.getQuantity = (item) ->
    quantity = []
    $.each item.stocks, (key, value) ->
      quantity = value.quantities if value.size is item.size

    quantity

  $scope.getSizes = (item) ->
    sizes = []
    $.each item.stocks, (key, value) ->
      sizes.push value.size

    sizes

  $scope.updateSize = (item) ->
    quantities    = $scope.getQuantity(item)
    item.quantity = 1 if item.quantity > quantities.length

    $scope.updateCart(item)

  $scope.updateCart = (item) ->
    NProgress.start()
    $(".place-order-now").attr("disabled", "disabled")

    $http.put Routes.cart_path(item.id), {quantity: item.quantity, size: item.size}
    .success (data, status, headers, config) ->
      if domestic.iso is $scope.country.iso
        shipping_cost   = (if domestic.iso is $scope.country.iso then shipping.domestic else shipping.international)
        shipping_cutoff = shipping.cutoff
      else
        shipping_cost   = shipping.international
        shipping_cutoff = shipping.intl_cutoff

      $scope.subtotal = getSubtotal($scope.items)
      $scope.shipping = (if total_qty < shipping_cutoff then shipping_cost else 0)

      $(".place-order-now").removeAttr("disabled")
      NProgress.done()
    .error (data, status, headers, config) ->
      $(".place-order-now").removeAttr("disabled")
      NProgress.done()

  # end updates for cart 12.16.2014

  # $scope.selectCountry = (e) ->
  #   $scope.checkout.country = e.country.iso
  #   if $scope.total_qty >= $scope.cutoff
  #     $scope.shipping = 0
  #   else
  #     $scope.shipping = (if $scope.country.iso is domestic.iso then shipping.domestic else shipping.international)
  #     $scope.shipping += (($scope.total_qty - 1) * $scope.additional_item) if $scope.country.iso isnt domestic.iso and $scope.total_qty > 1

  getSubtotal = (items) ->
    subtotal  = 0
    total_qty = 0
    $.each items, (key, item) ->
      total_qty += item.quantity
      subtotal  += (parseFloat(item.info.amount) * parseFloat(item.quantity))

    subtotal

  updateCartCounter = ->
    count = parseInt($("#cart-count").text()) - 1

    $("#cart-count").css("opacity", "0") if count < 1
    $("#cart-count").text(count)

  initValues = (data) ->
    shipping         = data.shipping
    $scope.total_qty = data.total_qty
    $scope.items     = data.items
    $scope.cutoff    = (if $scope.country.iso is domestic.iso then shipping.cutoff else shipping.intl_cutoff)
    $scope.delivery_date   = data.delivery_date
    $scope.additional_item = data.shipping.additional_item

    if $scope.total_qty >= $scope.cutoff
      $scope.shipping = 0
    else
      $scope.shipping = (if $scope.country.iso is domestic.iso then shipping.domestic else shipping.international)
      $scope.shipping += (($scope.total_qty - 1) * $scope.additional_item) if $scope.country.iso isnt domestic.iso and $scope.total_qty > 1
      # data.additional_item



  $scope.TotalwithGiftCards = (shipping,subtotal, giftcard) ->
    total = shipping + subtotal - giftcard
    if total < 0
      return 0
    else
      return total
  $(document).on "submit", "#new_order", ->
    error = false
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    delete $scope.checkout.cc_number
    delete $scope.checkout.cvn

    $.each $scope.checkout, (key, value) ->
      if value is "" && (key != "phone" && key != "coupon_code")
        unless $("input[name='order[#{key}]']").val()
          console.log "mao ni ang error", key
          error = true
          $("input[name='order[#{key}]']").addClass("txtbox-error")
      else if key is "email" and !regex.test(value)
        error = true
        $("input[name='order[#{key}]']").addClass("txtbox-error")
      else
        $("input[name='order[#{key}]']").removeClass("txtbox-error")


    console.log "error", error
    if error
      return false
    else
      return true

]