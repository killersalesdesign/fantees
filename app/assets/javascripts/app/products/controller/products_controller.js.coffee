@ProductShowCtrl = [ "$scope", "$http", "$location", "$modal", "flashr",  ($scope, $http, $location, $modal, flashr) ->
  NProgress.start()
  slug = window.location.pathname.replace(/\//g, "")
  $scope.test
  $http.get("/products/api/#{slug}").success((data, status, headers, config) ->
    init(data)
    init_county(data.product.date)

    # stop loader
    NProgress.done()
  ).error (data, status, headers, config) ->
    NProgress.done()

  upsells = null

  $http.get("/products/upsell/#{slug}").success((data, status, headers, config) ->
    upsells = data

  ).error (data, status, headers, config) ->
    # $scope.upsells = null
    console.log "no upsell"

  init = (data) ->
    $scope.prices = []
    if data.product.status != 0
      $.each data.prices, (key, value) ->
        has_stock = false
        $.each data.stocks, (index, stock) ->
          has_stock = true if stock.category_id is value.category_id && stock.amount > 0

        if has_stock
          value.name = "#{value.type} - $#{value.amount.toFixed(2)}"
          $scope.prices.push value

    else
      $.each data.prices, (key, value) ->
        value.name = "#{value.type} - $#{value.amount.toFixed(2)}"
        $scope.prices.push value

    if $scope.prices.length is 0
      $(".btn-big-custom").remove()
      $.each data.prices, (key, value) ->
        value.name = "#{value.type} - $#{value.amount.toFixed(2)}"
        $scope.prices.push value

    $scope.data    = data
    $scope.price   = $scope.prices[0]
    $scope.product = data.product
    $scope.stocks  = data.stocks
    $scope.gallery = data.gallery
    # $scope.selected   = $scope.categories[0]

    # kissmetrics
    _kmq.push ['record', 'Viewed Product', {name: data.product.title}]

    # $scope.images  = []
    type_images    = []
    $.each data.gallery, (key, value) ->
      # $scope.images.push value if $scope.price.category_id == value.category_id || value.category_id == null
      type_images.push value if $scope.price.category_id == value.category_id || value.category_id == null

    $scope.images     = data.gallery
    $scope.currentImg = type_images[0]
    # $('#big-image').elevateZoom()
    currentIndex      = 0
    $scope.showNextPrev = (if $scope.images.length > 1 then true else false)

    # hotfix for search select style
    if gon.category_id != null
      category = {selected: {}}
      $.each $scope.prices, (k, v) ->
        if parseInt(gon.category_id) == v.category_id
          category.selected = v
          $scope.price      = v
      $scope.selectCategory(category)
      $scope.openModal()

    # fix for select2 dropdown
    setTimeout(->
      $(".select2-chosen").text($scope.price.name)
    , 50)

  $scope.selectCategory = (category) ->
    $scope.selected = category.price
    setTimeout(->
      category_id = category.selected.category_id
      type_images = $scope.gallery.filter (item, i) ->
        return item if item.category_id == category_id || item.category_id == null
      $scope.currentImg = type_images[0]
      setTimeout(->
        $('.zoomWindowContainer div').css("background-image","url(#{$scope.currentImg.original})")
      , 300)
      $scope.$apply()
      $(".select2-chosen").text(category.selected.name)
    , 50)

  $scope.current = (id) ->
    if id is $scope.currentImg.id
      return "current"

  $scope.selecteImage = (image, index) ->
    currentIndex = index
    $scope.currentImg = image

  $(document).on "click", ".thumbnail-images img", ->
    # console.log "click?", $("")
    setTimeout(->
      $('.zoomWindowContainer div').css("background-image","url(#{$scope.currentImg.original})")
    , 300)

  $scope.next = ->
    currentIndex++
    currentIndex = $scope.images.length - 1 if currentIndex >= $scope.images.length - 1
    $scope.currentImg = $scope.images[currentIndex]

  $scope.back = ->
    currentIndex--
    currentIndex = 0 if currentIndex <= 0
    $scope.currentImg = $scope.images[currentIndex]

  init_county = (date) ->
    county = $('#my-count-down').county
      endDateTime: new Date(date)
      reflection: false
      animation: 'scroll'
      theme: 'white'
      timezone: +10

  $scope.openModal = ->
    $modal.open
      template: JST["app/products/views/products/modal"]
      backdrop: true
      windowClass: 'modal'
      controller: ["$scope", "$modalInstance", "Category", "Cart", "product", "price", "COUNTRIES", "data", "$modal", ($scope, $modalInstance, Category, Cart, product, price, countries, gon, $modal) ->
        $scope.upsells = upsells

        $scope.upsellData = []
        $scope.prices = gon.prices
        $scope.stocks = gon.stocks
        $scope.checkout_now = false

        $scope.delivery_date =
          international: gon.international
          domestic:      gon.domestic

        Category.query product_id: gon.product.id, (categories) ->
          $scope.categories = []
          $scope.flag_cat   = []
          angular.forEach categories, (value, key) ->
            if gon.product.status != 0 # for printing / in stock
              has_stock = false

              $.each gon.stocks, (index, stock) ->
                has_stock = true if stock.category_id is value.id && stock.amount > 0

              if has_stock
                angular.forEach $scope.prices, (price, k) ->
                  value.price = price.amount if price.category_id is value.id

                # filter sizes
                sizes = []
                angular.forEach gon.stocks, (size, k) ->
                  # sizes.push {label: size.size, amount: size.amount} if size.category_id == value.id && size.amount > 0
                  if size.category_id == value.id && size.amount > 0
                    quantities = []
                    i = 1
                    while i <= size.amount
                      quantities.push i
                      i++
                    sizes.push label: size.size, quantities: quantities

                value.sizes = sizes
                value.product_id = gon.product.id

                $scope.categories.push value
                $scope.flag_cat.push value
            else # if still in campaign
              angular.forEach $scope.prices, (price, k) ->
                value.price = price.amount if price.category_id is value.id

              quantities = []
              i = 1
              while i <= 25
                quantities.push i
                i++

              sizes = []
              angular.forEach value.sizes, (size, k) ->
                sizes.push label: size, quantities: quantities

              value.sizes = sizes

              $scope.categories.push value
              $scope.flag_cat.push value

            # if value.id is price.category_id
            #   $scope.selected = value
            #   $scope.sizes   = value.sizes
            #   $scope.product  = product
            #   $scope.qty      = 1
            #   $scope.size     = value.sizes[0]
            #   $scope.selected.total_price = value.price * $scope.qty

          $scope.product    = product
          $scope.more_category = []
          $scope.more_qty   = []
          $scope.more_size  = []
          $scope.more_price = []

          moreCheckOut()

        # init modal labels
        $scope.title  = "Select Your Size & Style"
        $scope.action = "/products/paypal_checkout"

        $scope.countries = countries
        $scope.country   = ""
        $scope.addMore   = []
        $scope.csrf      = $('meta[name=csrf-token]').attr('content')
        $scope.checkout  = 'order'
        $scope.gon_user  = gon.current_user
        $scope.user      = gon.current_user

        # $(document).on "submit", "#modal-form", ->
        #   error = false
        #   $("input[name='orders[qty][]']").each (key, value) ->
        #     if $(value).val() == 0 || $(value).val() == "0" || !$(value).val()
        #       error = true
        #       $(value).addClass("txtbox-error")
        #     else
        #       $(value).removeClass("txtbox-error")


        #   if gon.product.status != 0
        #     $.each $scope.addMore, (key, value) ->
        #       console.log "start"
        #       $.each $scope.addMore, (k, v) ->
        #         if key != k && value.id == v.id && $scope.more_size[key].label == $scope.more_size[k].label
        #           $(".row#{key}").addClass("txtbox-error")
        #           $(".row#{k}").addClass("txtbox-error")
        #           error = true
        #         else if key != k && value.id != v.id && $scope.more_size[key].label != $scope.more_size[k].label
        #           console.log value.id, v.id
        #           console.log $scope.more_size[key].label, $scope.more_size[k].label
        #           $(".row#{key}").removeClass("txtbox-error")
        #           $(".row#{k}").removeClass("txtbox-error")

        #   if error
        #     $("#modal-error").show()
        #     return false
        #   else
        #     $("#modal-error").hide()
        #     return true

        checkForm = ->
          error = false
          $("input[name='orders[qty][]']").each (key, value) ->
            if $(value).val() == 0 || $(value).val() == "0" || !$(value).val()
              error = true
              $(value).addClass("txtbox-error")
            else
              $(value).removeClass("txtbox-error")


          if gon.product.status != 0
            $.each $scope.addMore, (key, value) ->
              # console.log "start"
              $.each $scope.addMore, (k, v) ->
                if key != k && value.id == v.id && $scope.more_size[key].label == $scope.more_size[k].label
                  $(".row#{key}").addClass("txtbox-error")
                  $(".row#{k}").addClass("txtbox-error")
                  error = true
                else if key != k && value.id != v.id && $scope.more_size[key].label != $scope.more_size[k].label
                  console.log value.id, v.id
                  console.log $scope.more_size[key].label, $scope.more_size[k].label
                  $(".row#{key}").removeClass("txtbox-error")
                  $(".row#{k}").removeClass("txtbox-error")

          if error
            $("#modal-error").show()
            return false
          else
            $("#modal-error").hide()
            return true

        $scope.cart_checkout = ->
          NProgress.start()
          if checkForm()
            toCart( ->
              window.location = "/cart"
              # NProgress.done()
            )

        $scope.paypal = ->
          _kmq.push(['record', 'Paying using PayPal'])
          # $scope.action = "/products/paypal_checkout"
          # $modalInstance.dismiss('cancel')
          NProgress.start()
          if checkForm()
            toCart( ->
              window.location = "/cart/checkout?payment=PP"
              # NProgress.done()
            )

        $scope.eway = ->
          # $scope.action = "/products/set_checkout_session"
          # $modalInstance.dismiss('cancel')
          NProgress.start()
          if checkForm()
            toCart( ->
              window.location = "/cart/creditcard"
              # NProgress.done()
            )

        $scope.next = ->
          $scope.action   = "/products/eway_checkout"
          # $scope.title    = "Checkout via Credit Card"
          # $scope.checkout = 'cc'

        $scope.back = ->
          $scope.action   = "/products/paypal_checkout"
          $scope.title    = "Select Your Size"
          $scope.checkout = 'order'

        $scope.selectType = (e) ->
          $.each gon.prices, (key, value) ->
            e.selected.price = value.amount if value.category_id is e.selected.id

          $scope.selected = e.selected
          $scope.size = $scope.selected.sizes[0]

        $scope.selectType2 = (e) ->
          $scope.addMore[e.$index] = e.c
          flag = false
          $.each e.c.sizes, (key, value) ->
            if $scope.more_size[e.$index].label == value.label
              flag = true
              $scope.more_size[e.$index] = value
              $scope.updateSize(e)

          $scope.more_size[e.$index] = e.c.sizes[0] unless flag

          $.each gon.prices, (key, value) ->
            if value.category_id is e.c.id
              e.c.price = value.amount

        $scope.updateSize = (e) ->
          quantities  = $scope.more_size[e.$index].quantities
          current_qty = $scope.more_qty[e.$index]
          quantity    = 1
          $.each quantities, (key, value) ->
            quantity = value if value == $scope.more_qty[e.$index]

          e.c.qty = quantity
          $scope.more_qty[e.$index] = quantity

        $scope.submit = ->
          $modalInstance.dismiss('cancel')

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

        $scope.updateQty = (e, index) ->
          try
            e.c.qty = $scope.more_qty[index]

        # $scope.more_qty   = []
        # $scope.more_size  = []
        # $scope.more_price = []

        moreCheckOut = ->
          if $scope.addMore.length == 0
            $.each $scope.categories, (key, value) ->
              if value.id == price.category_id

                add_more     = value
                add_more.qty = 1

                $scope.addMore.push add_more
                $scope.more_category.push $scope.categories
                $scope.more_qty.push 1
                $scope.more_size.push add_more.sizes[0]
          else
            add_more     = $scope.categories[0]
            add_more.qty = 1

            $scope.addMore.push add_more
            $scope.more_category.push angular.copy $scope.categories
            $scope.more_qty.push 1
            $scope.more_size.push add_more.sizes[0]

            # updateStyes($scope.more_category.length - 1)





        updateStyes = (index = "") ->
          # console.log $scope.addMore
          unless index

          else
            $scope.more_category[index] = $scope.flag_cat.filter (item) ->
              not_in_array = true
              add_more_ids = []

              # return if category has more than 1 sizes
              $.each $scope.addMore, (key, value) ->
                if key == index

                  if item.id != value.id && item.sizes.length > 1
                    not_in_array = false
                  else if item.id != value.id && item.sizes.length <= 1
                    not_in_array = true
                  else
                    not_in_array = false
                else
                  add_more_ids.push value.id

                true
              item unless not_in_array

        $scope.moreCheckOut = ->
          moreCheckOut()

        $scope.upsellQty = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

        $scope.removeUpsell = (idx) ->
          $scope.upsellData.splice(idx, 1)
        $scope.selUpSize = (catId, categories) ->
          sizes = []
          $.each categories, (key, value) ->
            if catId is value.category_id
              sizes = value.sizes.map (size) ->
                if size.size_name isnt undefined
                  return size.size_name
                else
                  return size.name
          return sizes

        $scope.selUpCategory = (catId, categories) ->
          category = categories.map (cat) ->
            return {id: cat.category_id, name: cat.type}
          return category

        $scope.updateSelPrice = (i) ->
          newArray = $scope.upsellData[i].categories.filter (filter) ->
            if filter.category_id is $scope.upsellData[i].category_id
              return filter
          $scope.upsellData[i].price = newArray[0].amount
          $scope.upsellData[i].size = newArray[0].sizes[0].name

        $scope.moreCheckOutUpsell = (up)->
          if up.prices[0].sizes[0].size_name isnt undefined
            size = up.prices[0].sizes[0].size_name
          else
            size = up.prices[0].sizes[0].name
          newUpsell =
            product_id: up.id
            category_id: up.prices[0].category_id
            title: up.product.title
            size: size
            quantity: 1
            price: up.prices[0].amount
            categories: up.prices
          $scope.upsellData.push newUpsell


        $scope.remove = (e) ->
          $scope.addMore.splice(e.$index, 1)

        $scope.removeItem = (idx) ->
          $scope.addMore.splice(idx, 1)

        $scope.removeStyle = ->
          $scope.addMore.pop()

        $scope.addToCart = ->
          NProgress.start()
          toCart( ->
            # callback here
            $modalInstance.dismiss('cancel')
            NProgress.done()
            $modal.open
              template: JST["app/products/views/products/added_to_cart"]
              backdrop: true
              windowClass: 'modal'
              controller: ["$scope", "$modalInstance", "cart", "upsells", ($scope, $modalInstance, cart, upsells) ->
                $scope.upsells = upsells
                $scope.item = cart.items[cart.items.length - 1]
                $scope.upsellQty = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
                $scope.upsellData = []
                $scope.cart = cart
                $scope.showAlert = false
                CheckOutUpsell = (up)->
                  if up.prices[0].sizes[0].size_name isnt undefined
                    size = up.prices[0].sizes[0].size_name
                  else
                    size = up.prices[0].sizes[0].name
                  newUpsell =
                    product_id: up.id
                    category_id: up.prices[0].category_id
                    title: up.product.title
                    size: size
                    quantity: 1
                    price: up.prices[0].amount
                    categories: up.prices
                  $scope.upsellData.push newUpsell

                if $scope.upsells.length isnt 0
                  CheckOutUpsell($scope.upsells[0])

                $scope.selUpSize = (catId, categories) ->
                  sizes = []
                  $.each categories, (key, value) ->
                    if catId is value.category_id
                      sizes = value.sizes.map (size) ->
                        if size.size_name isnt undefined
                          return size.size_name
                        else
                          return size.name
                  return sizes

                $scope.selUpCategory = (catId, categories) ->
                  category = categories.map (cat) ->
                    return {id: cat.category_id, name: cat.type}
                  return category

                $scope.updateSelPrice = (i) ->
                  newArray = $scope.upsellData[i].categories.filter (filter) ->
                    if filter.category_id is $scope.upsellData[i].category_id
                      return filter
                  console.log "awa", $scope.upsellData[i].size = newArray[0].sizes[0].name
                  $scope.upsellData[i].price = newArray[0].amount
                  $scope.upsellData[i].size = newArray[0].sizes[0].name


                $scope.addCartUpsell = () ->
                  $scope.showAlert = false
                  toCart()
                toCart = () ->
                  $.each $scope.upsellData, (key, value) ->
                    cart_items.push
                      product_id: value.product_id,
                      category_id: value.category_id,
                      quantity: value.quantity,
                      size: value.size
                      price: value.price
                    $scope.cart.items = cart_items
                  $scope.cart.$save (cart, putResponseHeaders) ->
                    $scope.showAlert = true
                    $scope.newlyAdded = angular.copy $scope.upsellData[0].title
                    if $scope.upsellData.length isnt 0
                      $scope.upsellData.splice(0, 1)
                    if $scope.upsells.length isnt 0
                      $scope.upsells.splice(0, 1)
                      CheckOutUpsell($scope.upsells[0])
                    window.location = "/cart"


              ]
              resolve:
                cart: ->
                  $scope.cart
                upsells: ->
                  $scope.upsells
          )

        $scope.cart = new Cart(items: [])
        cart_items  = []
        toCart = (callback) ->
          $.each $scope.addMore, (key, value) ->
            cart_items.push
              product_id: gon.product.id,
              category_id: value.id,
              quantity: $scope.more_qty[key],
              size: $scope.more_size[key].label
              price: value.price
            $scope.cart.items = cart_items

          $.each $scope.upsellData, (key, value) ->
            cart_items.push
              product_id: value.product_id,
              category_id: value.category_id,
              quantity: value.quantity,
              size: value.size
              price: value.price
            $scope.cart.items = cart_items
          # console.log $scope.cart.items
          $scope.cart.$save (cart, putResponseHeaders) ->
            callback()
          # $modalInstance.dismiss('cancel')

        $(document).on "click", "#add-to-cart", ->
          cat_id = $scope.addMore[0].id
          img    = "/assets/thumbnail-default.jpg"
          $.each gon.gallery, (k, value) ->
            img = value.thumb if value.category_id is cat_id

          $("#cart-item-box img").attr("src", img)
          $("#cart-item-box p").text(gon.product.title)
          $("#cart-item-box").slideDown()

          $(document).off "click", "#add-to-cart"
          setTimeout(->
            count = parseInt($scope.addMore.length) + parseInt($("#cart-num-main").text())
            $("#cart-item-box").slideUp()
            $(".cart-num").text count
            $(".cart-num").css "opacity", "1" if parseInt($(".cart-num").text()) isnt 0
          , 1000)

      ]
      resolve:
        product: ->
          $scope.product
        price: ->
          $scope.price
        data: ->
          $scope.data

]