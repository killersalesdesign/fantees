@CartListCtrl = ["$scope", "$http", "geolocation", "COUNTRIES", "Coupon", "Cart", "Giftcard", ($scope, $http, geolocation, COUNTRIES, Coupon, Cart, Giftcard) ->
  $scope.countries = COUNTRIES
  domestic         = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  shipping         = {}
  total_qty        = 0
  $scope.payment   = gon.payment
  $scope.country   = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  $scope.shipping  = 0

  $http method: "GET", url: Routes.all_cart_index_path()
    .success (data, status, headers, config) ->
      initValues(data)
      # console.log data
      $scope.total    = 0
      $scope.subtotal = getSubtotal(data.items)

    .error (data, status, headers, config) ->
      # error message here
      console.log "error!"

  getSubtotal = (items) ->
    subtotal  = 0
    total_qty = 0
    $.each items, (key, item) ->
      total_qty += item.quantity
      subtotal += (parseFloat(item.info.amount) * parseFloat(item.quantity))

    subtotal

  # updates for cart 12.16.2014
  $scope.getQuantity = (item) ->
    quantity = []
    $.each item.stocks, (key, value) ->
      quantity = value.quantities if value.size is item.size

    quantity

  $scope.getSizes = (item) ->
    sizes = []
    $.each item.stocks, (key, value) ->
      sizes.push value.size

    sizes

  $scope.updateSize = (item) ->
    quantities    = $scope.getQuantity(item)
    item.quantity = 1 if item.quantity > quantities.length

    $scope.updateCart(item)

  setCategories = (items) ->
    $.each items, (key, value) ->
      $.each value.categories, (k, v) ->
        value.category = v if value.category_id is v.category_id

  $scope.updateCategory = (item) ->
    item.category_id = item.category.category_id

    $scope.updateCart(item)
    # console.log $scope.item

  onInstockUpdate = (data) ->
    # to be updated - keenan
    # $.each data.items, (key, value) ->
    #   if value.instock is true
    #     has_size = false
    #     has_qty  = false
    #     $.each value.stocks, (index, stock) ->
    #       console.log stock
    #       if stock.size == value.size
    #         has_size = true
    #         has_qty  = true if stock.quantities.length >= value.quantity
    #     console.log has_qty, has_size
    #     unless has_size || has_size
    #       console.log "SUD?"

    data

  cc_link = $(".checkout-btn a.green").attr("href")
  pp_link = $(".checkout-btn a.popup-checkout-button").attr("href")
  $scope.updateCart = (item) ->
    NProgress.start()
    $(".checkout-btn a.green").removeAttr("href")
    $(".checkout-btn a.popup-checkout-button").removeAttr("href")

    $http.put Routes.cart_path(item.id), {quantity: item.quantity, size: item.size, category_id: item.category_id}
    .success (data, status, headers, config) ->
      result = onInstockUpdate(data)
      initValues(result)

      $scope.subtotal = getSubtotal($scope.items)
      $scope.shipping = (if total_qty < shipping.cutoff then shipping.domestic else 0)

      $(".checkout-btn a.green").attr("href", cc_link)
      $(".checkout-btn a.popup-checkout-button").attr("href", pp_link)
      NProgress.done()
    .error (data, status, headers, config) ->
      $(".checkout-btn a.green").attr("href", cc_link)
      $(".checkout-btn a.popup-checkout-button").attr("href", pp_link)
      NProgress.done()

  # end updates for cart 12.16.2014

  $scope.removeItem = (id) ->
    NProgress.start()
    Cart.delete id: id, (data) ->
      initValues(data)

      $scope.subtotal = getSubtotal(data.items)
      updateCartCounter()
      NProgress.done()
      # counter update

  updateCartCounter = ->
    count = parseInt($("#cart-num-main").text()) - 1

    $(".cart-num").css("opacity", "0") if count < 1
    $(".cart-num").text(count)

  initValues = (data) ->
    shipping         = data.shipping
    $scope.total_qty = data.total_qty
    $scope.items     = data.items
    $scope.cutoff    = (if $scope.country.iso is domestic.iso then shipping.cutoff else shipping.intl_cutoff)
    $scope.delivery_date   = data.delivery_date
    $scope.additional_item = data.shipping.additional_item

    setCategories data.items

    if $scope.total_qty >= $scope.cutoff
      $scope.shipping = 0
    else
      $scope.shipping = (if $scope.country.iso is domestic.iso then shipping.domestic else shipping.international)
      $scope.shipping += (($scope.total_qty - 1) * $scope.additional_item) if $scope.country.iso isnt domestic.iso and $scope.total_qty > 1
      # data.additional_item

  # start coupon discounts
  $scope.coupon = {code: "", description: "", discount: 0, percent: 0}
  $scope.show_coupon = false

  $scope.toggleCoupon = ->
    $scope.show_coupon = (if $scope.show_coupon is false then true else false)

  $scope.applyCoupon = ->
    if $scope.coupon.code
      NProgress.start()
      Coupon.get code: $scope.coupon.code, (coupon) ->
        if !coupon.code
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if coupon.active != true
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if (coupon.expiration != null && new Date(coupon.expiration) < new Date())
          $scope.coupon.description = "Coupon already expired."
          $scope.coupon.error = true
        else if coupon.limit != null && coupon.usage >= coupon.limit
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else
          $scope.coupon.error    = false
          $scope.coupon.discount = $scope.subtotal * (coupon.discount / 100)
          $scope.coupon.percent  = coupon.discount
          $scope.coupon.description = "#{coupon.discount}% discount"

        if $scope.coupon.error is true
          $("#coupon-discount-form").addClass("error")
        else
          $("#coupon-discount-form").removeClass("error")

        NProgress.done()
  # end coupon discounts

  $scope.giftcard = {code: "", description: "", discount: 0, percent: 0, error:true, amount: 0}
  $scope.show_giftcard = false

  $scope.toggleGiftcard = ->
    $scope.show_giftcard = (if $scope.show_giftcard is false then true else false)

  $scope.applyGiftCard = ->
    if $scope.giftcard.code
      NProgress.start()
      Giftcard.get code: $scope.giftcard.code, (giftcard) ->
        # console.log "giftcard", giftcard
        if !giftcard.key_code
          $scope.giftcard.description = "Invalide giftcard code."
          $scope.giftcard.error = true
        else if giftcard.active != true
          $scope.giftcard.description = "Inactive."
          $scope.giftcard.error = true
        else if giftcard.amount <= 0
          console.log "amount", giftcard.amount
          $scope.giftcard.description = "Invalid amount."
          $scope.giftcard.error = true
        else
          $scope.giftcard.error    = false
          if $scope.subtotal <= giftcard.amount
            $scope.giftcard.remains = $scope.subtotal
            $scope.giftcard.amount = giftcard.amount - $scope.subtotal
          else
            $scope.giftcard.remains = giftcard.amount
            $scope.giftcard.amount = 0

        if $scope.giftcard.error is true
          console.log "giftcard new", $scope.giftcard
          $("#giftcard-discount-form").addClass("error")
        else
          $("#giftcard-discount-form").removeClass("error")

        NProgress.done()
]

@CartCreditCardCtrl = ["$scope", "$http", "geolocation", "COUNTRIES", "Coupon", "Cart", ($scope, $http, geolocation, COUNTRIES, Coupon, Cart) ->
  $scope.countries = COUNTRIES
  domestic         = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  shipping         = {}
  total_qty        = 0
  $scope.payment   = gon.payment
  $scope.country   = (if gon.website.name is "FanTees-NZ" then COUNTRIES[159] else COUNTRIES[13])
  $scope.shipping  = 0
  $scope.regex     = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  # default values
  $scope.checkout = gon.order
    # email: ""
    # first_name: ""
    # last_name: ""
    # phone: ""
    # address: ""
    # city: ""
    # zip: ""
    # country: (if gon.website.name is "FanTees-NZ" then "NZ" else "AU")
    # state: ""
    # cc_number: ""
    # month: ""
    # year: ""
    # cvn: ""


  # all_cart_index
  $http method: "GET", url: Routes.all_cart_index_path()
    .success (data, status, headers, config) ->
      initValues(data)

      initCheckoutDetails(data.unfinished_order)

      $scope.total    = 0
      $scope.subtotal = getSubtotal(data.items)

      $scope.coupon = {code: "", description: "", discount: 0, percent: 0}
      $scope.show_coupon = false

      if gon.coupon && gon.coupon.code
        $scope.coupon.code        = gon.coupon.code
        $scope.show_coupon        = true
        $scope.coupon.discount    = $scope.subtotal * (gon.coupon.discount / 100)
        $scope.coupon.percent     = gon.coupon.discount
        $scope.coupon.description = "#{gon.coupon.discount}% discount"

    .error (data, status, headers, config) ->
      # error message here

  $scope.removeItem = (id) ->
    Cart.delete id: id, (data) ->
      initValues(data)

      $scope.subtotal = getSubtotal(data.items)
      updateCartCounter()
      # counter update

  $scope.ci_country = $scope.country.name
  $scope.selectCountry = (e) ->
    result = $.grep(COUNTRIES, (e) ->
      e.name is $scope.ci_country
    )

    $scope.country = result[0] if result.length > 0

    # update shipping cost
    if domestic.iso is $scope.country.iso
      shipping_cost   = (if domestic.iso is $scope.country.iso then shipping.domestic else shipping.international)
      shipping_cutoff = shipping.cutoff
    else
      shipping_cost   = shipping.international
      shipping_cutoff = shipping.intl_cutoff

    $scope.shipping = (if total_qty < shipping_cutoff then shipping_cost else 0)

  $scope.updateCheckout = (e) ->
    console.log e

  initCheckoutDetails = (details) ->
    $scope.checkout.email       = details.email
    $scope.checkout.first_name  = details.first_name
    $scope.checkout.last_name   = details.last_name
    $scope.checkout.address     = details.address
    $scope.checkout.city        = details.town
    $scope.checkout.state       = details.state
    $scope.checkout.zip         = details.postal
    $scope.ci_country           = (if $scope.ci_country then $scope.ci_country else details.country)
    $scope.checkout.phone       = details.phone

  $scope.updateUnfinishedOrder = (param) ->
    $http method: "POST", url: Routes.update_unfinished_orders_path(), params: param
      .success (data, status, headers, config) ->
        # console.log data.msg
        return
      .error (data, status, headers, config) ->
        # console.log data.msg
        return

  $scope.fieldBlur = ->

    # check all textboxes
    $("input").each (index, elem) ->
      elemId = $.trim($(elem).prop("id"))
      elemVal = $(elem).val()
      if elemId is "ci-address"
        $scope.checkout.address = elemVal
      else if elemId is "ci-town"
        $scope.checkout.city = elemVal
      else if elemId is "ci-state"
        $scope.checkout.state = elemVal
      else if elemId is "ci-pcode"
        $scope.checkout.zip = elemVal
      else if elemId is "ci-country"
        $scope.checkout.country = elemVal

    params            = {}
    params.email      = $scope.checkout.email if $scope.checkout.email isnt "" and $scope.regex.test($scope.checkout.email)
    params.first_name = $scope.checkout.first_name if $scope.checkout.first_name isnt ""
    params.last_name  = $scope.checkout.last_name if $scope.checkout.last_name isnt ""
    params.address    = $scope.checkout.address if $scope.checkout.address isnt ""
    params.town       = $scope.checkout.city if $scope.checkout.city isnt ""
    params.state      = $scope.checkout.state if $scope.checkout.state isnt ""
    params.postal     = $scope.checkout.zip if $scope.checkout.zip isnt ""
    params.country    = $scope.ci_country if $scope.ci_country isnt ""
    params.phone      = $scope.checkout.phone if $scope.checkout.phone isnt ""

    $scope.updateUnfinishedOrder params

  # updates for cart 12.16.2014
  $scope.getQuantity = (item) ->
    quantity = []
    $.each item.stocks, (key, value) ->
      quantity = value.quantities if value.size is item.size

    quantity

  $scope.getSizes = (item) ->
    sizes = []
    $.each item.stocks, (key, value) ->
      sizes.push value.size

    sizes

  $scope.updateSize = (item) ->
    quantities    = $scope.getQuantity(item)
    item.quantity = 1 if item.quantity > quantities.length

    $scope.updateCart(item)

  setCategories = (items) ->
    $.each items, (key, value) ->
      $.each value.categories, (k, v) ->
        value.category = v if value.category_id is v.category_id

  $scope.updateCategory = (item) ->
    item.category_id = item.category.category_id

    $scope.updateCart(item)
    # console.log $scope.item

  onInstockUpdate = (data) ->
    # to be updated - keenan
    # $.each data.items, (key, value) ->
    #   if value.instock is true
    #     has_size = false
    #     has_qty  = false
    #     $.each value.stocks, (index, stock) ->
    #       console.log stock
    #       if stock.size == value.size
    #         has_size = true
    #         has_qty  = true if stock.quantities.length >= value.quantity
    #     console.log has_qty, has_size
    #     unless has_size || has_size
    #       console.log "SUD?"

    data

  $scope.updateCart = (item) ->
    NProgress.start()
    $(".place-order-now").attr("disabled", "disabled")

    $http.put Routes.cart_path(item.id), {quantity: item.quantity, size: item.size}
    .success (data, status, headers, config) ->
      if domestic.iso is $scope.country.iso
        shipping_cost   = (if domestic.iso is $scope.country.iso then shipping.domestic else shipping.international)
        shipping_cutoff = shipping.cutoff
      else
        shipping_cost   = shipping.international
        shipping_cutoff = shipping.intl_cutoff

      $scope.subtotal = getSubtotal($scope.items)
      $scope.shipping = (if total_qty < shipping_cutoff then shipping_cost else 0)

      $(".place-order-now").removeAttr("disabled")
      NProgress.done()
    .error (data, status, headers, config) ->
      $(".place-order-now").removeAttr("disabled")
      NProgress.done()

  # end updates for cart 12.16.2014

  # $scope.selectCountry = (e) ->
  #   $scope.checkout.country = e.country.iso
  #   if $scope.total_qty >= $scope.cutoff
  #     $scope.shipping = 0
  #   else
  #     $scope.shipping = (if $scope.country.iso is domestic.iso then shipping.domestic else shipping.international)
  #     $scope.shipping += (($scope.total_qty - 1) * $scope.additional_item) if $scope.country.iso isnt domestic.iso and $scope.total_qty > 1

  getSubtotal = (items) ->
    subtotal  = 0
    total_qty = 0
    $.each items, (key, item) ->
      total_qty += item.quantity
      subtotal  += (parseFloat(item.info.amount) * parseFloat(item.quantity))

    subtotal

  updateCartCounter = ->
    count = parseInt($("#cart-count").text()) - 1

    $("#cart-count").css("opacity", "0") if count < 1
    $("#cart-count").text(count)

  initValues = (data) ->
    shipping         = data.shipping
    $scope.total_qty = data.total_qty
    $scope.items     = data.items
    $scope.cutoff    = (if $scope.country.iso is domestic.iso then shipping.cutoff else shipping.intl_cutoff)
    $scope.delivery_date   = data.delivery_date
    $scope.additional_item = data.shipping.additional_item

    setCategories data.items

    if $scope.total_qty >= $scope.cutoff
      $scope.shipping = 0
    else
      $scope.shipping = (if $scope.country.iso is domestic.iso then shipping.domestic else shipping.international)
      $scope.shipping += (($scope.total_qty - 1) * $scope.additional_item) if $scope.country.iso isnt domestic.iso and $scope.total_qty > 1
      # data.additional_item

  # start coupon discounts
  $scope.toggleCoupon = ->
    $scope.show_coupon = (if $scope.show_coupon is false then true else false)

  $scope.applyCoupon = ->
    if $scope.coupon.code
      NProgress.start()
      Coupon.get code: $scope.coupon.code, (coupon) ->
        if !coupon.code
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if coupon.active != true
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else if (coupon.expiration != null && new Date(coupon.expiration) < new Date())
          $scope.coupon.description = "Coupon already expired."
          $scope.coupon.error = true
        else if coupon.limit != null && coupon.usage >= coupon.limit
          $scope.coupon.description = "Invalide coupon code."
          $scope.coupon.error = true
        else
          $scope.coupon.error    = false
          $scope.coupon.discount = $scope.subtotal * (coupon.discount / 100)
          $scope.coupon.percent  = coupon.discount
          $scope.coupon.description = "#{coupon.discount}% discount"

        if $scope.coupon.error is true
          $("#coupon-discount-form").addClass("error")
        else
          $("#coupon-discount-form").removeClass("error")

        NProgress.done()
  # end coupon discounts

  # Coupons

  # $scope.coupon = {description: "Retrieving description..", error: false, discount: 0, checked: false, btn: "Apply Coupon"}

  # $scope.applyCoupon = ->
  #   $scope.coupon.error    = false
  #   $scope.coupon.discount = 0
  #   $scope.checkCoupon ->
  #     if $scope.coupon.code && $scope.coupon.error is false && !$scope.coupon.check
  #       $scope.coupon.class = "active"
  #       $scope.coupon.check = true
  #       $scope.coupon.btn   = "Successfully Applied"
  #       $scope.coupon.txtClass = ""
  #     else if $scope.coupon.code && $scope.coupon.error
  #       $scope.coupon.class = ""
  #       $scope.coupon.check = false
  #       $scope.coupon.btn   = "Apply Coupon"
  #       $scope.coupon.txtClass = "txtbox-error"
  #     else if !$scope.coupon.code
  #       $scope.coupon.class = ""
  #       $scope.coupon.check = false
  #       $scope.coupon.btn   = "Apply Coupon"
  #       $scope.coupon.txtClass = ""

  # $scope.checkCoupon = (callback) ->
  #   if $scope.coupon.code
  #     NProgress.start()
  #     Coupon.get code: $scope.coupon.code, (coupon) ->
  #       unless coupon.code
  #         $scope.coupon.description = "Invalide coupon code."
  #         $scope.coupon.error = true
  #       else if coupon.active != true
  #         $scope.coupon.description = "Invalide coupon code."
  #         $scope.coupon.error = true
  #       else if (coupon.expiration != null && new Date(coupon.expiration) < new Date())
  #         $scope.coupon.description = "Coupon already expired."
  #         $scope.coupon.error = true
  #       else if coupon.limit != null && coupon.usage >= coupon.limit
  #         $scope.coupon.description = "Invalide coupon code."
  #         $scope.coupon.error = true
  #       else
  #         $scope.coupon.discount = $scope.subtotal * (coupon.discount / 100)
  #         $scope.coupon.description = "#{coupon.discount}% discount"

  #       NProgress.done()
  #       callback()
  #   else
  #     $scope.coupon =
  #       description: "Retrieving description.."
  #       error: false
  #       discount: 0
  #       code: null

  #     callback()

  # $scope.show_coupon = false
  # $scope.toggleCoupon = ->
  #   $scope.show_coupon = (if $scope.show_coupon is false then true else false)
  #   $scope.coupon =
  #     description: "Retrieving description.."
  #     error:       false
  #     discount:    0
  #     checked:     false
  #     btn:         "Apply Coupon"
  #     class:       ""
  #     checked:     false
  #     txtClass:    ""

  $(document).on "blur", "#ci-email", ->
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    value =
    errorMsg = $("<span class='error-message' style='z-index:999;position:absolute;display:inline-block;padding:8px;font-size:12px;'>Invalid Email address..!!</span>")
    if !regex.test($(this).val())
      $(this).data("error", errorMsg.clone().insertAfter($(this)))
    else
      $(this).data("error").remove()
      $(this).removeData("error")

  # On Submit
  $(document).on "submit", "#new_order", ->
    thisForm = $("#info")
    errorMsg = $("<span class='error-message' style='z-index:999;position:absolute;display:inline-block;padding:8px;font-size:12px;'>This field is required..!!</span>")
    isSuccess = true

    $("input", thisForm).not("#ci-state").each () ->
      if $(this).val() is ""
        isSuccess = false
        if !$(this).data("error")
          $(this).data("error", errorMsg.clone().insertAfter($(this)))
      else
        if $(this).data("error")
          $(this).data("error").remove()
          $(this).removeData("error")

    if isSuccess is false
      return false

    error = false
    regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    $scope.payment = "CC"
    if $scope.payment == "CC"
      $.each $scope.checkout, (key, value) ->
        if $scope.payment == "CC" && ["cc_number", "exp_month", "year", "cvn"].indexOf(key) != -1 && value is ""
          error = true
          $("input[name='order[cc_number]']").addClass("txtbox-error")
          $("input[name='order[month]']").addClass("txtbox-error")
          $("input[name='order[year]']").addClass("txtbox-error")
          $("input[name='order[cvn]']").addClass("txtbox-error")
        else if $scope.payment == "PP" && ["cc_number", "month", "year", "cvn"].indexOf(key) != -1
          $("input[name='order[#{key}]']").removeClass("txtbox-error")
        else if value is "" && (key != "phone" && key != "coupon_code")
          unless $("input[name='order[#{key}]']").val()
            error = true
            $("input[name='order[#{key}]']").addClass("txtbox-error")
        else if key is "email" and !regex.test(value)
          error = true
          $("input[name='order[#{key}]']").addClass("txtbox-error")
        else
          $("input[name='order[#{key}]']").removeClass("txtbox-error")

    # checking for coupon
    if ($scope.coupon.code && !$scope.coupon.check) || ($scope.coupon.check && !$scope.coupon.code) || ($scope.coupon.error && $scope.show_coupon)
      $("input[name=coupon_code]").addClass("txtbox-error")
      $("#coupon-desc").addClass("txtbox-error")
      $scope.coupon.check = false
      $scope.coupon.class = ""
      $scope.coupon.btn   = "Apply Coupon"
      $scope.$apply()
    else
      $("input[name=coupon_code]").removeClass("txtbox-error")
      $("#coupon-desc").removeClass("txtbox-error")

    # console.log error

    if error
      return false
    else
      $(".place-order-now").attr("disabled", "disabled")
      return true

]