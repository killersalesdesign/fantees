@App.directive "ngElevateZoom", ->
  restrict: "A"
  link: (scope, element, attrs) ->

    #Will watch for changes on the attribute
    linkElevateZoom = ->

      #Check if its not empty
      return  unless attrs.zoomImage
      element.attr "data-zoom-image", attrs.zoomImage
      $(element).elevateZoom()
      return
    attrs.$observe "zoomImage", ->
      linkElevateZoom()
      return

    linkElevateZoom()
    return
