'use strict';
@AppName = $("html").attr("ng-app")
@App     = angular.module @AppName, [ "ngResource", "ng-rails-csrf", "ngRoute", "ui.bootstrap", "flashr", "ui.select2", "geolocation" ]
