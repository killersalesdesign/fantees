App.factory "Giftcard", [ "$resource", ($resource) ->
  Giftcard = $resource "/giftcards/:code",
                        {code: "@code"}

  return Giftcard
]