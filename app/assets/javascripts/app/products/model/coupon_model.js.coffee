App.factory "Coupon", [ "$resource", ($resource) ->
  Coupon = $resource "/coupons/:code",
                        {code: "@code"}

  return Coupon
]