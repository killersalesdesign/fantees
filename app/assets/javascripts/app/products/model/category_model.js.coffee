App.factory "Category", [ "$resource", ($resource) ->
  Category = $resource "/categories/:id",
                        {id: "@id"}

  return Category
]