App.factory "Cart", [ "$resource", ($resource) ->
  Cart = $resource "/cart/:id",
                   {id: "@id"}

  return Cart
]