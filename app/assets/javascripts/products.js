// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.core
//= require lib/jquery.elevatezoom
//= require lib/jquery_easing
//= require hamlcoffee
//= require js-routes
//= require lib/html5shiv.min
//= require lib/respond.min
//= require lib/modernizr.custom
//= require lib/select2.min
//= require lib/angular/angular
//= require lib/county
//= require lib/nprogress
//= require ng-rails-csrf
//= require lib/angular/angular
//= require lib/angular/angular-flashr
//= require lib/angular/angular-resource
//= require lib/angular/angular-route
//= require lib/angular/angular-sanitize
//= require lib/angular/angular-select2
//= require lib/angular/angular-geolocation
//= require lib/bootstrap/bootstrap
//= require lib/bootstrap/ui-bootstrap-0.10.0
//= require application/main
//= require_tree ./app/products