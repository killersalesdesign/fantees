collection @products

attributes :id, :created_at, :active, :date, :price, :sales, :title, :url, :profit, :start_number, :slug, :ads_spend, :website_id, :status, :free_shipping, :show, :international_cutoff, :additional_item_cost

node do |product|
  {
    :product_image => product.product_images.sample.product_image(:medium),
    :total_sold => product.total_sold
  }
end