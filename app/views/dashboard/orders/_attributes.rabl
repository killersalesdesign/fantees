attributes :id, :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :shipping_info, :printed, :shipped, :description, :order_id, :coupon_id, :created_at

node do |order|
  {
    :ordered_products => ActiveSupport::JSON.decode(order.ordered_products.to_json),
    :shipping_info => ActiveSupport::JSON.decode(order.shipping_info),
    :product_title => order.product.title,
    :gift_cards => order.get_cards,
    :product_image => order.product_image.product_image(:medium),
    :order_total => order.total_orders_amount,
    :gift_cards => order.get_cards
  }
end