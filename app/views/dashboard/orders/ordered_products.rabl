collection @ordered_products
attributes :id, :quantity, :size, :category_id, :order_id, :product_id

node do |ordered_product|
  {
    :type  => ordered_product.category.name,
    :image => ordered_product.category_image.product_image(:medium),
    :price => ordered_product.price_and_profit[:amount]
  }
end