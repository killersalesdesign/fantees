class Website < ActiveRecord::Base
  has_many :products
  has_many :orders
  has_many :ratings

  attr_accessible :name, :url

  def support_mail
    mail_ext = self.url.split("//")
    "support@#{mail_ext[1]}"
  end
end
