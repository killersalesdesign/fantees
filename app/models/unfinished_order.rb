class UnfinishedOrder < ActiveRecord::Base
  attr_accessible :category, :price, :product_id, :qty, :size, :style, :tracking_id, :website_id, :recovered, :recovered_date

  has_settings do |s|
    s.key :cart, :defaults => {:items => [], :user_email => "", :first_name => "", :last_name => "", :address => "", :town => "", :state => "", :postal => "", :country => "", :phone => "", :shipping_fee => 0}
  end

  def self.remove_order(tid)
    res = UnfinishedOrder.find_by_tracking_id(tid)
    res.delete unless res.nil?
  end

  def self.find_tracking_id(tid)
    UnfinishedOrder.where('tracking_id = ?', tid)
  end
end
