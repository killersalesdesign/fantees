class Cart
  def initialize(session)
    @session = session
    @session[:items] ||= []
    @session[:item_index] ||= 1

    @delivery_date = DateTime.now.beginning_of_day
    @shipping_fee  = {domestic: 0, international: 0, cutoff: 0, intl_cutoff: 0, additional_item: 0}
    @quantity      = 0
    @sizes         = []
  end

  def update_attributes(id, params)
    @session[:items].each do |item|
      params.each {|name, value| item[name] = value} if item[:id].to_i == id.to_i
    end
  end

  def create(items = [])
    unless items.blank?
      items.each do |item|
        item[:id]      = @session[:item_index]
        item_to_update = @session[:items].detect{|i| item[:category_id].to_i == i[:category_id].to_i && item[:product_id].to_i == i[:product_id].to_i && item[:size] == i[:size] && item[:price] == i[:price]}
        unless item_to_update.blank?
          delete(item_to_update[:id])

          item_to_update[:quantity] += item[:quantity]
          item_to_update[:id]        = item[:id]
          @session[:items] << item_to_update
        else
          @session[:items] << item
        end
          @session[:item_index] += 1
      end
    end
  end

  def all
    items     = []
    postage   = 0
    i_postage = 0

    # delivery date
    first_item = @session[:items].first
    if first_item.blank?
      # @delivery_date = DateTime.now.beginning_of_day + 14.days
      @delivery_date = DateTime.now.beginning_of_day + 9.days
    else
      first_product  = Product.find(first_item[:product_id])
      # @delivery_date = first_product.date + 14.days
      @delivery_date = first_product.date + 9.days
      
      if first_product.status == 2
        delivery = DeliveryService.new(2, Time.now)
        @delivery_date = delivery.bussiness_days
      elsif first_product.status == 1
        # @delivery_date = Time.now.beginning_of_day + 12.days
        @delivery_date = Time.now.beginning_of_day + 9.days
      end
    end

    unless @session[:items].nil?
      @session[:items].each do |i|
        # update all value
        product = Product.find(i[:product_id])
        info    = product.price_and_profit_of_style(i[:category_id])
        sizes   = []
        sizes2  = []
        quantities = (1..25)
        # puts info.inspect

        stocks     = []
        flag       = []
        categories = []

        @session[:items].each { |f| flag << f[:category_id] if f[:product_id] == i[:product_id] }
        if product.status != Product::CAMPAIGN
          # stocks = product.stocks.where(:category_id => i[:category_id])

          Stock.where(:product_id => i[:product_id], :category_id => i[:category_id]).each do |s|
          # product.stocks.where(:category_id => i[:category_id]).each do |s|
            if s.amount > 0
              sizes << s.size
              stocks << {size: s.size, quantities: (1..s.amount.to_i)}
            end
          end

          # Stock.where("product_id = ? AND amount > ?", i[:product_id], 0).each do |s|
          product.settings(:options).prices.each do |p|
            has_stocks = Stock.where("product_id = ? AND amount > ? AND category_id = ?", i[:product_id], 0, p[:category_id]).count
            categories << {name: p[:type], category_id: p[:category_id]} if (p[:category_id] == i[:category_id] || !flag.include?(p[:category_id])) && has_stocks > 0
          end
          # product.stocks.where(:category_id => i[:category_id]).each
        else
          category = Category.find i[:category_id]
          category.settings(:options).sizes.each do |size|
            stocks << {size: size, quantities: (1..25)}
            sizes << size
          end

          product.settings(:options).prices.each { |p| categories << {name: p[:type], category_id: p[:category_id]} if p[:category_id] == i[:category_id] || !flag.include?(p[:category_id]) }
        end
        puts "categories #{categories.inspect}"
        # puts "INFO:", product.settings(:options).prices.count, product.settings(:options).categories
        info[:sizes] = sizes

        item = {
          :id            => i[:id],
          :category_id   => i[:category_id],
          :product_id    => i[:product_id],
          :quantity      => i[:quantity],
          :size          => i[:size],
          :price         => i[:price],
          :title         => product.title,
          :info          => info,
          :stocks        => stocks,
          :categories    => categories,
          :instock       => product.status != Product::CAMPAIGN ? true : false,
          :image         => product.product_images.where(:category_id => i[:category_id]).first.product_image.url(:medium)
        }
        items << item

        # total quantity
        @quantity += i[:quantity]

        # shipping fee
        info[:postage]                = 0 if info[:postage].nil?
        info[:i_postage]              = 0 if info[:i_postage].nil?
        @shipping_fee[:domestic]      = info[:postage] > @shipping_fee[:domestic] ? info[:postage] : @shipping_fee[:domestic]
        @shipping_fee[:international] = info[:i_postage] > @shipping_fee[:international] ? info[:i_postage] : @shipping_fee[:international]

        # cutoff
        product.free_shipping           = 0 if product.free_shipping.nil?
        product.international_cutoff    = 0 if product.international_cutoff.nil?
        product.additional_item_cost    = 0 if product.additional_item_cost.nil?
        @shipping_fee[:cutoff]          = product.free_shipping > @shipping_fee[:cutoff] ? product.free_shipping : @shipping_fee[:cutoff]
        @shipping_fee[:intl_cutoff]     = product.international_cutoff > @shipping_fee[:intl_cutoff] ? product.international_cutoff : @shipping_fee[:intl_cutoff]
        @shipping_fee[:additional_item] = product.additional_item_cost > @shipping_fee[:additional_item] ? product.additional_item_cost : @shipping_fee[:additional_item]

        # delivery date
        # date = product.date + 14.days
        date = product.date + 9.days
        if product.status == Product::IN_STOCK
          delivery = DeliveryService.new(2, Time.now)
          date     = delivery.bussiness_days
        end
        @delivery_date = @delivery_date > date ? @delivery_date : date
      end
    end

    items
  end

  def total_items
    @session[:items].count
  end

  def find(id)
    @session[:items].detect{|item| item[:id].to_i == id.to_i}
  end

  def delete(id)
    item = find id
    @session[:items].delete(item)
  end

  def delete_all
    @session[:items] = []
    @session[:item_index] = 1
  end

  def shipping_fee
    @shipping_fee
  end

  def delivery_date
    @delivery_date
  end

  def quantity
    @quantity
  end
end