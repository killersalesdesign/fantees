class ProductImage < ActiveRecord::Base
  belongs_to :product
  belongs_to :category

  attr_accessible :product_image, :color, :category_id, :product_id

  has_attached_file :product_image,
  :storage => :s3,
  :url => ':s3_alias_url',
  :s3_host_alias => ENV['AWS_PRODUCT_IMG_HOST'],
  :bucket => ENV['AWS_PRODUCT_IMG_BUCKET'],
  :path => ":class/:attachment/:id_partition/:style/:filename",
  :s3_credentials => {
    :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
    :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
  },
  :s3_protocol => 'https',
  :styles => { :medium => "415x500>", :thumb => "100x100>" },
  :default_url => "/assets/thumbnail-default.jpg"

  validates_attachment_content_type :product_image, :content_type => /\Aimage\/.*\Z/
end
