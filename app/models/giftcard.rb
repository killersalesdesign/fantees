class Giftcard < ActiveRecord::Base
  attr_accessible :key_code, :order_id, :is_used, :product_id, :amount, :active
  belongs_to :order
  belongs_to :user
end
