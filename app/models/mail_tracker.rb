class MailTracker < ActiveRecord::Base
  attr_accessible :action_count, :action_type, :time_frame

  # action types
  # open - when the user opens the email
  # click - when the user clicks a link or button from the email

  # time frames
  # 30-minutes - email sent on 30 minute mark
  # 1-day - email sent after 1 day
  # 10-days - email sent after 10 days of abandonment

  def self.increment_count(action_type, time_frame)
		last_entry = MailTracker.where(:action_type => action_type, :time_frame => time_frame).last
		if not last_entry.nil?
			last_date = last_entry.created_at.strftime("%Y-%m-%d")
			if last_date == Date.today.strftime("%Y-%m-%d")
				# already have a record
	  	 	# will update
	  	 	last_entry.action_count += 1
	  	 	last_entry.save!
			else
	  	 	# create new record
	  	 	MailTracker.create action_type: action_type, action_count: 1, time_frame: time_frame
			end
		else
			MailTracker.create action_type: action_type, action_count: 1, time_frame: time_frame
		end

  end

end
