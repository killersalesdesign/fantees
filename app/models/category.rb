class Category < ActiveRecord::Base
  has_many :orders
  has_many :ordered_products
  has_many :product_images
  has_many :stocks
  attr_accessible :name, :slug, :active

  before_validation :generate_slug

  has_settings do |s|
    s.key :options, :defaults => {:sizes => ["S", "M", "L", "XL", "XXL"]}
  end

  def total_products
    self.products.count
  end

  def total_orders
    self.ordered_products.count
  end

  private

  def generate_slug
    self.slug ||= name.gsub("<br />", " ").parameterize
  end
end
