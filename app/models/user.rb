class User < ActiveRecord::Base
  has_many :orders
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :first_name, :last_name, :address, :avatar

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "50x50>" }, :default_url => "/assets/thumbnail-default.jpg"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  # attr_accessible :title, :body
  has_many :giftcards

  def search_orders(params)
    unless params.blank?
      results = self.orders
      params = ActiveSupport::JSON.decode(params)

      if params[:name]
        p_ids   = Product.where("title LIKE ?", "%#{params[:name]}%").collect(&:id)
        results = results.where(:id => products)
      elsif params[:date]
      else
      end
    else
      results = self.orders
    end

    return results.order("created_at DESC")
  end
end
