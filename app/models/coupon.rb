class Coupon < ActiveRecord::Base
  has_many :orders

  attr_accessible :code, :expiration, :limit, :usage, :active, :discount

  validates :code, :presence => true
  validates :discount, :presence => true

  def self.check(coupon)
    coupon = Coupon.find_by_code coupon
    result = true
    if coupon.nil?
      result = false
    elsif coupon.active != true
      result = false
    elsif coupon.expiration != nil && coupon.expiration < DateTime.now.to_date
      result = false
    elsif coupon.limit != nil && coupon.usage >= coupon.limit
      result = false
    end

    result
  end

  def to_server_timzone
    unless self.expiration.nil?
      self.expiration.in_time_zone('Arizona')
    else
      self.expiration
    end
  end
end
