class Stock < ActiveRecord::Base
  belongs_to :product
  belongs_to :category

  attr_accessible :amount, :size, :category_id, :product_id

  def self.update_stocks(id, stocks, categories)
    in_stocks = Stock.where(:product_id => id)
    # in_stocks.delete_all

    stocks.each do |stock|
      pid = id.to_i
      cid = stock["category_id"].to_i
      stock["sizes"].each do |size|
        unless size["id"].blank?
          s = Stock.find(size["id"].to_i)
          s.amount = size["value"].to_i
        else
          s = Stock.new(:category_id => cid, :product_id => pid, :size => size["label"], :amount => size["value"].to_i)
        end
        s.save
      end
    end

    cat_ids    = in_stocks.group(:category_id).collect(&:category_id)
    invalid_id = cat_ids - categories
    in_stocks.where(:category_id => invalid_id).delete_all unless invalid_id.blank?
  end
end
