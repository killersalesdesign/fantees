class ContactUs < MailForm::Base
  attribute :name,      :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject,   :validate => true

  attribute :message,   :validate => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => self.subject,
      # :to => "support@fantees.com.au",
      # :to => "eralph@graphicsadmin.com",
      # :to => "eralphamodia@gmail.com",
      # :to => "matt@fantees.com.au",
      :to => "support.16983.446efa1608bba6a2@helpscout.net",
      # :from => %("#{name}" <#{email}>)
      # :from => "no-reply@fantees.com.au",
      :from => "matt@fantees.com.au",
      :"reply-to" => %("#{name}" <#{email}>)
    }
  end
end