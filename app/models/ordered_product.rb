class OrderedProduct < ActiveRecord::Base
  belongs_to :category
  belongs_to :order
  belongs_to :product

  attr_accessible :quantity, :size, :category_id, :order_id, :created_at, :product_id

  before_create :check_stocks, :strip_sizes, :check_duplicate

  def product
    self.order.product
  end

  def price_and_profit
    unless self.product_id.nil?
      Product.find(self.product_id).price_and_profit_of_style(self.category_id)
    else
      self.order.product.price_and_profit_of_style(self.category_id)
    end
  end

  def title
    title = self.order.product.title
    title = Product.find(self.product_id).title unless self.product_id.nil?

    title
  end

  def category_image
    unless self.product_id.nil?
      # image = self.product.product_images.where(:category_id => self.category_id)
      image = Product.find(self.product_id).product_images.where(:category_id => self.category_id)
    else
      image = self.order.product.product_images.where(:category_id => self.category_id)
    end

    image.sample || self.order.product.product_images.first
  end

  private

  def strip_sizes
    self.size = self.size.strip
  end

  def check_stocks
    product = Product.find(self.product_id)
    if product.status != Product::CAMPAIGN
      stock = Stock.where(:size => self.size, :category_id => self.category_id, :product_id => self.product_id).first
      unless stock.amount.nil?
        if stock.amount > 0 && stock.amount >= self.quantity
          stock.decrement(:amount, self.quantity)
          stock.save
          return true
        else
          return false
        end
      end
    end
  end

  def check_duplicate
    # counter = 0
    # results = Order.where(:product_id => self.product.id, :user_id => self.order.user.id, :payer_id => self.order.payer_id, :created_at => 5.minutes.ago..Time.now)
    # if results.count > 0
    #   results.each do |o|
    #     o.ordered_products.each do |op|
    #       if op.size.strip == self.size.strip && op.category_id == self.category_id
    #         counter += 1
    #         # UserMailer.send_duplicate_order(self.order).deliver
    #         # break
    #       end
    #     end
    #   end

    #   if counter == 1
    #     UserMailer.send_duplicate_order(self.order).deliver
    #   end
    # end

    send_duplicate = false
    results = Order.where(user_id: self.order.user.id, created_at: 5.minutes.ago..Time.now)
    if results.count > 0
      results.each do |o|
        if o.id != self.order_id
          o.ordered_products.each do |op|
            if op.product_id == self.product_id && op.category_id == self.category_id && op.size.strip == self.size.strip
              send_duplicate = true
            end
          end
        end
      end

      if send_duplicate
        UserMailer.send_duplicate_order(self.order).deliver
      end
    end

  end
end
