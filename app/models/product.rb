class Product < ActiveRecord::Base
  include AlgoliaSearch

  algoliasearch per_environment: true, enqueue: :trigger_sidekiq_worker do
    # all attributes will be sent
    attribute :title, :description, :price, :url, :slug, :tags


  end

  CAMPAIGN = 0
  PRINTING = 1
  IN_STOCK = 2

  # special FB conversion
  SP_FB_CONVERSION = ['1307ghy', '1306ksg', '1314xhg', '1325-nurse', '1326-nurse', '1327-nurse', '1328-nurse', '1329mhc', '1337-v8', '1338-auscr', '1338-auirr', '1349-dyls', '1355-v8', '1356-reel', '1357-christ', '1358-master', '1359-footy']

  extend FriendlyId
  # belongs_to :category
  belongs_to :website

  has_many :product_images
  has_many :orders
  has_many :stocks
  has_many :unfinished_orders
  has_many :ordered_products

  has_one :extra_order
  has_one :fb_pixel
  has_one :google_analytic

  attr_accessible :id, :active, :date, :description, :price, :sales, :title, :url, :profit, :start_number,
                  :slug, :ads_spend, :website_id, :status, :free_shipping, :show, :international_cutoff,
                  :additional_item_cost, :tags, :gift_card

  friendly_id :url, use: :slugged

  validates :url, presence: true

  has_settings do |s|
    s.key :options, :defaults => {:categories => [], :prices => []}
  end

  def self.trigger_sidekiq_worker(record, remove)
    AlgoliaProductSearchWorker.perform_async(record.id, remove)
  end

  def self.search_by_tags(keyname)
    search = "#{keyname}"
    puts search
    (!keyname) ? [] : Product.where((['products.tags LIKE ?'] * search.split.length).join(' OR '), *search.split(" ").map{|keyword| "%\"#{keyword}\%"})
  end

  def sanitized_description
    ActionView::Base.full_sanitizer.sanitize self.description
  end

  def condition
    "new"
  end

  def image_link
    unless self.product_images.sample.nil?
      self.product_images.sample.product_image(:medium)
    end
  end

  def availability

    unless active
      "out of stock"
    else
      if self.time_left[:days] > 0
        "preorder"
      else
        "in stock"
      end
    end

  end

  def brand
    "FanTees"
  end

  def product_category
    "Apparel &amp; Accessories &gt; Clothing &gt; Shirts &amp; Tops"
  end

  def gallery
    gallery = []
    self.product_images.each do |i|
      image = {}
      image[:id]       = i.id
      image[:category_id] = i.category_id
      image[:original] = i.product_image
      image[:medium]   = i.product_image(:medium)
      image[:thumb]    = i.product_image(:thumb)
      gallery << image
    end
    gallery
  end

  def time_left
    time_now    = self.date
    t_in_au     = time_now.in_time_zone('Australia/Brisbane')
    offset      = t_in_au.gmt_offset
    time_now_au = self.date + offset

    secs_left   = time_now_au - Time.now
    day_in_secs = 86400
    days        = secs_left / day_in_secs
    hours       = 24 * (days - days.to_i)
    mintues     = 60 * (hours - hours.to_i)

    {:days => days.to_i, :hours => hours.to_i, :mintues => mintues.to_i}
  end

  def price_and_profit_of_style(category_id)
    current_price = nil
    self.settings(:options).prices.each do |p|
      current_price = p if p[:category_id].to_i == category_id.to_i
    end
    current_price = {:category_id => category_id.to_i, :type => "", :amount => 0, :profit => 0} if current_price.nil?

    return current_price
  end

  def total_sold
    # total = 0
    # self.orders.each do |o|
    #   total += o.ordered_products.sum(:quantity)
    # end

    # total
    self.ordered_products.sum(:quantity)
  end

  def stats
    stats     = []
    order_ids = self.orders.collect(&:id)
    products  = OrderedProduct.where(:order_id => order_ids)

    self.settings(:options).prices.each do |p|
      p[:total_sold] = products.where(:category_id => p[:category_id]).sum(:quantity)
      stats << p
    end
    stats
  end

  def self.my_search(keyword, bundle, giftcard)
    begin
      search = {title_or_url_or_tags_contains: keyword}
      if keyword.nil?
        products = self.where(:show => true).where("active = ? AND (date >= ? OR status != ?)", true, Time.zone.now.beginning_of_day, 0)
      else
        # products = self.where(:show => true, :active => true).search(search)
        dashed_keyword = keyword.gsub(" ","-")
        sql = "title LIKE ? OR url LIKE ? OR tags LIKE ?"
        products = self.where(:show => true, :active => true).where(sql, "%#{keyword}%", "%#{keyword}%", "%#{dashed_keyword}%")
      end
      products.where(:bundle => bundle, :gift_card => giftcard).order("id desc")
    rescue
    end
  end

  def stock_list
    in_stocks = []
    self.stocks.group(:category_id).each do |category|
        obj = {:type => category.category.name, :category_id => category.category_id}
        obj[:sizes] = []
      self.stocks.where(:category_id => category.category_id).each do |stock|
        obj[:sizes] << {label: stock.size, value: stock.amount, id: stock.id}
      end
      in_stocks << obj
    end
    in_stocks
  end

  def cart_stocks
    if self.status == Product::CAMPAIGN
      (1..25)
    else

    end
  end
end
