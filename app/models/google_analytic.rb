class GoogleAnalytic < ActiveRecord::Base
  DEFAULT_ID = {
    landing: 'UA-68159572-1',
    thankyou: 'UA-68159572-1'
  }

  belongs_to :product
  attr_accessible :landing_id, :thank_you_id, :product_id
end
