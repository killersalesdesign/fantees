class Order < ActiveRecord::Base
  belongs_to :product
  belongs_to :user
  belongs_to :website
  belongs_to :coupon
  has_many :ordered_products, :dependent => :destroy
  has_many :giftcards, :dependent => :destroy

  # attr_accessible :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :category_id, :size, :quantity

  attr_accessible :card_expires_on, :card_type, :ip_address, :product_id, :user_id, :token, :payer_id, :shipping_info, :printed, :shipped, :description, :order_id, :website_id, :coupon_id,
                  :items, :first_name, :last_name, :email, :phone, :address, :city, :state, :zip, :country, :cc_number, :month, :year, :cvn, :coupon_code, :giftcard_code

  attr_accessor :items, :first_name, :last_name, :email, :phone, :address, :city, :state, :zip, :country, :cc_number, :month, :year, :cvn, :coupon_code, :giftcard_code

  before_update :send_mail

  def send_order_mail(password = nil)
    UserMailer.delay_for(1.minute).send_on_order(self.id, password)
    # UserMailer.send_on_order(self.id, password).deliver
  end

  def cart
    CartCheckout.new(self)
  end

  def new_rder_mail(password = nil, ordered_product)
    UserMailer.on_purchase(self.id, ordered_product, password).deliver
  end

  def total_orders_amount
    subtotal = 0
    quantity = 0
    shipping = 0
    # product  = self.product
    self.ordered_products.each do |p|
      product   = Product.find(p.product_id)
      info      = product.price_and_profit_of_style(p.category_id)
      subtotal += p.quantity * info[:amount]
      quantity += p.quantity
      shipping = info[:postage].to_f if info[:postage] && info[:postage] > shipping
    end

    subtotal = subtotal - (subtotal * (self.coupon.discount.to_f / 100)) unless self.coupon_id.nil?
    shipping = self.product.free_shipping > quantity ? shipping : 0
    # total    = subtotal + shipping
    total    = subtotal
    # number_with_precision(total, :precision => 2)
    total
  end

  def total_sold
    self.orders.sum(:quantity)
  end

  def purchase
    response = EXPRESS_GATEWAY.purchase(price_in_cents, express_purchase_options)
    response.success?
  end

  def price_in_cents
    total   = 0
    product = self.product
    self.ordered_products.each do |p|
      total += p.quantity * product.price_and_profit_of_style(p.category_id)[:amount]
    end

    (total * 100).round
  end

  def product_prices
    prices   = []
    product = self.product
    self.ordered_products.each do |p|
      prices << [:price => product.price_and_profit_of_style(p.category_id)[:amount], :profit => product.price_and_profit_of_style(p.category_id)[:profit],  :type => product.price_and_profit_of_style(p.category_id)[:type], :quantity => p.quantity, :size => p.size ]
    end

    prices
  end

  def product_image(cat_id = "")
    cat_id = self.ordered_products.first.category_id if cat_id.blank?
    images = self.product.product_images.where(:category_id => cat_id)
    images.sample || self.product.product_images.first
  end

  def get_cards
    giftcards = Giftcard.where(:order_id => self.id)
    gift_cards = []
    giftcards.each do |gc|
      gift_cards << {:code => gc.key_code, :status => gc.is_used, :amount => gc.amount}
    end

    giftcards = gift_cards
    return giftcards
  end

  def self.generate_order_id
    Rails.logger.debug("GENERATE & CHECK IF ORDER ID EXIST!")
    code = loop do
      random_code = SecureRandom.hex[0..7].upcase
      break random_code unless Order.exists?(order_id: random_code)
    end

    code
  end

  private

  def express_purchase_options
    {
      :ip => ip_address,
      :currency => 'AUD',
      :token => token,
      :payer_id => payer_id
    }
  end

  def send_mail
    if self.printed == true && changed_attributes["printed"] == false
      UserMailer.delay.send_on_print(self.id)
    end

    if self.shipped == true && changed_attributes["shipped"] == false
      UserMailer.delay.send_on_shipping(self.id)
    end
  end



end