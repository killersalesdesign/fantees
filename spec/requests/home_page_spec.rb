require 'rails_helper'

describe "home page" do
  before :each do
    @website = FactoryGirl.create(:website)
    @categories = FactoryGirl.create(:category)
    @products = FactoryGirl.create(:product)

  end
  it "should display the landing page" do
    get root_path
    expect(response).to be_success
    expect(response.status).to eq(200)
  end

end