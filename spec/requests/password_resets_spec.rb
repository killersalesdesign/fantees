require 'rails_helper'

describe "PasswordResets" do
  it "emails when user requesting password reset" do
    user = create(:user)
    visit new_user_password_path
    # click_link "Forgot Password"
    fill_in "Email", :with => user.email
    click_button "Reset Password"
  end
end
