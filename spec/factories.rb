FactoryGirl.define do

  factory :user do |f|
    first_name "wiredots"
    last_name "esparcia"
    email "test@test.com"
    password "secretasdf"
  end

  factory :product do |p|
    p.url "testonly"
    p.title "testonly"
    p.description "this is lorem ipsum sit dolore amet"
    p.price 0.0
    p.sales 35
    p.active true
    p.profit 0
    p.start_number 1
    p.slug "testonly"
    p.website_id 1
    p.status 2
    p.category_id nil
    p.created_at Time.now.to_date
  end

  factory :website do |w|
    w.url 'https://fantees.com.au'
    w.name 'FanTees'
  end

  factory :category do |c|
    c.name "Tee"
    c.slug "tee"
    c.active true
  end
end